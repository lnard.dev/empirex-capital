<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    use HasFactory;

    protected $fillable=['user_id','sale_volumen', 'reports','sales_goals','sales_month','sales_other_agents'];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
}
