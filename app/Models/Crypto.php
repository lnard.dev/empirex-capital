<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crypto extends Model
{
    use HasFactory;

    protected $fillable = ['looking','crypto','amount','vallet','firstname','email','address','lastname','city','country','phone','code','document','status'];

}
