<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    protected $fillable = [
        'name',
        'lastname',
        'investor_id',
        'identification',
        'state',
        'bank',
        'account_number',
        'type_account',
        'routing_number',
        'zelle',
        'email',
        'second_email',
        'agreements',
        'profits',
        'password',
        'phone',
        'market',
        'payouts',
        'plan',
        'amount',
        'term',
        'address',
        'country',
        'city',
        'region',
        'link',
        'url',
        'type_contrat',
        'status',
        'code_postal',
        'avatar',
        'document',
        'contrat_number',
        'start_date',
        'end_date'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function documents(){
        return $this->hasMany(Document::class);
    }

    public function agreements(){
        return $this->hasMany(Agreement::class);
    }

    public function profits(){
        return $this->hasMany(Profit::class);
    }

    public function reports(){
        return $this->hasMany(Reporte::class);
    }
}
