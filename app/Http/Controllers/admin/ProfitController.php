<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profit;
use App\Models\Agreement;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
class ProfitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profit= new Profit();
        $profit->user_id=$request->id;
        $profit->date_porfit = $request->date_porfit;
        $profit->amount = $request->amount;
        $profit->total = $request->total;
        $profit->notes = $request->notes;

        if($request->file('upload')){
            $profit->upload = $request->file('upload')->store('public');
        }
        $profit->save();

        return back()->with('info','profits added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    $profit = Profit::find($id);
    return view('admin.investor.profit',['profit'=>$profit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      
        $profit = Profit::find($id);
        
        $user =user::find( $profit->user_id);
 
        $agreements  = Agreement::where('user_id',$user->id);
        $profits  = Profit::where('user_id',$user->id);
        $profit->fill($request->all());
        if($request->file('upload')){
            $profit->upload = $request->file('upload')->store('public');
        }

        $profit->save();
        return Redirect::route('investors.index')->with('info','profit updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profit = Profit::find($id);
        Storage::delete($profit->upload);
        $profit->delete();
        return back();
    }
}
