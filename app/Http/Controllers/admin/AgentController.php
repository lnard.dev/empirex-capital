<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Document;
class AgentController extends Controller
{
    
    public function index()
    {
        $agents = User::role('Agent')->take(10)->get();
        return view('admin.agent.index',[
            'agents'=> $agents
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.agent.create');
    }


    public function editarDoc($id, Request $request){
        $document = Document::find($id);
        $document->status = $request->status;
        $document->save();
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|unique:users',
            'contrat_number'=>'required',
        ]);
         $verify = User::where('email',$request->email)->get();
         
         if( $verify->count() > 1 ){
            return back()->with('error','Agent al ready exist');
         }else{
            $user = user::create($request->all());
         
            $user->assignRole('Agent');
            $user->save();
            return redirect()->route('root')->with('info','user created successfuly');
    
         }
        
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agent = User::find($id);
        $document = Document::where('user_id',$id)->get();
        return view('admin.agent.show',[
            'agent'=>$agent,
            'document'=>$document
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agent= User::find($id);
        $agent->delete();
        return back()->with('info','Agent was deleted');
    }
}
