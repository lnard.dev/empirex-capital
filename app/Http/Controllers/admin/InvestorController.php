<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Agreement;
use App\Models\Profit;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;
use App\Mail\MailRegister;
use Illuminate\Support\Facades\Mail;

class InvestorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $investors= User::role('Investor')->get();
         return view('admin.investor.index',[
             'investors'=> $investors
         ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.investor.create');
    }


    public function store(Request $request)
    {
       
        
        $this->validate($request,[
            'email'=>'required|email|unique:users',
            'contrat_number'=>'required',
            'address'=>'required',
            'state'=>'required',
            'country'=>'required',
             'bank'=>'required',
             'routing_number'=>'required',
             'identification'=>'required',
             'lastname'=>'required',
             'second_email'=>'required',
             'phone'=>'required',
             'city'=>'required',
            'code_postal'=>'required',
            'account_number'=>'required',
            'type_account'=>'required',
            'document'=>'mimes:pdf',
            'name'=>'required',
            
        ],['contrat_number.required'=>'the filed Investor # is required']);


        $user = User::create($request->all());
        $pass = Str::random(10);
        $user->password = bcrypt($pass);

        if($request->file('document')){
            $user->document = $request->file('document')->store('public');  
        }

        if($request->file('url')){
            $user->url = $request->file('url')->store('public');  
        }

        $response = [
            'email'=>$request->email,
            'password'=> $pass
        ];

        $user->assignRole(['Investor', 'Newsletter']);
        $user->save();
       /*  Mail::to($request->email)->send(new MailRegister($response)); */
        return back()->with('info','user created successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user = User::find($id);
        return view('admin.investor.show', ['user'=>$user]);
    }

    public function send($id){
   
        $investors = User::role('Investor')->get();
        $user = User::find($id);
        $pass = Str::random(10);

         $response = [
             'email'=>$user->email,
             'password'=>$pass
         ];

         $user->password = bcrypt($pass);
         $user->save();
        // Mail::to($user->email)->send(new MailRegister($response));
         return view('admin.investor.send',['password'=>$pass,'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $agreements = Agreement::orderBy('start_date','asc')->where('user_id',$user->id)->get();
        $profits = profit::orderBy('date_porfit','asc')->where('user_id',$user->id)->get();
        $total = $profits;
      

        return view('admin.investor.edit', [
            'user'       => $user, 
            'agreements' => $agreements,
            'profits'    => $profits,
            'total'=>  $total
            
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
      

        $user = User::find($id);
        $user->assignRole('Investor');
        $user->fill($request->all())->save();
        if($request->file('document')){
          $user->document = $request->file('document')->store('public');  
        }
 
        if($request->file('url')){
            $user->url = $request->file('url')->store('public');  
         }
        $user->save();
        return back()->with('info','client updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $investor = User::find($id);
        $investor->delete();
        return back()->with('info','Investor was deleted');
    }
}
