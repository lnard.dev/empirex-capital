<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class AdminController extends Controller
{
   

    public function __construct(){
        return $this->middleware('auth');
    }

      public function index(){
          return view('admin.dashboard');
      }

      public function root(){
        $investor        = User::role('Investor')->get();
        $ambassador      = User::role('Ambassadors')->get();
        $agent           = User::role('Agent')->get();
        $total           = User::role(['Ambassadors','Investor', 'Agent'])->get();

        $lastInvestors   = User::role('Investor')->orderBy('id','desc')->take(5)->get();
        $lastAmbassadors = User::role('Ambassadors')->orderBy('id','desc')->take(10)->get();
        $lastAgent       = User::role('Agent')->orderBy('id','desc')->take(10)->get();

        return view('admin.root.index',[
            'lastInvestors'=>$lastInvestors,
            'lastAmbassadors'=>$lastAmbassadors,
            'lastAgent' =>$lastAgent,
            'investor'=>$investor->count(),
            'ambassador'=>$ambassador->count(),
            'agent'=>$agent->count(),
            'total'=>$total->count(),
        ]);
    }


    public function lastAddInvestor(){
  
    }


}
