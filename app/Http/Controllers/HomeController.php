<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use  Artesaos\SEOTools\Facades\SEOTools as SEO;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    
    {
       /*  SEO::setTitle('Página SEO');
        SEO::setDescription('Ejemplo de descripción de la página');
        SEO::opengraph()->setUrl('http://nigmacode.com');
        SEO::setCanonical('https://nigmacode.com');
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite('@Nigmacode');
        */

        if ( isset( $_COOKIE[ 'visitas' ] ) ) {

            setcookie( 'visitas', $_COOKIE[ 'visitas' ] + 1, time() + 3600 * 24 );
            $mensaje = 'Numero de visitas: '.$_COOKIE[ 'visitas' ];
            return view('web.home',['mensaje'=> $mensaje]);
        }
        else {
        
            setcookie( 'visitas', 1, time() + 3600 * 24 );
            $mensaje = 'Bienvenido por primera vez a nuesta web';
            return view('web.home',['mensaje'=> $mensaje]);
        }

      
    }
}
