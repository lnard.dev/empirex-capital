<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\MailMessage;
use App\Mail\MailAbout;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNewsletter;
use App\Mail\MailBanner;
use App\Mail\MailNotification;
use App\Models\User;
use App\Models\Crypto;
use App\Models\Message;
use App\Notifications\MessageNotification;

class MessageController extends Controller
{
    
      public function index(Request $request)
      {
                $recaptch = $request->input('g-recaptcha-response');
                if(isset($recaptch) && $recaptch){
                  $secret="6LdlqykaAAAAAENBqgE2xtQXc_xikuo77_BnM4yS";
                  $validador = file_get_contents("https://google.com/recaptcha/api/siteverify?secret=$secret&response=$recaptch"); 
                if($validador==true){ 
                    $response = [ 
                      'name'=>$request->name,
                      'email'=>$request->email,
                      'phone'=>$request->phone,
                      'message'=>$request->message,
                  ];
                  Mail::to('info@empirexcapital.com')->send(new MailMessage($response));
                  return redirect()->route('home')->with('info', 'message  send successfully');
                }
                }else{
                  return back()->with('err','please recaptch is required');
                }
              
      }

      public function newsletter(Request $request)
      {
        $user = User::where('email',$request->email);
        if($user->count() == 1){
          return redirect()->route('home')->with('err','user already exist');
        }else{
           $user= User::create($request->all());
            $user->assignRole('Newsletter');
            $params = [
              'name'=>$request->name,
              'lastname'=>$request->lastname,
              'email'=>$request->email,
            ];
            Mail::to('info@empirexcapital.com')->send(new MailNewsletter($params));
            return redirect()->route('home')->with('info','subscription sent');
        }
        
      }

      public function store(Request $request)
      {
          $message = Message::create([
            'sender_id'=>auth()->user()->id,
            'recipient_id'=>1,
            'name'=>auth()->user()->name,
            'subject'=>$request->subject,
            'body'=>$request->body
          ]);

         $response=[
           "name"=>auth()->user()->name,
           'subject'=>$request->subject,
           "email"=>auth()->user()->email,
           "body"=>$request->body
         ];
          $receive = User::find(1);
          $receive->notify(new MessageNotification( $message) );
          //billing@empirexcapital.com
          //Mail::to('info@empirexcapital.com')->send(new MailNotification($response));
          return back()->with('info','your message was send');
      }

      public function banner(request $request){
            if($request->amount < 1000 ){
              return redirect()->route('home')->with('err','the amount cannot be less than 1000 USD');
            }

          $crypto =  Crypto::create([
              "looking"=>$request->looking,
              'crypto'=>$request->crypto != 'OTHER' ? $request->crypto : $request->otherCrypto,
              "amount"=>$request->amount,
              "vallet"=>$request->wallet,
              "firstname"=>$request->firstname,
              "email"=>$request->email,
              "address"=>$request->address,
              "phone"=>$request->phone,
              "lastname"=>$request->lastname,
              "city"=>$request->city,
              "country"=>$request->country,
              "code"=>$request->code
            ]);

            if($request->file('file')){
                $crypto->document = $request->file('file')->store('public');
            }

            $crypto->save();

            /* $response=[
              "looking"=>$request->looking,
              'crypto'=> $request->crypto != 'OTHER' ? $request->crypto : $request->otherCrypto,
              "amount"=>$request->amount,
              "wallet"=>$request->wallet,
              "firstname"=>$request->firstname,
              "email"=>$request->email,
              "address"=>$request->address,
              "phone"=>$request->phone,
              "lastname"=>$request->lastname,
              "city"=>$request->city,
              "country"=>$request->country,
              "code"=>$request->code
            ];

      
          
            Mail::to('info@empirexcapital.com')->send(new MailBanner($response)); */
            return redirect()->route('home')->with('info','Thank you! You account is being verified. We’ll get in contact with you as soon as you have been approved.');
      }

      public function show($id){
          $users = User::all()->where('name','!=',auth()->user()->name);

          $message = Message::findOrFail($id);
          return view('admin.message.show',[
              'message'=>$message,
              'users'=>$users
          ]);
    }

    public function response(Request $request){
    
     $message =  Message::create([
        'sender_id'=>auth()->user()->id,
            'recipient_id'=>$request->user,
            'name'=>auth()->user()->name,
            'subject'=>"Answering Request",
            'body'=>$request->message
      ]);
      $receive = User::find($request->user);
      $receive->notify(new MessageNotification( $message) );
      return back()->with('info','your message was send');
}



    public function about(Request $request){
      
     $response = [
       'firstname'=>$request->firstname,
       'lastname'=>$request->lastname,
       'email'=>$request->email,
       'phone'=>$request->phone,
       'address'=>$request->address,
       'message'=>$request->message,
     ];

      Mail::to('info@empirexcapital.com')->send(new MailAbout($response));
      return back()->with('info','message was send');
    }

}