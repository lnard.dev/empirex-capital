<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use  Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Activity;

class LoginController extends Controller
{
    
    public function index(){
        return view('auth.login');
    }

    public function login(Request $request){
     $credentials =  request()->validate([
          'email'   =>   'required|string|email',
          'password'=>   'required'
      ]);

      $user = User::where('email',$request->email)->first();
      if($user === null){
          return back()->with('err','user not found');
      }
      if($user->status ==="inactive"){
        return back()->withError(['email'=>trans('auth.failed')]); 
      }

      if(Auth::attempt($credentials)){
          Activity::create([
              "name"       => "el usuario ". auth()->user()->name . " ha iniciado session",
              "description"=> "session inicada"
          ]);
          return redirect()->route('dashboard')->with('info', 'Welcome ' . $user->name);
      }
      return back()->withError(['email'=>trans('auth.failed')]);
  }


  public function logout(){
        Activity::create([
        "name"       => "el usuario  ha cerrado session",
        "description"=> "session cerrada"
       ]);
        Auth::logout();
        return view('web.home');
    }
}
