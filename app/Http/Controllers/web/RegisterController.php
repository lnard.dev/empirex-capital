<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Mail\MailRegister;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
      public function index(){
          return view('auth.register');
      }


      public function register(Request $request){
         $this->validate($request,[
             'email'=>'required|email',
             'contrat_number'=>'required|string',
             'seleccion'=>'required'
         ]);

       if($request->seleccion==""){
          return  back()->with('err', 'Email or contract number incorrect');
       }

       $user =  User::where([ ['email','=',$request->email ], ['contrat_number','=',$request->contrat_number] ])->first();
        if($user === null){
            return back()->with('err','error user not found');
        }
        $pass= Str::random(10);
         $response = [
             'email'=>$request->email,
             'password'=>$pass
         ];
         $user->password = bcrypt($pass);
         $user->save();

         if($request->seleccion=="Agent"){
            $user->assignRole('Agent');
         }
         
         if($request->seleccion=="Investor"){
            $user->assignRole('Investor');
         }
         if($request->seleccion=="Ambassador"){
            $user->assignRole('Ambassador');
         }
  
        if(!empty($user)){
            Mail::to($request->email)->send(new MailRegister($response));
            return back()->with('info','Thank you for sign up, we will send you an email with the password');
        }else{
            return  back()->with('err', 'Email or contract number incorrect');
        }
        
    }
}
