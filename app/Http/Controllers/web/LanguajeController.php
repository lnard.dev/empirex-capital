<?php

namespace App\Http\Controllers\web;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class LanguajeController extends Controller
{
    
      public function index($locale){
         Session::put('locale',$locale);
         return  redirect()->back();
      }

}
