<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Agreement;
use App\Models\Profit;
use Illuminate\Support\Facades\Storage;
class InversionistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id' ,auth()->user()->id)->first();
        return view('admin.information.investment.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = User::role('Admin')->get();
       
        $user = User::find($id);

        $agreements = Agreement::orderBy('start_date','asc')->where('user_id',$user->id)->get();
        $profits    = Profit::orderBy('date_porfit','asc')->where('user_id',$user->id)->get();
        return view('admin.information.investment.show',[
             'user'      => $user, 
             'agreements'=> $agreements, 
             'profits'   => $profits,
             'admin'=>$admin
             ]);
    }

    public function bank($id){
        $user = User::find($id);
        return view('admin.information.investment.bank',[
            'user'=> $user, 
            ]);
        
    }
    public function document($id){
        $user = User::find($id);
        $agreements = Agreement::orderBy('start_date','desc')->where('user_id',$user->id)->get();
        $profits    = Profit::orderBy('date_porfit','desc')->where('user_id',$user->id)->get();
        return view('admin.information.investment.document',[
            'agreements'=> $agreements, 
            'profits'   => $profits,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.information.investment.edit',[ 'user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->fill($request->all());
        if($request->file('avatar')){
            $user->avatar = $request->file('avatar')->store('public');
        }

        if($request->password){
           $user->password = $request->password;
        }

        $user->password = bcrypt($request->password);
        $user->save();
        return back()->with('info','updated information');
    }


    public function avatar(Request $request, $id){
        $user = User::find($id);
    
        if($request->file('avatar')){
            Storage::delete($user->avatar);
            $user->avatar = $request->file('avatar')->store('public');
        }
        $user->save();
        return back()->with('info','updated information');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
