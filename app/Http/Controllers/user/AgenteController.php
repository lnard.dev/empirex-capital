<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Document;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Mail;
use Spatie\Browsershot\Browsershot as PDF2;
use Dompdf\Dompdf;
use Dompdf\Options;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Redirect;
use Barryvdh\Snappy\Facades\SnappyPdf as PDFS;

class AgenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = User::where('id',auth()->user()->id)->first();
        return view('admin.information.agent.profile', compact('user'));
    }

   
     public function contrat(){
         return view('admin.information.agent.contrat');
     }

     public function reports(){
        return view('admin.information.agent.report');
    }
     

     public function upload(){
        $documents= Document::where('user_id',auth()->user()->id)->get();
        return view('admin.information.agent.upload',[ 'documents' => $documents ]);
    }

    public function client(){
        return view('admin.information.agent.client');
    }

    public function agente_investor(request $request){
        request()->validate([
            'email'=>'required|unique:users'
        ]);
        $user = User::create($request->all());
        $user->assignRole('Investor');

        if($request->type_contrat=="Traditional Fixed"){
          return Redirect::to("https://documentcloud.adobe.com/link/review?uri=urn:aaid:scds:US:033b7d18-36df-4b6e-b3d4-cb5773912601");
        }

        if($request->type_contrat=="Traditional Fixed Monthly Payouts"){
            return Redirect::to("https://documentcloud.adobe.com/link/review?uri=urn:aaid:scds:US:dd562d67-21ad-44c3-83f3-4aaa9598c966");
        }

        if($request->type_contrat=="Traditional Variable"){
            return Redirect::to("https://documentcloud.adobe.com/link/review?uri=urn:aaid:scds:US:b58d9f54-f7a8-4964-bfe4-eb43dc82b6b6");
        }

        if($request->type_contrat=="Traditional Variable Monthly Payouts"){
            return Redirect::to("https://documentcloud.adobe.com/link/review?uri=urn:aaid:scds:US:563de7b0-3b7f-4c3c-8709-b154e38fbe79");
        }

        if($request->type_contrat=="Crypto Variable"){
            return Redirect::to("https://documentcloud.adobe.com/link/review?uri=urn:aaid:scds:US:e769680f-ef10-47d6-a240-e77f2c95f1bd");
        }

        if($request->type_contrat=="Traditional Short Term Variable"){
            return Redirect::to("https://documentcloud.adobe.com/link/review?uri=urn:aaid:scds:US:8265754c-fe6e-428f-92bb-8515e9016275");
        }
     
        return back()->with('info','client created ');
    }


    public function contrat_generate(Request $request){
         $data["name"]  = $request->investor_name;
         $data["amount"] = $request->amount;
         $data["email"]  = $request->email;
        

       $month=["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];

      /*if($request->term == 3){
            $day = date("d");
            $year = date('Y');
            $res = (int)date("m",strtotime(date("d-m-Y")."+ 3 month")); 
            $month = $month[$res-1];    
        
            if(date('m') == "10" || date('m') == "11"  || date('m') == "12" ){
                $year1 = date('Y')+1;
                $data["term"]= $month . " ". $day . " " .$year1;
            }
            $data["term"]= $month . " ". $day . " " .$year;
            
      }
         if($request->term == 6){
                $day = date("d");
                $year = date('Y');
                $res = (int)date("m",strtotime(date("d-m-Y")."+ 6 month")); 
                $month = $month[$res-1];
        
        if(date('m') === "7" || date('m') === "8"  || date('m') === "9" || date('m') === "10" || date('m') === "11"  || date('m') === "12" ){
             $year1 = date('Y')+1;
             $data["term"]= $month . " ". $day . " " .$year1;
        }
          
            $data["term"]= $month . " ". $day . " " .$year;
          


        }

        if($request->term == 12){
            $day = date("d");
            $year = date('Y')+1;
            $res = (int)date("m",strtotime(date("d-m-Y")."+ 12 month")); 
            $month = $month[$res-1];
            $data["term"]= $month . " ". $day . " " .$year;
        }*/

      
       $pdf   = PDF::loadview('contrat');
       return  $pdf->stream();
  
      /*Mail::send('contrat', $data, function($message)use($data,$pdf) {
        $message->to($data['email'])
                ->from('empirexcapital2021@gmail.com')
                ->subject('Contract')
                ->attachData($pdf->output(), "contract.pdf");
        });
         return "enviado";*/   
      
    }

    public function deleteDocument($id){
        $document = Document::find($id);
        Storage::delete($document->url);
        $document->delete();
       return back()->with('info','document removed');
    }



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'file'=>'required|max:10000|mimes:doc,docx,pdf'
        ]);
        $document = new Document();
       
        $document->user_id = auth()->user()->id;
        $document->name = $request->name;
 
        if($request->file('file') ){
            $document->url = $request->file('file')->store('public');
        }
        $document->save();
        return back()->with('info','file updated successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user= User::find($request->id);

        $user->fill($request->all());
        $user->password= bcrypt($request->password);
        $user->save();
        return back()->with('info','profile was updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
