<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reportes', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')->nullable();
            
            $table->string('sale_volumen')->nullable();
            $table->string('reports')->nullable();
            $table->string('sales_goals')->nullable();
            $table->string('sales_month')->nullable();
            $table->string('sales_other_agents')->nullable();
            $table->string('documents')->nullable();

            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reportes');
    }
}
