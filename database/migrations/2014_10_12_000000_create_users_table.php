<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();

            $table->string('investor_id')->nullable();
            $table->string('identification')->nullable();
            $table->string('state')->nullable();

            $table->string('bank')->nullable();
            $table->string('account_number')->nullable();
            $table->string('type_account')->nullable();
           
            $table->string('routing_number')->nullable();
            $table->string('zelle')->nullable();
            

            $table->string('email')->unique();
            $table->string('second_email')->nullable();

            $table->string('phone')->nullable();
            $table->string('contrat_number')->nullable();
            $table->string('market')->nullable();
            $table->string('payouts')->nullable();
            $table->string('plan')->nullable();
            $table->float('amount',8,2)->nullable();
            $table->string('term')->nullable();

            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('link')->nullable();
            $table->string('url')->nullable();
            $table->string('code_postal')->nullable();
            $table->string('type_contrat')->nullable();

            $table->enum('status',['active','inactive'])->default('active');
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('avatar')->nullable();
            $table->string('document')->nullable();

            $table->string('agreements')->nullable();
            $table->string('profits')->nullable();

           
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
