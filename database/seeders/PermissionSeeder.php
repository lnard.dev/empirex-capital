<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
      //investor
        Permission::create([
             'name'=>'create.investor', 
        ]);
        Permission::create([
            'name'=>'show.investor', 
   
       ]);
            Permission::create([
                'name'=>'edit.investor', 
             
        ]);
        Permission::create([
            'name'=>'delete.investor', 
        
        ]);

        //Ambassadors
        Permission::create([
            'name'=>'create.ambassador', 
        
       ]);
       Permission::create([
           'name'=>'show.ambassador', 
          
      ]);
       Permission::create([
               'name'=>'edit.ambassador', 
               
       ]);
       Permission::create([
           'name'=>'delete.ambassador', 
          
       ]);

       //agente

       Permission::create([
        'name'=>'create.agent', 
    
        ]);
        Permission::create([
            'name'=>'show.agent', 
            
        ]);
        Permission::create([
                'name'=>'edit.agent', 
                
        ]);
        Permission::create([
            'name'=>'delete.agent', 
            
        ]);

         $admin            = Role::create(['name'=>'Admin']);
         $investor         = Role::create(['name'=>'Investor']);
         $Ambassadors      = Role::create(['name'=>'Ambassadors']);
         $agent            = Role::create(['name'=>'Agent']);
         $newsletter       = Role::create(['name'=>'Newsletter']);

         $admin->givePermissionTo(Permission::all()); 
         
         $investor->givePermissionTo('show.investor','edit.investor');
         $Ambassadors->givePermissionTo('show.ambassador','edit.ambassador');
         $agent->givePermissionTo('show.agent', 'edit.agent');

         User::find(1)->assignRole('Admin');
         User::find(2)->assignRole('Investor');
      

    }
}
