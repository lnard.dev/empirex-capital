<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\User::create([
            'name'=>'Empirex Capital',
            'email'=>'admin@empirexcapital.com',
            'password'=>bcrypt('Empirex12345')
        ]);

        \App\Models\User::create([
            'name'=>'leonardo pernett jimenez',
            'email'=>'pernettleonardo@gmail.com',
            'password'=>bcrypt('123456')
        ]);


        $this->call([PermissionSeeder::class]);
        
        
    }
}
