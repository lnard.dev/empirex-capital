<?php

use Illuminate\Support\Facades\Route;
use App\Models\Activity;


Route::view('cont','contrat');
//web
Route::get('/',[App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::view('about','web.about')->name('about');
Route::view('portfolio','web.portafolio')->name('portafolio');
Route::view('blog','web.blog')->name('blog');
Route::view('embajadores','web.embajadores')->name('embajadores');
Route::view('contact','web.contact')->name('contact');
Route::view('ver','mail.message');


  Route::view('/bitcoin-price-eyes-30%-correction','web.blogger.first')->name('first');
  Route::view('/bitcoin-real-daily-trading-volume-tops','web.blogger.second')->name('second');
  Route::view('/investing-in-times-crisis','web.blogger.third')->name('third');
  Route::view('/dow-jones-falls-more-than-200-points-in-volatile-trading','web.blogger.four')->name('four');
  Route::view('/bitcoin-sets-new-record-to-top-$23,000','web.blogger.five')->name('five');
  Route::view('/blockchain-mysteries-biggest-crypto-transaction-fee-oddities','web.blogger.six')->name('six');
  Route::view('/top-5-cryptocurrencies','web.blogger.eight')->name('eight');
  Route::view('/netflix-might-be-next-fortune-100-firm-to-buy-bitcoin','web.blogger.nine')->name('nine');
  Route::view('/bitcoin-price-hits-$40,000-less-than-three-weeks','web.blogger.ten')->name('ten');
  Route::view('/Ethereum-into-list-of-top-100-assets-in-the-world','web.blogger.eleven')->name('eleven');
  Route::view('/young-americans-will-buy-bitcoin-with-their-stimulus-checks','web.blogger.twelve')->name('twelve');
  Route::view('/fidelity-doubles-down-on-hong-kong-crypto-operator','web.blogger.thirteen')->name('thirteen');
  Route::view('/ether-price-finally-beats-its-2018-all-time-high','web.blogger.fourteen')->name('fourteen');
  
  Route::view('/the-european-stock-exchanges-remain-without-fuelle','web.blogger.fifteen')->name('fifteen');
  Route::view('/dubai-financial-regulator-working-on-regulations-for-cryptocurrencies','web.blogger.sixteen')->name('sixteen');
  Route::view('/robinhood-users-still-have-a-purchase-limit','web.blogger.seventeen')->name('seventeen');
  Route::view('/optimism-of-the-Joe-Biden-presidency-for-bitcoin','web.blogger.eighteen')->name('eighteen');
  Route::view('/biden-preps-$3T-stimulus-bitcoin-could-be-bet-to-erupt','web.blogger.nineteen')->name('nineteen');
  Route::view('/bitcoin-price-breaks','web.blogger.twenty')->name('twenty');
   
  Route::view('/Winklevoss-Brothers-Top-Forbes-Bitcoin-Billionaires-List','web.blogger.twentyOne')->name('twentyOne');
  Route::view('/Ether-is-Flying-to-the-Moon-The-Analyze-for-Empirex-Capital','web.blogger.twentyTwo')->name('twentyTwo');
  Route::view('/the-cryptocurrency-that-became-the-new-target-of-investors','web.blogger.twentyThree')->name('twentyThree');
  Route::view('/bitcoins-coinbase-premium-turned-negative','web.blogger.twentyFour')->name('twentyFour');
  Route::view('/soccer-fan-tokens-on-the-march-as-poland-biggest-club-adopts-crypto','web.blogger.twentyFive')->name('twentyFive');
  
  Route::view('/Swiss-Crypto-ETP-Issuer-Passes-$1B-Assets-Under-Management','web.blogger.twentysix')->name('twentysix');
  Route::view('/Tesla-Bitcoin-and-the-Crypto-Space','web.blogger.twentyseven')->name('twentyseven');
  Route::view('/how-crypto-is-going-to-shake-up-the-world-of-mixed-martial-arts','web.blogger.twentyeight')->name('twentyeight');
  
  Route::view('/3-reasons-bitcoin-recovered-by-8-overnight','web.blogger.twentynine')->name('twentynine');
  Route::view('/IBEX-Exceeds-8400-Points-While-Markets-Weigh-the-Effect-of-Biden-Stimulus','web.blogger.thirty')->name('thirty');
  Route::view('/The-Cardano-Surpasses-the-Bitcoin-Rally-and-Ranks-in-the-Top-3-of-the-Crypto','web.blogger.thirtyone')->name('thirtyone');

  Route::view('/Hong-Kong-Companies-have-Committed-Billion-in-just-Seven-Months','web.blogger.thirtyTwo')->name('thirtyTwo');
  Route::view('/What-are-the-Non-Fungible-Tokens-with-Which-you-Buy-Art-Music-and-even-Tweets','web.blogger.thirtyThree')->name('thirtyThree');
  Route::view('/US-China-Talks-Keys-on-Wall-Street','web.blogger.thirtyfour')->name('thirtyfour');

//blogger


//languages
Route::get('locale/{locale}',[App\Http\Controllers\web\LanguajeController::class, 'index']);
Route::view('/terms-and-conditions.html','term')->name('term');

//mail
Route::post('message',[App\Http\Controllers\web\MessageController::class, 'index'])->name('message.index');
Route::post('newsletter',[App\Http\Controllers\web\MessageController::class, 'newsletter'])->name('newsletter');
Route::post('send',[App\Http\Controllers\web\MessageController::class, 'store'])->name('message.store');


Route::get('newsletter',function(){
  return view('newlatter');
})->name('noticia');

Route::view('/ver','mail.message')->name('ver');

///login
Route::get('/login',[App\Http\Controllers\web\LoginController::class, 'index'])->name('login');
Route::post('/login',[App\Http\Controllers\web\LoginController::class, 'login'])->name('login');


//register
Route::get('/register',[App\Http\Controllers\web\RegisterController::class, 'index'])->name('register.index');
Route::post('/register',[App\Http\Controllers\web\RegisterController::class, 'register'])->name('register');

//admin
Route::get('/dashboard',[App\Http\Controllers\admin\AdminController::class, 'index'])->name('dashboard')->middleware('auth');
Route::get('/dashboard/admin',[App\Http\Controllers\admin\AdminController::class, 'root'])->name('root')->middleware('auth');
Route::resource('investors',App\Http\Controllers\admin\InvestorController::class)->middleware('auth');
Route::get('invertir/{id}',[App\Http\Controllers\admin\InvestorController::class, 'send'])->name('invertir.send')->middleware('auth');



Route::resource('ambassadors',App\Http\Controllers\admin\AmbassadorController::class)->middleware('auth');
Route::resource('agent',App\Http\Controllers\admin\AgentController::class)->middleware('auth');
Route::resource('newsletters',App\Http\Controllers\admin\NewsletterController::class)->middleware('auth');
Route::resource('buy/sell',App\Http\Controllers\admin\BuyController::class)->middleware('auth');



Route::get('agent/edit/{id}',[App\Http\Controllers\admin\AgentController::class,'editarDoc'])->name('edit.doc')->middleware('auth');

//investor
Route::resource('investment',App\Http\Controllers\user\InversionistaController::class)->middleware('auth');

Route::put('investment/avatar/{id}',[App\Http\Controllers\user\InversionistaController::class,'avatar'])->name('investment.avatar')->middleware('auth');
Route::get('investment/bank/{id}',[App\Http\Controllers\user\InversionistaController::class,'bank'])->name('investment.bank')->middleware('auth');
Route::get('investment/document/{id}',[App\Http\Controllers\user\InversionistaController::class,'document'])->name('investment.document')->middleware('auth');
Route::view('inbox','admin.information.investment.request')->name('inbox')->middleware('auth');



//agent
Route::get('/agente',[App\Http\Controllers\user\AgenteController::class, 'profile'])->name('agente.profile')->middleware('auth');
Route::put('/agente/update',[App\Http\Controllers\user\AgenteController::class, 'update'])->name('agente.update')->middleware('auth');
Route::get('/contrat',[App\Http\Controllers\user\AgenteController::class, 'contrat'])->name('agente.contrat')->middleware('auth');
Route::get('/upload-file',[App\Http\Controllers\user\AgenteController::class, 'upload'])->name('agente.upload')->middleware('auth');
Route::post('/upload-file',[App\Http\Controllers\user\AgenteController::class, 'store'])->name('agente.file')->middleware('auth');
Route::get('/delete/document/{id}',[App\Http\Controllers\user\AgenteController::class, 'deleteDocument'])->name('remove.document')->middleware('auth');
Route::get('/agente/report',[App\Http\Controllers\user\AgenteController::class, 'reports'])->name('agente.report')->middleware('auth');
Route::post('/agente/contrat',[App\Http\Controllers\user\AgenteController::class, 'contrat_generate'])->name('agente.contratos')->middleware('auth');
Route::get('/agente/client',[App\Http\Controllers\user\AgenteController::class, 'client'])->name('agente.client')->middleware('auth');
Route::post('/agente/client',[App\Http\Controllers\user\AgenteController::class, 'agente_investor'])->name('agente.investors.store')->middleware('auth');


//Agreemnet
Route::resource('agreements',App\Http\Controllers\admin\AgreementController::class)->middleware('auth');
Route::resource('profits',App\Http\Controllers\admin\ProfitController::class)->middleware('auth');

//commnet
Route::resource('/comment',App\Http\Controllers\CommentController::class);

//activity
Route::get('/activity',[App\Http\Controllers\admin\ActivityController::class, 'index'])->name('activity.index');

Route::get('message/{id}',[App\Http\Controllers\web\MessageController::class, 'show'])->name('message.show')->middleware('auth');
Route::post('response',[App\Http\Controllers\web\MessageController::class, 'response'])->name('response')->middleware('auth');


Route::get('/notifications', [App\Http\Controllers\NotificationController::class, 'index'])->name('notification.index')->middleware('auth');

Route::patch('/notifications/{id}', [App\Http\Controllers\NotificationController::class, 'read'])->name('notification.read')->middleware('auth');
Route::delete('/notifications/{id}', [App\Http\Controllers\NotificationController::class, 'destroy'])->name('notification.destroy')->middleware('auth');


Route::post('/banner', [App\Http\Controllers\web\MessageController::class, 'banner'])->name('banner');

//Auth::routes();
Route::get('/logout',[App\Http\Controllers\web\LoginController::class, 'logout'])->name('logout');

//hace storage:link
Route::get('storage-link',function(){
  
  if(file_exists(public_path('storage'))){
    return "the public storage fue creado";
  }

  app('files')->link(storage_path('app/public'), public_path('storage'));
  return "the public storage fue creado";
  
});

Route::get('migration', function(){
  
   Artisan::call('migrate', ['--force' => true]);


   return "migration successfully";
});

/* Route::get('refresh', function(){
  Artisan::call('migrate:refresh --seed');
  return "migration successfully";
});  */

Route::get('clear',function(){
  Artisan::call('config:cache');
  Artisan::call('config:clear');
  Artisan::call('cache:clear');
  return "exito";
});



use App\Models\User;
use Illuminate\Support\Str;

Route::get('/send/{id}', function($id){
  $user = User::find($id);
  $pass = Str::random(10);
  $user->password= bcrypt($pass);
  $user->save();
  $correo = $user->email;
  $beautymail = app()->make(Snowfire\Beautymail\Beautymail::class);
	$beautymail->send('mail.register', [
    'name'=>$user->name,
    'email'=>$user->email,
    'password'=>$pass,
  ], function($message) use ($correo)
	{
		$message
			->from('info@empirexcapital.com', 'Empirex Capital')
			->to($correo,'Clientes Empirex Capital')
			->subject('Credenciales Portal Empirex Capital');
	});
  return back()->with('info','Thank you for sign up, we will send you an email with the password');
})->name('investors.send')->middleware('auth');



Route::post('send-about',[App\Http\Controllers\web\MessageController::class, 'about'])->name('message.about');

use Illuminate\Http\Request;

/* Route::post('/send-about', function(Request $request){
  dd($request->all());
  $beautymail = app()->make(Snowfire\Beautymail\Beautymail::class);
	$beautymail->send('mail.about', [
    'firstname'=>$request->firstname,
    'lastname'=>$request->lastname,
    'email'=>$request->email,
    'phone'=>$request->phone,
    'address'=>$request->address,
    'message'=>$request->message
  ], function($message)
	{
		$message
			->from('info@empirexcapital.com', 'Empirex Capital')
			->to('pernettleonardo@gmail.com','Clientes Empirex Capital')
			->subject('Credenciales Portal Empirex Capital');
	});
  return back()->with('info','Thank you for sign up, we will send you an email with the password');
})->name('message.about');
 */
