<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title') </title>
     <!--===============================================================================================-->
	   <link rel="stylesheet" type="text/css" href="{{asset('vendor/animate/animate.css')}}">
    <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/login.css')}}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    @yield('styles')
</head>
<body>
    
  @yield('content')
      <!--===============================================================================================-->	
	<script src="{{asset('vendor/jquery/jquery-3.2.1.min.js')}}"></script>
  <!--===============================================================================================-->
      <script src="{{asset('vendor/bootstrap/js/popper.js')}}"></script>
      <script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
  <!--===============================================================================================-->
      <script src="{{asset('vendor/select2/select2.min.js')}}"></script>
  <!--===============================================================================================-->
      <script src="{{asset('vendor/tilt/tilt.jquery.min.js')}}"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      <script >
          $('.js-tilt').tilt({
              scale: 1.1
          })
      </script>

      @yield('scripts')
</body>
</html>