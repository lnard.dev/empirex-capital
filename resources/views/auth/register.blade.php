@extends('auth.layouts')
@section('styles')
      <!-- principal style -->
      <link href="{{asset('css/app.css') }}" rel="stylesheet">
  
      <!-- Vendor CSS Files -->
      <link href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
      <link href="{{asset('assets/vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
      <link href="{{asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
      <link href="{{asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
      <link href="{{asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
      
      <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,600;0,800;0,900;1,700&display=swap" rel="stylesheet">    <!-- Styles -->
      <link href="{{ asset('css/style.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  

      <style>
         
      </style>

@endsection

@section('content')
 <!-- ======= Header ======= -->
 @include('partials.navigation')
 <!-- End Header -->
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="{{asset('assets/img/logo1.png')}}" alt="IMG">
            </div>

              <div class="contact-imagen">
                  <img src="{{asset('assets/img/logo1.png')}}" alt="rocket_contact"/>
              </div>

           <form action="{{route('register')}}" method="POST" class="mx-auto">
                @csrf
                <span class="login100-form-title" style="color: #001838; font-weight:bold" >
                    {{__('Register')}}
                </span>


                <div class="wrap-input100 validate-input @error('email') is-invalid @enderror">
                    <input class="input100" type="text" name="email" placeholder="{{__('Email')}}" value="{{old('email')}}" required >
                    @error('email') <small class="text-danger">{{$message}}</small>@enderror

                </div>

                <div class="wrap-input100 validate-input @error('password') is-invalid @enderror">
                    <input class="input100" type="text" name="contrat_number" placeholder="{{__('Number')}}" required>
                    @error('contrat_number') <small class="text-danger">{{$message}}</small>@enderror
                </div>

                <div class="wrap-input100 validate-input @error('password') is-invalid @enderror">
                    <select  class="input100" name="seleccion" id="" required>
                        <option value="">{{__("You're a")}} ? </option>
                        <option value="Investor">{{__('Investor')}}</option>
                        <option value="Ambassador">{{__('Ambassador')}}</option>
                        <option value="Agent">{{__('Agent')}}</option>

                    </select>
                    @error('seleccion') <small class="text-danger">{{$message}}</small>@enderror
                </div>
                
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn" type="submit">
                        {{__('Save')}}
                    </button>+
                    <p>  {{__('Do you have an account')}}<a href="{{route('login')}}"> Sign In </a> <br>
                      
                    </p>  
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
     @if(session('info'))
        <script>
            toastr.success("{!! session('info') !!}")
        </script>
     @endif
    
     @if(session('err'))
     <script>
         toastr.error("{!! session('err') !!}")
     </script>
  @endif

@endsection