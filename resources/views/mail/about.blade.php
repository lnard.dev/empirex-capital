<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>message</title>
   <style>
     
     p{
       color:#000;

     }
     h2 {
       color:#001838;
       text-align: center;
       font-family: 'Courier New', Courier, monospace;
       font-weight: bold;
     }
     b{
       color:#001838;
     }

     .card{
       box-shadow: 0 0 15px rgba(0, 0, 0, .4);
       border-radius: 10px;
       padding: 20px;
       width: 400px;
       font-family: 'Courier New', Courier, monospace
     }

   </style>
</head>
<body>

 <div class="container">
  <div class="card">
    <h2>EMPIREX CAPITAL</h2>
    <p> <b>First Name: </b> {{$sms['firstname']}} </p>
    <p> <b>Last name: </b> {{$sms['lastname']}} </p>
    <p> <b>Email:</b> {{$sms['email']}} </p>
    <p> <b>Phone:</b> {{$sms['phone']}}</p>
    <p> <b>message: </b> {{$sms['message']}} </p> 
  
</div>
 </div>
</body>
</html>