<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>message</title>
   <style>
     

html, body{
    margin: 0;
    padding: 0;
}

body{
    width: 100%;
    height: 100%;
    font-family: sans-serif;
    letter-spacing: 0.03em;
    line-height: 1.6;
    font-family: 'Open Sans', sans-serif;
}

.title{
    font-size: 40px;
    color: #6a6a6a;
    margin-top: 100px;
    font-weight: 100;
    font-family: 'Roboto', sans-serif;
}


.container{
    width: 100%;
    max-width: 1200px;
    height: 430px;
    display: flex;
    flex-wrap: wrap;
  
    margin: auto;
}

.container .card{
    width: 330px;
    height: 430px;
    border-radius: 8px;
    box-shadow: 0 2px 2px 0px rgba(0, 0, 0, 0.2);
    overflow: hidden;
    margin: 20px;
    transition: all 0.25s;
    padding:1em;
}

.container .card:hover{
    transform: translateY(-15px);
    box-shadow: 0 12px 16px rgba(0, 0, 0, 0.2);
}


.container .card h4{
    font-weight: 600;
    color:#001838;
    text-align: center;
    font-size: 30px
}

.container .card p{
    padding: 0 1rem;
    font-size: 16px;
    font-weight: 300;
}

.container .card a {
    font-weight: 500;
    text-decoration: none;
    color: #3498db;
}

   </style>
</head>
<body>
 <div class="container">
  <div class="card">
    <h4>Empirex Capital</h4>
   
    <p> <b>First name:</b> {{$sms['name']}}    </p>
    <p> <b>Email:</b>      {{$sms['email']}}    </p>
    <p> <b>subject:</b>      {{$sms['subject']}}    </p>
    <p> <b>Message:</b>      {{$sms['body']}}    </p>
</div>
 </div>
</body>
</html>