<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Contract</title>

  <style>
    body {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 14px;
      
    }
    .principal{
      margin:0 auto;
      width: 90%;
    }
     h3{
       text-align: center;
     }

     .subtitle h5{
        text-decoration:underline;
     }

     .parrafo1 p {
       text-align: justify;
     }

     .firma{
       display: flex;
       flex-direction: column;
       margin-top: 100PX;
     }

     footer .pie {
       text-align: center;
       margin-top: 10px;
       font-size: 13PX;
     }

   h3{
     color:red;
   }
     
  </style>
</head>
<body>
  <div class="principal">
    <img src="{{ public_path('/assets/img/logo1.png')}}">
    <img src="{{ storage_path('app/public/default.jpg') }}" style="width: 200px; height: 200px">

    <h3>INVESTMENT AGREEMENT</h3>
    <p class="date">Agosto 15 2020</p>
    <p>I, <strong>RAFAEL VARGAS </strong> from <strong>Empirex Capital LLC</strong>, Received from  ( $  Dollars in Cash -WIRE) as a traditional investment.</p>
    
    <p>We'll diversify the investment under our own criteria, and we'll deliver a 5% fixed rate of monthly profits on your investment.</p>
   
    <div class="subtitle">
      <h5>Global Macro Strategy:</h5>
    </div>

    <div class="parrafo1">
      <p>
        Empirex Capital's long/short equity solutions are designed to maximize gains while minimizing risk through a well-
        diversified and hedged portfolio. With a long-term outlook which we believe is most important, we create a portfolio that is
        easy to understand, operate efficiently, transparent, liquid, and most of all – profitable, regardless of market direction.
     </p>

      <p>We understand that individuals are sometimes forced to make financial decisions in the short-to-medium term. Therefore,
        through our strategy we ensure liquidity, and that portfolios can handle periodic volatility without abandoning the long-
        term investment plan. Our goal with our strategy is to provide a diversified return stream across different market
        environments, provide transparency, reduce volatility, and hedge appropriately through severe market disruption.
      </p>
      <p>A well-established client relationship, a balanced investment plan and consistent communication are the tools that we
        employ to help our clients fully understand their portfolio and market movement.
      </p>
      <p>We seek highly asymmetric investments, in which the upside potential significantly exceeds the downside risk.</p>
    
      <p><strong> Agreement:</strong> no disbursements will be done until , at that point you can withdraw the $  US Dollars investment. Profits will be paid out monthly. If any money will be left in your fund, to continue the investment we'll  generate an amendment to this agreement specifying the money will be reinvested and their respective terms.</p>
     
      <p>Disclaimer: Deposit of profits make take up to 30 business days to be completed.</p>
    </div>


    <div class="firma">
      <span><strong>RAFAEL VARGAS </strong></span>
     
    </div>
  
    <footer>
      <div class="pie">
        <span>3 Columbus Circle, 15th Floor. New York, NY 10019</span><br>
        <span>rafael@empirexcapital.com - www.empirexcapital.com</span><br>
        <span>(201) 207-9236</span>
      </div>
    </footer>
  
  </div>

 

 

</body>
</html>

<script>



</script>


