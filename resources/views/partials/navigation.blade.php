<style>
   .btn-portal{
    background: #001838 ;
    color:#fff !important;
    border-radius: 1000px;
    padding: 1px 25px;
    width: 70px;
    text-align: center
   }

   .btn-portal:hover{
      
      color: #001838 !important;
      border: 1px solid #001838;
      background: #fff
   }

   #header .logo {
    margin-left: 50px!important;
   }

   
   .main-nav a {  
    margin-left: 20px!important;
    }

    .mt-5, .my-5 {
    margin-top: 20px!important;
    
    }


   @media only screen and (min-width: 320px) and (max-width: 736px) {
      #header .logo {
      margin-left: 20px!important;
    }
    .menu_navigation{
    
    font-size: 20px!important;
    margin-left: 56px!important;
    
    }

   }

   


    .empirex_logo{
      width: 45%!important;
      height: auto!important;
    }



    .header_top{
      background: #ffffffde!important;
    }

    .header_scroll{
      background: white!important;
    }

    .menu_item{
      font-family: var(--font-principal)!important;
    }
</style>

<header id="header" class="fixed-top header-transparent">
  <div class="contenedor d-flex justify-content-between">

    
    <h1 class="logo mr-auto"><a href="/">
      <img class="empirex_logo" src="{{asset('assets/img/logo1.png')}}" alt="" >
      </a>
    </h1>
      
    <nav class="main-nav d-none d-lg-inline mt-5 ">
      <ul class="mt-5 menu_navigation">
        <li ><a href="{{route('home')}}" class="menu_item {{ request()->routeIs('home') ? 'active' :''}}">{{__('Home') }}</a></li>
        <li><a href="{{route('about')}}" class="menu_item {{ request()->routeIs('about') ? 'active' :''}}">{{__('About Us') }}</a></li>
        <li><a href="{{route('portafolio')}}" class="menu_item {{ request()->routeIs('portafolio') ? 'active' :''}}">{{__('Portfolio') }}</a></li>
        <li><a href="{{route('contact')}}" class="menu_item {{request()->routeIs('contact') ? 'active' :'' }}">{{__('Contact Us') }}</a></li>
        <li><a href="{{route('blog')}}" class="menu_item {{ request()->routeIs('blog') ? 'active' :''}}">{{__('Blog') }}</a></li>
        <li class="">
          <a href="{{ url('locale/es') }}">
             <img src="{{asset('assets/img/spanish.png') }}" alt="" width="25" height="15">
          </a>
          </li>
          <li class="">
            <a href="{{ url('locale/en') }}">
               <img src="{{asset('assets/img/english.png') }}" alt="" width="25" height="15">
            </a>
            </li  >
            <li ><a href="{{route('login')}}" class=" menu_item btn-portal {{request()->routeIs('login') ? 'active' : ''}}">{{__('Login') }}</a></li>
      {{--       <li><a href="{{route('register.index')}}" class=" {{ request()->routeIs('register') ? 'active' : ''}} primary">{{__('Register') }}</a></li> --}}
      </ul>
    </nav><!-- .main-nav-->
   
   
    <!-- Uncomment below if you prefer to use an image logo -->
    <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
  </div>

  <!--<div class="menu2">
    <nav class="main-nav d-none d-lg-block">
      <ul class="content-menu2">
        <li ><a href="{{route('home')}}" class="{{ request()->routeIs('home') ? 'active' :''}}">{{__('Home') }}</a></li>
        <li><a href="{{route('about')}}" class="{{ request()->routeIs('about') ? 'active' :''}}">{{__('About Us') }}</a></li>
        <li><a href="{{route('portafolio')}}" class="{{ request()->routeIs('portafolio') ? 'active' :''}}">{{__('Portfolio') }}</a></li>
        <li><a href="{{route('embajadores')}}" class="{{ request()->routeIs('embajadores') ? 'active' :''}}">{{__('Ambassadors') }}</a></li>
        <li><a href="{{route('blog')}}" class="{{ request()->routeIs('blog') ? 'active' :''}}">{{__('Blog') }}</a></li>
        <li>
          <a href="{{ url('locale/es') }}">
             <img src="{{asset('assets/img/spanish.png') }}" alt="" width="25" height="15">
          </a>
          </li>
          <li>
            <a href="{{ url('locale/en') }}">
               <img src="{{asset('assets/img/english.png') }}" alt="" width="25" height="15">
            </a>
            </li>
      </ul>
    </nav>
  
  </div>-->
</header>

