<footer id="footer" class="section-bg ">
  <div class="footer-top">
 
    <div class="container-fluid p-5">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-4 col-12 text-center logotipo">
         <img src="{{asset('assets/img/logo2.png')}}" alt="" width="300" height="150">
       </div>
        <div class="col-md-6 col-sm-12 col-lg-4 col-12 pt-3  redes-sociales" >
           <div class="address">
              <p class="line text-left " style="font-size: 17px;padding-left:13px">
               <i class="fa fa-map-marker"></i> 848 Brickell Ave, Suite 1005. Miami, FL 33131
                <br>
                <i class="fa fa-phone"></i><a href="#" class="text-white">(305)-396-6699</a>
                <br>
                <i class="fa fa-envelope"></i>info@empirexcapital.com
              </p>
           
             <div class="social-links text-left">
               <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter fa-1x" ></i></a>
               <span style="border-left:1px solid #fff"></span>
               <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook fa-1x" ></i></a>
               <span style="border-left:1px solid #fff"></span>
               <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram fa-1x" ></i></a>
               <span style="border-left:1px solid #fff"></span>
               <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin fa-1x"></i></a>
               <span style="border-left:1px solid #fff"></span>
               <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube fa-1x"></i></a>
             </div>
             
             <div class="text-left mt-2 pl-2">
              <a class="text-white" href="{{route('term')}}"  style="font-size: 18px">Terms & Conditions</a>
            </div>
           </div>
           
         </div>
        <div class="col-md-6 col-sm-6 col-lg-3 col-12 text-left pt-3">
             
               <form action="{{route('newsletter')}}" method="POST" class="input-color">
               @csrf
         
               <div class="row">
                 <div class="col-md-11 col-lg-9 p-0  offset-lg-3 ">
                   <h4 class="text-white text-center  font-weight-bold">{{__('Subscribe to Our Newsletter')}}</h4>
                   <div class="form-group">
                     <input type="text" class="form-control" name="name" placeholder="First Name" required>
                   </div>
                   <div class="form-group">
                     <input type="text" class="form-control" name="lastname" placeholder="Last Name" required>
                   </div>
                   <div class="form-group">
                     <input type="text" class="form-control" name="email" placeholder="Email" required>
                   </div>
                 </div>
               </div>
              <button class="btn-footer float-right">Subscribe</button>
             </form> 
      
        </div>
 
        <hr>
 
      </div>
    </div>
    <div class="row">
     <div class="col-md-10 mx-auto termino condiciones">
       <p class="text-white text-justify px-5 ">{{__('There are risks associated with investing in securities. Investing in stocks, bonds, exchange traded funds, mutual funds, and money market funds involve risk of loss.  Loss of principal is possible. Some high-risk investments may use leverage, which will accentuate gains & losses. Foreign investing involves special risks, including a greater volatility and political, economic and currency risks and differences in accounting methods. An investor may get back less than the amount invested. Information on past performance, where given, is not necessarily a guide to future performance. Or: The capital value of units in the fund can fluctuate and the price of units can go down as well as up and is not guaranteed. Therefore, you should never invest any money that you might need for living or for company expenses, expressing this you agreed that this is a risking capital')}}.</p>
   
      </div>
    </div>
 
      <!--<div class="container">
 
       <div class="row">
 
         <div class="col-lg-6">
 
           <div class="row">
 
             <div class="col-sm-6">
 
               <div class="footer-info">
                 <h3>
                 </h3>
                 <p class="text-justify text-white" >
                   {{__('We are a private investment fund from the United States founded in New York in 2012, with offices in Miami, New York, Hong Kong, Medellín and Cali. Empirex Capital is dedicated to managing money to invest in the financial markets')}}
                 </p>
               </div>
 
             </div>
 
             <div class="col-sm-6">
               <div class="footer-links">
                 <ul class=" text-white">
                   <li ><a href="{{route('home')}}" class="text-white">{{__('Home') }}</a></li>
                   <li><a href="{{route('about')}}" class="text-white">{{__('About Us') }}</a></li>
                   <li><a href="{{route('portafolio')}}" class="text-white">{{__('Portfolio') }}</a></li>
                   <li><a href="{{route('embajadores')}}" class="text-white">{{__('Ambassadors') }}</a></li>
                   <li><a href="{{route('blog')}}" class="text-white">{{__('Blog') }}</a></li>
                 </ul>
               </div>
 
               <div class="footer-links text-white">
                 <h4 class="text-white">{{__('Contact Us') }}</h4>
                 <p>
                   848 Brickell Ave <br>
                   Suite 1005<br>
                   Miami, FL 33131 <br>
                   <strong>Phone:</strong>  (305) 396-6699<br>
                   <strong>Email:</strong> info@empirexcapital.com<br>
                 </p>
               </div>
 
               
 
             </div>
 
           </div>
 
         </div>
 
         <div class="col-lg-6">
 
           <div class="form">
 
             <h4 class="text-white">{{__('Contact Us') }}</h4>
     
           <form action="{{route('message.index')}}" method="post" role="form" class="php-email-form">
             @csrf
             <div class="form-group">
               <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
               <div class="validate"></div>
             </div>
             <div class="form-group">
               <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
               <div class="validate"></div>
             </div>
             <div class="form-group">
               <input type="number" class="form-control" name="phone" id="subject" placeholder="phone" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
               <div class="validate"></div>
             </div>
             <div class="form-group">
               <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
               <div class="validate"></div>
             </div>
 
             <div class="mb-3">
               <div class="loading">Loading</div>
               <div class="error-message"></div>
               <div class="sent-message">Your message has been sent. Thank you!</div>
             </div>
 
             <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
           </form>
 
           </div>
 
         </div>
 
       </div>
 
     </div>-->
   </div>


  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header mx-auto">
        <h5 class="modal-title text-center font-weight-bold" style="color:#001838" id="exampleModalLabel">Terms & Conditions</h5>
         
        </button>
      </div>
      <div class="modal-body">
         <p class="text-justify">Empirex Capital LLC. Is the only operator collecting or maintaining personal information through the www.empirexcapital.com website. A member (any visitor or other person    making use of the website) may contact www.empirexcapital.com using the following email address: info@empirexcapital.com.</p>
         
         <h5 style="color:#001838" class="font-weight-bold">Risk Disclaimer</h5>
         <p class="text-justify"> <strong>EMPIREX CAPITAL LLC</strong>  provides investments opportunities in the financial markets, giving the opportunity to any investor to participate in the strategies that Empirex Capital create in its own discretion. Investors subscribed in investments with Empirex Capital LLC make an agreement directly with the company, either natural persons or legally registered companies.Empirex Capital LLC does not have third parties, nor mediators in the process of Investing.</p>

         <p class="text-justify">You must be aware of the risks and be willing to accept the them in order to invest in the stock, binary , futures markets and cryptocurrencies. Do not invest  with any capital you cannot afford to lose. No representation is being made that any account will or is likely to achieve profits or losses similar to those discussed on this website or on any reports. The past performance of any investment is not necessarily indicative of future results.</p>

         <h5 style="color:#001838" class="font-weight-bold">Privacy Policy</h5>

            <p class="text-justify">This privacy notice discloses the privacy practices for <strong>EMPIREX CAPITAL</strong>. This privacy notice applies solely to information collected by this website. It will notify you of the following: <strong>Information Collection, Use, and Sharing</strong>.
             </p>

              <p class="text-justify">We are the sole owners of the information collected on this site. We only have access to and/or collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone. We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request (ie. set up your account). Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p>

           
              <p class="text-justify">   <strong>Your Access to and Control Over Information</strong> <br> You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website.</p>

              <ul>
                <li>See what data we have about you, if any. </li>
                <li>Change/correct any data we have about you.</li>
                <li>Have us delete any data we have about you.</li>
               <li>Express any concern you have about our use of your data.</li>
                
              </ul>
              <h5 style="color:#001838" class="font-weight-bold">Security</h5>

              <p class="text-justify">We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline. We also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>

              <p class="text-justify">
                The owner of <strong>EMPIREX CAPITAL</strong>, their Instagram, Twitter, Telegram, LinkedIn, Pinterest & Facebook Account, their INVESTMENT portafolios  hereby waive any liability whatsoever due to the use of its website. Use of the <strong>EMPIREX CAPITAL</strong>  account, the content and the information is made on the user’s sole liability. The user hereby releases the owner of EMPIREX CAPITAL from any liability for damage caused, through the use of <strong>EMPIREX CAPITAL</strong> and/or its content and/or its various services. The user hereby releases the owner of <strong>EMPIREX CAPITAL</strong>  and any of their social media account, their partners, agents, employees, officers, managers, directors, shareholders, etc. from any liability for losses and/or financial damages and/or personal bodily damages and/or fraud, caused through third parties. All rights reserved. If you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at 646-741-3770 in New York (USA) or via email at info@empirexcapital.com.
              </p>
              <h5 style="color:#001838" class="font-weight-bold"> Data Policy</h5>
             
              <p class="text-justify"><strong>Collection of Information</strong><br>
                1.	The website (www.empirexcapital.com) actively collects information from its members such as first and last name, age, city and state, email address, gender, birth date, and information through signup forms, applications, voluntary surveys, sweepstakes, contests, purchases, and participation in public forums such as bulletin boards. <br>
                2.	The site reserves the right to passively collect information related to site usage such as time spent on site, areas visited, and sites linked to and from. In the future, the site may collect additional information and may install cookies on members’ hard drives. <br>
                3.	The site allows members to make personal information available in public forums such as bulletin boards, to the extent the members choose to disclose such information. <br>
                4.	If a member contacts any employee or affiliated person, a record of that correspondence may be kept. <br>
                5.	The site is prohibited from conditioning a member’s participation in an activity on the member is disclosing more personal information than is reasonably necessary to participate in such activity. <br>
                
              </p>


              <h5 style="color:#001838" class="font-weight-bold"> Usage of Information</h5>
              <p class="text-justify">
                1.	Personal information is used for record keeping, site management, activities on the site, the fulfillment of requested transactions, and marketing only to members who have consented to such marketing. This may be in the form of email, text messaging, or direct mail amongst others. <br>
                2.	Personal information collected from members is not disclosed to third parties, except companies with which the web site is affiliated by common ownership. The site may 6 retain third parties to review certain personal information to advise it on demographic and marketing related issues, typically on an aggregated basis, and only after such third parties have agreed to maintain the confidentiality, security, and integrity of any personal information obtained.

              </p>

      </div>
     
    </div>
  </div>
</div>

     
  <!-- <div class="container">
     <div class="copyright">
       &copy; Copyright <strong>Empirex Capital</strong>.{{__(' All Rights Reserved ')}} 2020
     </div>
     <div class="credits">
       {{__('Designed by')}} <a style="color:#001838">{{__('team Empirex capital')}}</a>
     </div>
   </div>-->
 </footer>