<div class="container">
  <div class="row">
    <div class="col-md-8 my-5 mx-auto">
      <h2 style="color:#001838">{{__('Comments')}}</h2>
      <div class="card">
          <div class="card-body">
            <form action="{{route('comment.store')}}" method="post">
              @csrf
              <input type="hidden" class="form-control" name="blog_name" value="{{$blog_name}}">

              <div class="form-group">
                 <input type="text" class="form-control" name="name" placeholder="your name">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="email" placeholder="your Email">
             </div>
             <div class="form-group">
              <textarea  id="" class="form-control" name="comment"  rows="5" placeholder="your message"></textarea>
              </div>
              <button class="primary">Send comment</button>
            </form>
          </div>
      </div>
    </div>
  </div>
</div>



