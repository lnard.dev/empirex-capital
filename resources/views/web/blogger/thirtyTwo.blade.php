@php($blog_name="Hong Kong Companies have Committed $ 1.3 Billion in just Seven Months")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/28.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__("Hong Kong Companies have Committed $ 1.3 Billion in just Seven Months")}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Hong Kong firms have been linked to two conglomerates used by the junta. Post-coup, only some of those firms have had a rethink.Last June, when the Hong Kong Trade Development Council spotlighted business opportunities in Myanmar in an article on its website, there was no shortage of Hong Kong businesses on hand to express their bullish sentiment. Hong Kong had recently emerged as the Southeast Asian country’s largest source of foreign investment, with businesses committing US$1.3 billion in the first seven months of the financial year 2019/20. In a survey by the Myanmar Hong Kong Chamber of Commerce, 77 per cent of respondents said they planned to maintain their presence in Myanmar over the next year, despite the challenges of Covid-19. Over one-fifth of the respondents, which included more than half of the representatives of the chamber’s 64 member companies, said they hoped to expand")}}</p>

             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Only a few months later, firms in Hong Kong and elsewhere are facing much thornier, ethical questions than the scope for expansion. The Myanmar junta that seized power from the democratically elected government of Aung San Suu Kyi on February 1 has launched increasingly violent crackdowns on anti-coup protesters, fuelling calls for governments and businesses worldwide to sanction military leaders and cut ties with military-backed commercial outfits")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/Hong-Kong-Companies-have-Committed-Billion-in-just-Seven-Months&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>


       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/Hong-Kong-Companies-have-Committed-Billion-in-just-Seven-Months" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>

       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/Hong-Kong-Companies-have-Committed-Billion-in-just-Seven-Months" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
