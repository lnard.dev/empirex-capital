@php($blog_name="Biden Preps $3T Stimulus, Bitcoin Could Be Set To Erupt'")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/12.jpg')}}" alt="" height="800" class="img-fluid mt-5">
         </div>
        </section> 

    <main id="main">

    {{$blog_id=2}}

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Biden Preps $3T Stimulus, Bitcoin Could Be Set To Erupt')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('The incoming Biden administration’s plan to flood the U.S. economy with trillions of dollars could ignite the next leg of the Bitcoin (BTC) bull market, as more investors seek refuge from a crumbling United States dollar. Axios, an Arlington-based news outlet, reported Thursday that Joe Biden has asked Congress to provide Americans with $2,000 in stimulus payments to help offset the economic devastation of Covid-19. The incoming president has also proposed a $3 trillion tax and infrastructure package as part of his “Build Back Better” program. Biden doubled down on his call for more direct relief to Americans following Friday’s disappointing jobs report showing a loss of 140,000 positions in December')}}</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('If 2020 is anything to go by, the new tidal wave of stimulus could be another catalyst for Bitcoin as more money floods the market and makes its way into asset prices.Even Donald Trump, a Republican, was no stranger to vast government outlays. Under his leadership, the United States passed a historic $2 trillion stimulus bill in March. Trump also signed a $900 billion relief package last month that would pave the way for $600 stimulus checks.The federal government’s inflation-boosting policies have coincided with record intervention from the Federal Reserve, which deployed trillions of dollars in 2020 to combat a liquidity crisis and keep overnight rates under control.While these policies provided strong support for risk assets—a category that has included Bitcoin in the past—the emerging narrative surrounding BTC is that it is inflation coverage')}}.</p>
             
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>

   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/biden-preps-$3T-stimulus-bitcoin-could-be-bet-to-erupt&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
       <a href="https://www.facebook.com/sharer/sharer.php?u=https://empirexcapital.com/biden-preps-$3T-stimulus-bitcoin-could-be-bet-to-erupt" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/biden-preps-$3T-stimulus-bitcoin-could-be-bet-to-erupt&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
