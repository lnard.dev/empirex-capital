@php($blog_name="Robinhood Users Still Have a Purchase Limit for One Gamestop Share")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5 pt-5">
           <img src="{{asset('assets/img/portfolio/10.jfif')}}" alt="" height="800" width="700" class="img-fluid mt-5">
         </div>
        </section> 

    <main id="main">


    

    <section id="" class="portfolio section-bg mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__("Robinhood Users Still Have a Purchase Limit for One Gamestop Share")}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('The Robinhood trading app has been narrowing down the list of restricted stocks on its platform, but still maintains significant restrictions on GameStop (GME) shares.According to its website, Robinhood has lowered its list of restrictions to eight different stocks, including GME, AMC, BlackBerry, Express, Genius Brands International, Koss, Naked Brand Group and Nokia. Robinhood previously limited trading to up to 50 stocks on January 29, CNBC reported')}}.</p>
     
              <p class="text-justify" style="font-size: 20px; color:#001838" >{{__("Robinhood has been struggling to maintain its trading services, while the Reddit r / Wallstreetbets community pushed stocks like GME to new all-time highs. On January 28, Robinhood suspended GME purchases after shares soared 1,400% from about $ 20 on January 12. On January 29, the company also temporarily disabled instant deposits for cryptocurrency purchases, citing extraordinary market conditions. The US Securities and Exchange Commission subsequently hinted that it was investigating the Robinhood platform, citing concerns about extreme volatility in the trading prices of certain stocks")}}.</p>


            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>
   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/robinhood-users-still-have-a-purchase-limit&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/robinhood-users-still-have-a-purchase-limit" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/robinhood-users-still-have-a-purchase-limit&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>
   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
