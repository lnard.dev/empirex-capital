@php($blog_name="Winklevoss Brothers Top Forbes Bitcoin Billionaires List")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/14.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Winklevoss Brothers Top Forbes Bitcoin Billionaires List')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Major business magazine Forbes has featured Gemini co-founders Cameron and Tyler Winklevoss as the richest Bitcoin (BTC) billionaires. Compiled by Forbes crypto-focused reporter Michael del Castillo, the ranking provides a list of Bitcoin investors that won the most from Bitcoin’s massive surge up to $42,000 in early January. The ranking includes three Bitcoin investors with an estimated crypto net worth above $1 billion, including the Winklevoss brothers, major industry investor Tim Draper and Matthew Roszak, chairman and co-founder of blockchain firm Bloq. The twins topped the list with an estimated cryptocurrency net worth of around $1.4 billion apiece. Roszak and Draper follow, with net worths estimated at $1.2 billion and $1.1 billion, respectively")}}</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("The list also includes three additional crypto investors: MicroStrategy CEO Michael Saylor, Galaxy Digital founder and CEO Mike Novogratz and Ethereum co-founder Vitalik Buterin. According to Forbes data, Saylor’s crypto fortune amounts to $600 million, while Novogratz and Buterin’s crypto net worths stand at $478 million and $360 million, respectively")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/Winklevoss-Brothers-Top-Forbes-Bitcoin-Billionaires-List" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>

   <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/Winklevoss-Brothers-Top-Forbes-Bitcoin-Billionaires-List" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>


       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/Winklevoss-Brothers-Top-Forbes-Bitcoin-Billionaires-List&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
