@php($blog_name="The European Stock Exchanges Remain Without Fuelle: Ibex 35 Trades Down After Colliding with 8,300 Points")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/9.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('The European Stock Exchanges Remain Without Fuelle: Ibex 35 Trades Down After Colliding with 8,300 Points')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Europe stock markets have faced the session with timid progress. While major indices began the day with moderate ascents, these took little time to minimize. Even the Ibex 35 turned down after touching the psychological level of the 8,300 points and ends the day with 0.4% descents. And that, unlike yesterday, the equities of the Old Continent have had the reference of Wall Street, whose selective ones record a positive day across the Atlantic.Shopping has been widespread in some Asian squares this morning. Tokyo stock exchanges (Nikkei: +1.39%) and Seoul (Kospi: +2.61%) they have bounced strongly today and have therefore approached their respective and recent record levels.Wall Street moves in the same (upward) direction, marking profits hovering around 0.5% after the 'rest' that the North American market took yesterday")}}</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("However, Europe's selectives continue to be without bellows, waiting for some catalyst to get them back to the rally with which they started the year. Although if technical analysis is addressed, this consolidation does not call into question the chances of continuing to see higher hikes in future dates. As Joan Cabrero, Advisor at Ecotrader, believes that as long as the EuroStoxx 50 (the benchmark on the continent) does not lose its support at 3,524 points, the current phase is a simple pause before further rises. The EuroStoxx now holds over 3,600 integers (intraday minimum: 3,601.4. According to this expert, it has a path ahead of it to 3,745, i.e. 4%")}}</p>
               <div class="row">
                
                  <div class="col-md-12">
                    <p class="text-justify" style="font-size: 20px; color:#001838">{{__("The biotechnology Grifols highlights this day in Spanish parquet. His shares are recorded rises of around 4%. Yesterday more than 2% grew after the company announced that it has started a clinical trial with a new drug that could provide immediate immunity against covid-19. Colonial, Acciona and a 'heavyweight' like Telefónica accompany him at the top of the table with ascents of about 2%. However, other 'big values' push down. BBVA ranks among the highest sales by leaving about two percentage points. The other big bank, Santander, yields 1%. Most Ibex entities are placed on negative ground.Inditex and Iberdrola also record their respective drops of about 1% and 0.5%. However, steelmaker ArcelorMittal and socimi Merlin Properties are among the worst firms of the day with drops of 3% to 1.5%, respectively")}}</p>
                  </div>
               </div>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>

   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/the-european-stock-exchanges-remain-without-fuelle&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/the-european-stock-exchanges-remain-without-fuelle" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/the-european-stock-exchanges-remain-without-fuelle&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>
   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
