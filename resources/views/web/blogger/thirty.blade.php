@php($blog_name="IBEX Exceeds 8,400 Points While Markets Weigh the Effect of Biden's Stimulus")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/26.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__("IBEX Exceeds 8,400 Points While Markets Weigh the Effect of Biden's Stimulus")}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Markets remained uneasy about the uncertainty of whether President Joe Biden's $1.9 trillion package will contribute to a faster recovery from the impact of COVID-19, or whether it will instead lead to overheating of the world's largest economy resulting in galloping inflation that burdens business performance. Treasury Secretary Janet Yellen said Monday that the aid package will provide enough resources to drive a very strong economic recovery, noting nonetheless that there are tools to deal with inflation. Most analysts expect an acceleration in inflation, fueled in part by the latest rise in oil prices, with the Brent briefly surpassing $70 for the first time since January 2020, following a series of attacks on Saudi Arabia's facilities and the US Senate's green light on aid, before starting to fall during the session")}}</p>

             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Markets remained uneasy about the uncertainty of whether President Joe Biden's $1.9 trillion package will contribute to a faster recovery from the impact of COVID-19, or whether it will instead lead to overheating of the world's largest economy resulting in galloping inflation that burdens business performance. Treasury Secretary Janet Yellen said Monday that the aid package will provide enough resources to drive a very strong economic recovery, noting nonetheless that there are tools to deal with inflation. Most analysts expect an acceleration in inflation, fueled in part by the latest rise in oil prices, with the Brent briefly surpassing $70 for the first time since January 2020, following a series of attacks on Saudi Arabia's facilities and the US Senate's green light on aid, before starting to fall during the session")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/IBEX-Exceeds-8400-Points-While-Markets-Weigh-the-Effect-of-Biden-Stimulus&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>


       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/IBEX-Exceeds-8400-Points-While-Markets-Weigh-the-Effect-of-Biden-Stimulus" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>

       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/IBEX-Exceeds-8400-Points-While-Markets-Weigh-the-Effect-of-Biden-Stimulus" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
