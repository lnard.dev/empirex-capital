@php($blog_name="Dogecoin: The Cryptocurrency that Became the New Target of Investors")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/17.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Dogecoin: The Cryptocurrency that Became the New Target of Investors')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Social media came to Wall Street to stay. The GameStop phenomenon seems not to be an isolated fact, because after the action of the video game store fell more than 60%, the liners go for more.In Reddit's trading-focusedWallstreetbets forum, another massive asset purchase broke out. This time it was Dogecoin, also known as meme Doge, a cryptocurrency that began in 2013 as a mockery of the crypto fever that was once driven by Bitcoin. Currently on the social network there are 869,000 people talking about that cryptoactive.The digital currency, which is priced at US$0.05 per unit, is valued at US$6.1 million and has recorded an increase of 46.48% according to the Coin Market Cap portal")}}</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("In addition to the discussion on Reddit, another social media-related factor also influenced the rise of Dogecoin. Tesla founder Elon Musk posted several positive comments about the cryptocurrency on his official Twitter account. No ups and downs, just Doge, Musk wrote, also published a meme of the Disney film The Lion King, which shows the monkey shaman Rafiki holding the simba puppy who is the billionaire with the Dogecoin logo of a Shiba Inu, breed of dogs that has been related from the beginning to the asset, in his hands. On the billionaire's publications, Edward Moya, a market analyst in Oanda, said that with this, Dogecoin rose and caused a wide uptick in all major cryptocurrencies. While Bitcoin initially fell after Musk removed Bitcoin from its biography, although the most popular cryptocurrency appears to be ready for another race to the $40,000 level.However, the Bloomberg news agency reported that earlier this week, Musk said in the Clubhouse social audio app that he does not have a solid opinion on other cryptocurrencies and that his comments on Dogecoin are jokes, adding that 'the most ironic result would be for Dogecoin to become Earth's currency in the future.'This infers that Tesla's CEO is also part of the game of many liners taking some stock to quickly value them and sell them when they fall again. The Reddit forums are full of memes that suggest that the cryptocurrency trend has already passed and that selling is the right thing to do, for traders on that social network, buying Dogecoin is no longer fashionable")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/the-cryptocurrency-that-became-the-new-target-of-investors&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
 
      <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/the-cryptocurrency-that-became-the-new-target-of-investors" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
 
 
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/the-cryptocurrency-that-became-the-new-target-of-investors" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
