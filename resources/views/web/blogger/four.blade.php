@php($blog_name="Bitcoin Sets New Record to Top $ 23,000")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5 pt-5">
           <img src="{{asset('assets/img/portfolio/1.jpg')}}" alt="" height="800" width="700" class="img-fluid mt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio section-bg mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Bitcoin Sets New Record to Top $ 23,000')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('According to data from CoinMarketCap, at 9:21 a.m. (GMT) bitcoin reached $ 23,535.36, an increase of $ 1,433.61, or 6.49%, since the opening of the session. And it has been a historic week since this Wednesday, December 16, the price of bitcoin surpassed $ 20,000 for the first time and then set another record of more than $ 22,000, which presented an unprecedented increase in value')}}.</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('The fact: Bitcoin accounts for about 65.9% of the global cryptocurrency market. As of December 16, the increase in the price of bitcoin has already reached 20%, independent expert Mansur Guséynov told RIA Novosti. If there is no euphoria in the market, of course the price should stop now and begin a consolidation, he valued')}}.</p>
               <h4>{{__('The Perspective')}}</h4>
              <p class="text-justify" style="font-size: 20px; color:#001838" >{{__('A new cap was finally set and attention is moving to the next round figure of $ 30,000, said Antoni Trenchev, co-founder and managing partner of cryptocurrency lender Nexo, as quoted by Bloomberg. It is the beginning of a new chapter for bitcoin, he added')}}.</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>

   <div class="container">
    <div class=" text-left">
     <span style="color:#001838; font-size:20px">Share</span>
      <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/dow-jones-falls-more-than-200-points-in-volatile-trading&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
      <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/dow-jones-falls-more-than-200-points-in-volatile-trading" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
      <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/dow-jones-falls-more-than-200-points-in-volatile-trading&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
   </div>
  </div>
   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
