@php($blog_name = "Optimism of the Joe Biden presidency for Bitcoin")


@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/11.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">
  
    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Optimism of the Joe Biden presidency for Bitcoin')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('After repeating “we won this election” some two hundred times and kicking to the point that Big Tech decided to eliminate his social networks, Donald Trump seems to be in his last days in the west wing of the United States White House.Coupled with the victory of the Democratic Party in the Georgia Senate elections , which makes the progressive party have the majority of the Senate, the “Blue Wave” is complete, leaving them a majority in Congress, the Senate and the Presidency. Biden and company will have no brakes to come up with the largest stimulus package the American economy has ever seen')}}</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('If there is something that can excite a Bitcoiner at this time (in addition to the price of Bitcoin that has given so much joy in recent months) it is to hear of the stimulus packages within the large economies.And no, make no mistake, it is not that the average Bitcoiner is happy waiting for his juicy stimulus check to arrive, it is that he simply rejoices to see how the largest States fall into the practices that they have been denouncing for years, the delinquent”printed paper stamped with numbers.Particularly for the Latin American Bitcoiner, the printing of “inorganic money” is known first-hand, it has been experienced first-hand and, most importantly, its effects are also known, inflation. Just that monster that we constantly find ourselves saying that Bitcoin is destined to defeat, in order to pose as the “ultimate refuge of value”.It is precisely in the middle of this scenario that we find the most popular and widespread narrative in recent months: The arrival of Joe Biden to the presidency of the United States will take the US dollar to the ground and this will lead to Bitcoin rising (yes, that go even higher). However, I think this surreal optimism could be stifled with harsh reality sooner rather than later')}}</p>
          
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>

   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/optimism-of-the-Joe-Biden-presidency-for-bitcoin&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/optimism-of-the-Joe-Biden-presidency-for-bitcoin" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>

       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/optimism-of-the-Joe-Biden-presidency-for-bitcoin&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>

   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
