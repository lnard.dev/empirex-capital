@php($blog_name="Swiss Crypto ETP Issuer Passes $1B Assets Under Management")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/20.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
            <h2 style="color:#001838">{{__("Swiss Crypto ETP Issuer Passes $1B Assets Under Management")}}</h2>
            <p class="text-justify" style="font-size: 20px; color:#001838">{{__("21Shares, a Switzerland-based provider of cryptocurrency exchange-traded products, or ETPs, has recorded a 100% growth in assets under management over the past two weeks The company announced Monday that 21Shares has passed the $1 billion mark in AUM across its diversified 12 crypto asset ETPs. 21Shares CEO Hany Rashwan said that the company has seen a meteoric rise in its crypto ETP products recently, doubling AUM in less than two weeks. The company previously announced that 21Shares broke $500 million in AUM on Feb. 8.According to the announcement, the massive surge in 21Shares’ crypto ETP business is mainly attributed to the rapid adoption of crypto by institutional investors as well as the products’ availability on regulated European exchanges 21Shares, un proveedor con sede en Suiza de productos cotizados en bolsa de criptodivisas, o ETP, ha registrado un crecimiento del 100% en los activos bajo gestión durante las últimas dos semanas. La compañía anunció el lunes que 21Shares ha superado la marca de $1 mil millones en AUM en sus 12 ETP de criptoactivos diversificados. El CEO de 21Shares, HanyRashwan, dijo que la compañía ha visto un aumento meteórico en sus productos criptográficos ETP recientemente, duplicando AUM en menos de dos semanas. La compañía anunció previamente que 21Shares rompió $500 millones en AUM el 8 de febrero. Según el anuncio, el aumento masivo en el negocio de cripto ETP de 21Shares se atribuye principalmente a la rápida adopción de cripto por parte de los inversores institucionales, así como a la disponibilidad de los productos en las bolsas europeas reguladas")}}</p>

             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("According to a product break down chart in the announcement, the biggest part of 21Shares’ total AUM came from 21Shares Binance BNB ETP (ABNB) — an ETP tracking the investment results of Binance Coin (BNB). BNB’s dominant share in the total AUM comes amid Binance Coin's recent surge, which saw it become the world’s third-largest cryptocurrency on Feb. 19. As of Feb. 18, ABNB was apparently still behind 21Shares Bitcoin ETP, or ABTC, with AUM amounting to around $214 milion, while ABTC AUM stood at $272 million, according to official records on the 21Shares website. Formerly branded as Amun AG, 21Shares is known for launching the world’s first multi-crypto ETP, listing the product SIX Swiss Exchange in November 2018. In just over two years after launching its first crypto ETP, 21Shares has been actively diversifying its crypto ETP offering, witnessing a 200-fold increase in AUM. In early February, 21Shares launched the world’s first ETP based on Polkadot (DOT)")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/Swiss-Crypto-ETP-Issuer-Passes-%25241B-Assets-Under-Management&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
    
     <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/Swiss-Crypto-ETP-Issuer-Passes-%241B-Assets-Under-Management" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
 
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/Swiss-Crypto-ETP-Issuer-Passes-%25241B-Assets-Under-Management" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
