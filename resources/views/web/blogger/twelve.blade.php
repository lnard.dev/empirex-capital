@php($blog_name="Young Americans will buy Bitcoin with their stimulus checks")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/3.png')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Young Americans will buy Bitcoin with their stimulus checks')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Amid a long battle over whether or not United States citizens will get their $2,000 stimulus checks to help them to weather the economic repercussions of the COVID-19 pandemic, Mike Novogratz has weighed in on what the news could mean for the stock and crypto markets.The CEO of Galaxy Digital spoke to CNBC Squawk Box amid the surreal image of a Wall Street apparently unfazed by the commotion in the Capitol yesterday. As a Fox News caption had it in real-time yesterday, Markets rally as protestors disrupt electoral college vote')}}</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('A further round of stimulus checks at $2,000 each, recently approved by the House of Representatives, could, from Novogratzs perspective, be further good news for the markets. With the Senate now flipped after the runoff election in Georgia, he noted that: A lot of that [stimulus] will find it is way into the markets. Certainly, when it comes into young people  hands, they are going right to their Robinhood accounts. One of the most unique things last time was seeing how many people bought Bitcoin with the exact amount of stimulus. Boom, boom')}}</p>
               <div class="row">
                
                  <div class="col-md-12">
                    <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Bitcoin — the only other cryptocurrency on the top 100 list — has also been on a tear through the rankings, surpassing Visa, Berkshire Hathaway, Alibaba and most recently Tencent, to move up to the #10 spot. With an estimated market cap of $12.19 trillion, gold sits atop the list by a margin of $10 trillion; its next closest competitor being Apple, at #2')}}</p>
                  </div>
               </div>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>

   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/young-americans-will-buy-bitcoin-with-their-stimulus-checks&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/young-americans-will-buy-bitcoin-with-their-stimulus-checks" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/young-americans-will-buy-bitcoin-with-their-stimulus-checks&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>
   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
