@php($blog_name="Investing in Times of Crisis")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/app2.jpg')}}" alt="" class="img-fluid mt-5">
         </div>
        </section> 
    <main id="main">

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Investing in Times of Crisis')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">Today we live in a time of uncertainty due to the COVID-19 pandemic, which has changed our daily lives and caused the suspension of business activities. It is not surprising that due to the current situation, many investors are wondering if it is a good idea to continue with their investment projects or if they need to take some time off.</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">The first thing you have to ask yourself is: Why should I invest my money in times of crisis? Many people have the wrong idea that investing in times of economic crisis is not a good idea because it is very risky and you could lose your money or it could take a long time to obtain a return on investment.</p>
              
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>

   <div class="container">
    <div class=" text-left">
     <span style="color:#001838; font-size:20px">Share</span>
      <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/investing-in-times-crisis&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
      <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/investing-in-times-crisis" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
      <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/investing-in-times-crisis&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
   </div>
  </div>
   <!-- End #main -->
   @include('partials.comment')  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
