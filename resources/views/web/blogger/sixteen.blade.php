@php($blog_name="Dubai Financial Regulator Working on Regulations for Cryptocurrencies")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5 pt-5">
           <img src="{{asset('assets/img/portfolio/10.jpg')}}" alt="" height="800" width="700" class="img-fluid mt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio section-bg mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__("Dubai Financial Regulator Working on Regulations for Cryptocurrencies")}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('The Dubai Financial Services Authority, the financial regulatory agency for the special economic zone, the Dubai International Financial Centre, is looking to enhance local cryptocurrency-related regulations.The DFSA is planning to introduce a regulatory framework for diverse digital assets as part of its 2021–2022 business plan released on Jan. 18')}}.</p>
     
              <p class="text-justify" style="font-size: 20px; color:#001838" >{{__("According to the DFSA, the upcoming crypto framework will further expand the DFSA’s regulation of digital asset issuers and associated trading platforms. The framework will include a number of digital asset types like tokenized securities and cryptocurrencies like Bitcoin (BTC): We will build upon recent achievements in this space over the business planning period through developing a regulatory regime for digital assets (such as tokenised securities and crypto-currencies), having already implemented regulations supporting various innovative business models")}}.</p>

              <p class="text-justify" style="font-size: 20px; color:#001838" >{{__("According to a report by local news agency The National, the DFSA plans to publish two consultation papers seeking feedback on the upcoming rules. Peter Smith, the DFSA’s head of strategy, policy and risk, said that the two consultations will be released in the first two quarters of 2021. “We will look to regulate a wide range of digital assets, including security tokens, utility tokens, the various types of exchange tokens, such as cryptocurrencies and the firms that provide relevant services in these markets,” Smith noted")}}.</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>

   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/dubai-financial-regulator-working-on-regulations-for-cryptocurrencies&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/dubai-financial-regulator-working-on-regulations-for-cryptocurrencies" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/dubai-financial-regulator-working-on-regulations-for-cryptocurrencies&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>
   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
