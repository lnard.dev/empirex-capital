@php($blog_name="How Crypto is Going to Shake Up the World of Mixed Martial Arts")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/22.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__("How Crypto is Going to Shake Up the World of Mixed Martial Arts")}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("A team of mixed martial arts and marketing experts have created a fan-centric platform that’s designed to bring the sport into the digital age. MMAON says mixed martial arts is considered to be the world’s fastest-growing sport, with the likes of Conor McGregor propelling the discipline to an international stage. The online platform is designed to connect stakeholders of this thrilling sport, including organizers, athletes, sponsors and fans. With COVID-19 affecting MMA events globally, causing fights to be postponed and fans to stay at home, digitization has never been more important.TomažAmbrožič, a renowned sports marketing expert, described the creation of the MMA Digital Hub as “a very innovative approach.” The platform is set to adopt a similar approach to that of IMDb, which has now become an undisputed authority in the world of film")}}</p>

             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("MMAON says that it also wants to help fighters make a comfortable living. Despite the fact that MMA is one of the most demanding and difficult sports in the world, many stars currently have low earning potential and only a handful earn a substantial amount. Through this crypto-focused platform, fighters will be able to receive tips from loyal fans and athletes will be able to offer branded merchandise and sports equipment to generate revenue. A broader range of sponsorship opportunities will also be offered, something that contracts with professional sporting bodies often prohibit. Frank Mir, a former UFC heavyweight world champion who serves as a consultant on the project, has said he wished that platforms such as MMAON existed during the early stages of his career, describing it as a “phenomenal concept” that will allow young fighters to be seen by everyone. He added: “Sometimes you get a little envious when you think about what it would be like to have all these things when I was fighting in my younger years… How much more could I earn? How could I become more popular? It must be easier for every generation. One of MMAON’s top priorities will involve encouraging as many world-famous fighters as possible to become part of the revolution")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/soccer-fan-tokens-on-the-march-as-poland-biggest-club-adopts-crypto&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>


       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/how-crypto-is-going-to-shake-up-the-world-of-mixed-martial-arts" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>

       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/soccer-fan-tokens-on-the-march-as-poland-biggest-club-adopts-crypto" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
