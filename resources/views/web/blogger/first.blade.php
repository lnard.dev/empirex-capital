@php($blog_name="Bitcoin Price Eyes 30% Correction")
@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/5.jpg')}}" alt="" height="800" class="img-fluid mt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio section-bg mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Bitcoin Price Eyes 30% Correction')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Bitcoin (BTC) heads into a new week after another weekend of major volatility — but this time, the way was down, not up. As the asset loses $7,000, the most since the “vertical” price rises began, presents four things that could keep it moving in the coming days')}}.</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Top on the list of topics among Bitcoiners will be the cryptocurrency’s sudden fall over Saturday and Sunday. From highs of near $42,000, BTC/USD faced a sell-off out of hours, with bears driving down the pair to current local lows of $32,300.
              The drop is the largest this year and most significant since the coronavirus pandemic caused a cross-asset crash in March 2020, but it was widely predicted by analysts, who argued that Bitcoin had become overextended')}}.</p>

              <p class="text-justify" style="font-size: 20px; color:#001838" >{{__('In fact, a 23% drop is nothing new for long-term Bitcoin hodlers, and the lack of more significant losses suggests that even above $30,000, buyer support remains strong.These and upcoming dips are the opportunities you where looking for during the #FOMO feelings you had at $40,000. Use them,” Cointelegraph Markets analyst Michaël van de Poppe meanwhile continued. As of publication, Bitcoin was already rebounding, with $35,000 swiftly following the lows. This was not enough for institutional Bitcoin-buyer Guggenheim, however, with chief investment office Scott Minerd suddenly signalling that the fund would sell some of its BTC position. Bitcoin parabolic rise is unsustainable in the near term Vulnerable to a setback, he claimed on Monday')}}.</p>
              <p class="text-justify" style="font-size: 20px; color:#001838">{{__('In the United States, a potential bull sign for Bitcoin in the form of a massive $3 trillion stimulus program from the incoming Biden administration is being masked by a recent rebound in the strength of the dollar. A classic inverse correlation for Bitcoin, the U.S. dollar currency index (DXY) continued its gains over recent days, heading back above the 90 mark after hitting its lowest levels since March 2018. Last year often saw dollar-weakening give Bitcoin a boost, at a time when other price relationships were steadily breaking down')}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>
   <!-- End #main -->
   <div class="container">
    <div class=" text-left">
     <span style="color:#001838; font-size:20px">Share</span>
      <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/bitcoin-price-eyes-30%-correction&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
      <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/bitcoin-price-eyes-30%-correction" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
      <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/bitcoin-price-eyes-30%-correction&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
   </div>
  </div>
  
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
