@php($blog_name = "Ethereum into list of top 100 assets in the world by market cap")


@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/2.png')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Ethereum into list of top 100 assets in the world by market cap')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('A 13% rise in the price of ETH over the past 24 hours has propelled Ethereum onto a list of the top 100 traded assets by market cap for the first time, as measured by CompaniesMarketCap.com, Ethereum marched past 12 other entries on the list today and is currently positioned in the #88 spot, between Bristol-Myers Squibb at #89 and Anheuser-Busch at #87')}}</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Up 62% in 2021 alone, the price of ETH crossed $1,200 today for the first time since Jan. 2018 and appears poised to overtake its all-time high set Jan. 13 of that year. This year’s gains translate into a $53 billion increase in Ethereum’s market cap, enough for it to overtake the market cap of several household conglomerates, including Wells Fargo, AstraZeneca, Philip Morris, Morgan Stanley, and Citigroup')}}</p>
               <div class="row">
                
                  <div class="col-md-12">
                    <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Bitcoin — the only other cryptocurrency on the top 100 list — has also been on a tear through the rankings, surpassing Visa, Berkshire Hathaway, Alibaba and most recently Tencent, to move up to the #10 spot. With an estimated market cap of $12.19 trillion, gold sits atop the list by a margin of $10 trillion; its next closest competitor being Apple, at #2')}}</p>
                  </div>
               </div>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>
   <div class="container">
   <div class=" text-left">
    <span style="color:#001838; font-size:20px">Share</span>
     <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/Ethereum-into-list-of-top-100-assets-in-the-world&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
     <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/Ethereum-into-list-of-top-100-assets-in-the-world" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
     <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/Ethereum-into-list-of-top-100-assets-in-the-world&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
  </div>
 </div>
   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
