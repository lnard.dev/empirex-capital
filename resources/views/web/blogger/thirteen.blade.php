@php($blog_name="Fidelity Doubles Down on Hong Kong Crypto Operator")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/6.jpg')}}" alt="" class="img-fluid mt-5">
         </div>
        </section> 
    <main id="main">

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Fidelity Doubles Down on Hong Kong Crypto Operator')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">
              {{__('Fidelity Investments, one of the world’s largest asset managers, has poured more capital into a Hong Kong-based cryptocurrency operator — offering yet another bullish indicator for the evolution of digital-asset markets worldwide. An exchange filing obtained by Singapore-based Business Times shows that Fidelity acquired a 6.29% stake in BC Technology Group after investing $6.71 million in the company. Business Times indicated that Fidelity had increased its exposure to the company, though did not specify the initia investment amount')}}
              .</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">
              {{__('The investment was announced shortly after BC Technology Group disclosed that it had reached an agreement to raise HK$697 million ($89.9 million) in a “top up share placement')}}
            .</p>

            <p class="text-justify" style="font-size: 20px; color:#001838">
              {{__('BC Technology Group is the operator of OSL, a popular cryptocurrency exchange that recently obtained a coveted license from Hong Kong’s Securities and Futures Commission, or SFC. A BC Group press release dated Dec. 15, 2020 claims that OSL is the world’s “first SFC-licensed, listed, digital asset wallet-insured, Big-4 audited digital asset trading platform for institutions and professional investors.Fidelity has made a number of strategic investments in the cryptocurrency market, including expanding its custody services in Asia through a partnership with Stack Funds, a Singapore-based crypto startup. At the end of 2019, Fidelity’s digital-asset unit established an official entity in the United Kingdom to serve institutional investors in Europe')}}
            .</p>
              
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>
   <div class="container">
   <div class=" text-left">
    <span style="color:#001838; font-size:20px">Share</span>
     <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/fidelity-doubles-down-on-hong-kong-crypto-operator&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
     <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/fidelity-doubles-down-on-hong-kong-crypto-operator" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
     <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/fidelity-doubles-down-on-hong-kong-crypto-operator&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
  </div>
 </div>
   <!-- End #main -->
   @include('partials.comment')  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
