@php($blog_name="Tesla, Bitcoin and the Crypto Space: The show Musk go on")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/21.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
            <h2 style="color:#001838">{{__("Tesla, Bitcoin and the Crypto Space: The show Musk go on")}}</h2>
            <p class="text-justify" style="font-size: 20px; color:#001838">{{__("While 2020 was a significant year for Bitcoin (BTC), the just-begun year of 2021 is already full of great surprises. On Feb. 8, Bitcoin’s price jumped by almost $3,000 in minutes, then it surged 20% in 24 hours — all due to the news of Elon Musk’s Tesla allocating $1.5 billion of its balance sheet to BTC.  The same week, the oldest bank in the United States, Bank of New York Mellon, announced plans to hold, transfer and issue Bitcoin. Mastercard is also planning to support cryptocurrencies in 2021 for its almost 1 billion users. And SEC Commissioner Hester Peirce, commonly referred to as “Crypto Mom” in the crypto community, even underlined the urgent necessity for regulatory clarity due to the recent events in the space")}}</p>

             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Alex Tapscott, managing director at Ninepoint Partners: Tesla’s purchase of Bitcoin no doubt sent a shockwave through the corner offices of every chief financial officer and corporate treasurer in America and beyond. One of Bitcoin’s many benefits is that it acts like digital gold, diversifying corporate holdings and lowering currency risk. Every financial executive ought to be sharpening their pencils to understand if and ultimately when they should be buying it. While Tesla is not the first public company to buy Bitcoin for its treasury, it is by far the most important. How poetic it is that Elon Musk, a space entrepreneur, pushed Bitcoin to its escape velocity in corporate America!Da Hongfei, founder of Neo and founder and CEO of Onchain: This is a very promising sign for mainstream interest and its increasing willingness to embrace blockchain. Moreover, it affirms that blockchain and Bitcoin are here to stay for the future.Throughout 2020, increasingly more financial institutions invested in Bitcoin, and I am confident that blockchain adoption is accelerating as the global financial paradigm shifts to fully embrace digitization and decentralization. Moving forward, we must continue pushing for effective standards across the industry as well as greater integration to fully deliver on blockchain’s game-changing potential")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/Tesla-Bitcoin-and-the-Crypto-Space&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/Tesla-Bitcoin-and-the-Crypto-Space" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/Tesla-Bitcoin-and-the-Crypto-Space" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
