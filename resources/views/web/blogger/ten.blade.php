@php($blog_name="Bitcoin price hits $40,000 less than three weeks after shattering $20K")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/1.png')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__('Bitcoin price hits $40,000 less than three weeks after shattering $20K')}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Bitcoin (BTC) hit $40,000 on Jan. 7 in the latest psychologically significant milestone for cryptocurrency bulls.Data from Cointelegraph Markets and TradingView confirmed BTC/USD officially hitting dizzying new highs during trading on Thursday, bringing weekly gains to more than 40%.In just one of various achievements for Bitcoin this year, the largest cryptocurrency conquered bearish prognoses of a retraction after rising rapidly throughout the past 24 hours')}}</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('As Cointelegraph reported, Bitcoin’s NVT ratio led signals suggesting that the bull run was far from over, and likely only beginning')}}</p>
               <div class="row">
                
                  <div class="col-md-12">
                    <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Markets are doing great and the bull cycle is starting up nicely here. That means that the market will probably continue running heavily this coming year. With the standard 20-30% corrections, be prepared, they occur and they are opportunities. Less than a month after Bitcoin broke through $20k its price has doubled to $40k.  It is now hard for anyone to deny we are seeing the maturation of a whole new asset class, This could be the broadband moment for cryptocurrency - where every company and individual needs to think seriously about how they engage and interact with cryptocurrency, added Sui Chung, CEO of CF Benchmarks, which is the FCA-regulated indices provider used by CME')}}</p>
                  </div>
               </div>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>
   <div class="container">
   <div class=" text-left">
    <span style="color:#001838; font-size:20px">Share</span>
     <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/bitcoin-price-hits-$40,000-less-than-three-weeks&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
     <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/bitcoin-price-hits-$40,000-less-than-three-weeks" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
     <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/bitcoin-price-hits-$40,000-less-than-three-weeks&title=&summary=&source=" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
  </div>
 </div>
   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
