@php($blog_name="Soccer Fan Tokens on the March as Poland’s Biggest Club Adopts Crypto")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/19.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__("Soccer Fan Tokens on the March as Poland’s Biggest Club Adopts Crypto")}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("The spread of blockchain-based soccer fan tokens across Europe continued on Monday as Poland’s most successful soccer club partnered with sports fintech firm Chiliz. Legia Warsaw, which holds the record for the most number of Polish league championship wins, will launch a token on the fan engagement and rewards platform Socios. The LEG Fan Token is expected to launch in the coming months and will grant supporters voting rights on various club decisions. Tokenholders will also be eligible to take part in exclusive games, competitions and VIP experiences. The supply of LEG Fan Tokens will be capped at 5 million. Legia Warsaw joins 21 European football giants to have created fan tokens on the Socios platform recently. Major soccer institutions such as Lionel Messi’s FC Barcelona, Cristiano Ronaldo’s Juventus, and Zlatan Ibrahimovic’s A.C. Milan have all launched club-based fan tokens on the Chiliz blockchain")}}</p>

             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("Dariusz Mioduski, CEO of Legia Warsaw, appeared optimistic about interacting with the new technology offered up by blockchain and tokenization. Pioneering solutions and new technologies, which additionally give our fans unique opportunities to actively participate in the life of our club, are the exact direction in which we want to develop,” said Mioduski, adding, “We believe that the potential of the most popular sports brand in this part of Europe, which Legia Warsaw undoubtedly is, will allow Socios.com to gain many fans not only in Poland, but also in the international arena.” The CEO and founder of Socios.com and Chiliz, Alexandre Dreyfus, welcomed Legia Warsaw onto the Socios platform and anticipated it would increase overall fan engagement.“Very soon Legia fans all around the world will have a new way to get closer to the club they love and will be able to influence the team in polls, access VIP rewards, exclusive promotions, chat forums and much more on Socios.com,” Dreyfus said, adding, “We’re sure LEG Fan Tokens will be a massive hit with fans and become a very powerful new engagement tool for the club")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/soccer-fan-tokens-on-the-march-as-poland-biggest-club-adopts-crypto&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>


     <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/soccer-fan-tokens-on-the-march-as-poland-biggest-club-adopts-crypto" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/soccer-fan-tokens-on-the-march-as-poland-biggest-club-adopts-crypto" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
