@php($blog_name="US-China Talks: Keys on Wall Street")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5">
           <img src="{{asset('assets/img/portfolio/30.jpg')}}" alt="" height="500" width="800" class="img-fluid mt-5 pt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio  mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__("US-China Talks: Keys on Wall Street")}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("The Bank of Japan begins to reduce its hyper-aggressive monetary stimulus, while Russia becomes the third major emerging economy to raise interest rates this week. The new wave of Covid-19 of the EU is increasing, while Paris returns to decree the total confinement; Nike (NYSE: NKE ) becomes the latest company affected by supply chain problems. Here we have the five main issues to be aware of this Friday, in the financial markets")}}</p>

             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("The talks between the United States and China start on the wrong foot: The first face-to-face talks between the United States and China since President Joe Biden took office have gotten off to a bad start in Alaska, as Chinese delegates have criticized the United States for everything from institutional racism to trade tariffs and cyber espionage.US Secretary of State Anthony Blinken responded by accusing China of undermining the rules-based world order with its actions towards Hong Kong and Taiwan, and it is treatment of Muslim Uighurs in its Xinjiang province. One of the first actions of the Biden government was to accuse China of genocide in Xinjiang")}}</p>

             <p class="text-justify" style="font-size: 20px; color:#001838">{{__("The Bank of Japan reduces stimuli, while Russia increases them: The Bank of Japan has signaled that it is losing faith in its decades of efforts to avoid deflation with asset purchases. The central bank abandoned its promise to buy 60 trillion yen shares this year, claiming it would adopt other policies to ensure that banks' profitability is not severely hurt by its negative interest rate policy.The Bank of Japan has also said it would allow long-term bond yields to fluctuate by 25 basis points, having previously led the market to assume a range of 20 basis points. This has been interpreted as a modest monetary policy tightening, although Governor Haruhiko Kuroda has trivialized such a claim")}}</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>


   <div class="container">
   
    <div class=" text-left">
      <span style="color:#001838; font-size:20px">Share</span>
       <a href="https://twitter.com/intent/tweet?url=https://empirexcapital.com/US-China-Talks-Keys-on-Wall-Street&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>


       <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/US-China-Talks-Keys-on-Wall-Street" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>

       <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://empirexcapital.com/US-China-Talks-Keys-on-Wall-Street" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
    </div>
   </div>


   <!-- End #main -->
   @include('partials.comment')

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
