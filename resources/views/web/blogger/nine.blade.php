@php($blog_name="Netflix 'Might' be Next Fortune 100 Firm to Buy Bitcoin")

@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
    
        <section id="" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100 mt-5 pt-5">
           <img src="{{asset('assets/img/portfolio/24.jpg')}}" alt="" height="800" width="700" class="img-fluid mt-5">
         </div>
        </section> 

    <main id="main">

    

    <section id="" class="portfolio section-bg mt-5">
     <div class="container">
         <div class="row">
           <div class="col-lg-12">
             <h2 style="color:#001838">{{__("Netflix 'Might' be Next Fortune 100 Firm to Buy Bitcoin")}}</h2>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('The famous investor Tim Draper picks Netflix among major companies as the most likely to put Bitcoin on its balance sheet')}}.</p>
             <p class="text-justify" style="font-size: 20px; color:#001838">{{__('Netflix may just be the next Fortune 100 company to buy Bitcoin (BTC) and Amazon will have to accept it, says billionaire Tim Draper.In an appearance on the Unstoppable Podcast on Feb. 28, the serial investor and hodler forecast that out of all possible candidates, Netflix is his pick for putting BTC on its balance sheet')}}.</p>
              <p class="text-justify" style="font-size: 20px; color:#001838" >{{__('The prediction comes as telltale signs of institutional buy-ins continue to surface at current prices, with $48,000 seeing multiple large transactions at Coinbase Pro over the past week. Classic buyers MicroStrategy and Square both added to their positions. Ever the optimist, Draper also considered that Amazon would add a direct Bitcoin payment option in future. Amazon will probably start accepting Bitcoin pretty soon, he said, noting that consumers have been able to buy products indirectly using cryptocurrency for many years. Amazon added Ethereum availability to its Managed Blockchain this week')}}.</p>
            </div>
         </div>
       
     </div>
    </section><!-- End Why Us Section -->
        
   </main>
   <div class="container">
   <div class=" text-left">
    <span style="color:#001838; font-size:20px">Share</span>
     <a href="https://twitter.com/intent/tweet?url=http://empirexcapital.com/netflix-might-be-next-fortune-100-firm-to-buy-bitcoin&text=" target="_blank" class="twitter p-1"><i class="fa fa-twitter fa-2x"></i></a>
     <a href="https://www.facebook.com/sharer/sharer.php?u=http://empirexcapital.com/netflix-might-be-next-fortune-100-firm-to-buy-bitcoin" target="_blank" class="facebook p-1"><i class="fa fa-facebook fa-2x"></i></a>
     <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://empirexcapital.com/netflix-might-be-next-fortune-100-firm-to-buy-bitcoin" target="_blank" class="linkedin p-1"><i class="fa fa-linkedin fa-2x"></i></a>
  </div>
 </div>
   <!-- End #main -->
   @include('partials.comment')
  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
