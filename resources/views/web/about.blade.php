@extends('layouts.principal')
@section('title','About Empirex Capital US | EMPIREX CAPITAL')

<style>
  .card-about{
    box-shadow: 0 0 15px 0 rgba(0, 0, 0, .4);
    padding: 20px;
    border-radius: 15px
  }

  .btn-about-form{
    border:none;
    color:#fff;
    font-weight: 600;
    background: #001838;
    border:2px solid #001838;
    border-radius: 1000px;
    padding: 5px 25px;
    cursor: pointer;
  }

  label{
    font-weight: 700;
    color:#001838;
  }

 .titulo{
  font-family: var(--font-principal)!important;
 }

  .about {
    padding: 0px !important;
  }
  

  .btn-portal{
      width:120px !important;
  }

 @media (min-width: 1200px){
.contenedor {
    max-width: 1519px!important;
    margin: 0 auto!important;
    font-family: var(--font-principal)!important;
}

  .slider-video{
    width:100% !important;
  
}

.about-content{
  padding:90px!important;
}

}

@media (min-width: 1500px){
      .slider-video{
    width:1519 !important;
  
}
}

@media only screen and (min-width: 320px) and (max-width: 1000px) {
  .contenedor {
    font-family: var(--font-principal)!important;
}

.slider-video{
    width:100% !important;
    height:400px !important;
  }
  
  .social-links{
    margin-top:-150px !important;
   }

}



</style>

@section('content')
<!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->


<!-- ======= Header ======= -->
@include('partials.navigation')
<!-- End Header -->

<!-- ======= Hero Section ======= 
      <section id="hero1" class="clearfix">
        <div class="d-flex justify-content-center align-items-center h-100 w-100">
          <div class="intro-info" data-aos="zoom-in" data-aos-delay="100">
            <h2 class="">{{__('Private investor fund')}}<br>
            <span>{{__('Grow your portfolio with us')}}</span>
          </h2>
    
        </div>
       </div>
      </section>-->

<main id="main">
<div class="contenedor" data-aos="fade-up">

  <section id="about" class="about">

   
      <div class="row">

        <div class="col-lg-12 col-md-12">
          <div class="slide-bg-wrap" style="position: relative">
            <div class="video-wrap translate" style=" transform: translate3d(0px, 0px, 0px);">
               <img src="{{asset('assets/img/Portada3.jpg')}}" alt="" class="portada">
            </div>
            <div class="title-app">
               <h2>{{__('Leadership')}}</h2>
               <p>{{__('in the new economy')}}</p>
            </div>
          </div>
        </div>

        <div class="col-lg-11 col-md-9 mx-auto">
          <div class="about-content pt-5" data-aos="fade-left" data-aos-delay="100">
            <!-- <h2 class="text-center">{{__('About Us')}}</h2>-->
            <p class="color text-justify about-title px-4 mb-3" style="font-size: 17px; color:#001838; font-weight:500">
              {{__('Empirex Capital is a private investment fund from the United States founded in 2012. We offer investment plans and raise capital from accredited and institutional clients to invest in the financial markets. Our offices are located in New York, Miami, and Hong Kong')}}.
            </p>

            <p class="color text-justify about-title mb-3 px-4" style="font-size: 17px; color:#001838; font-weight:500">
              {{__('Even though, we started as a traditional hedge fund our CEO, Rafael Vargas, had the vision to incorporate Cryptocurrencies as part of our portfolio in 2015, making us successful pioneers in digital asset management in the United States')}}.
            </p>

            <p class="color text-justify about-title pb-0 px-4" style="font-size: 17px; color:#001838; font-weight:500">
              {{__('We have recently launched our new Crypto OTC service so clients can either buy or sell large orders of Cryptocurrencies through us')}}.
            </p>
            <!-- <ul>
                  <li class="color"><i class="ion-android-checkmark-circle"></i> {{__('Traditional Markets')}}.</li>
                  <li class="color"><i class="ion-android-checkmark-circle"></i> {{__('Cryptocurrency Market')}}</li>
                  <li class="color"><i class="ion-android-checkmark-circle"></i> {{__('Mixed Markets')}}</li>
                </ul>-->

               <div class="btn-about text-center mb-2" >
                <a href="{{url('/portfolio')}}" class="btn-about-boton ml-2 btn1" >{{__('Hedge Fund')}}</a>
                <a href="{{url('/')}}" class="btn-about-boton  btn2">{{__('Buy Crypto')}}</a>
               </div>
               <div class="lineales mx-auto">
                <span class="lineal"></span>
               </div>
          </div>
        </div>
      </div>
    

  </section><!-- End About Section -->
  <!-- ======= Why Us Section ======= -->


  <section id="why-nosotros" class="why-us  ">
    <div class="contenedor" data-aos="fade-up">

      <div class="row">

        <div class="col-lg-4 offset-lg-1 p-0" data-aos="zoom-in" data-aos-delay="100">
          <div class="why-us-img">
            <img src="{{asset('assets/img/why.png')}}" alt="" class="img-fluid img-about">
          </div>
        </div>

        <div class="col-lg-5 offset-lg-1  pt-3">
          <div class="contenido">
            <h3 class="text-center titulo ">{{__('Why Investing with Us')}}?</h3>
            <div class="features" data-aos="fade-up" data-aos-delay="100">
              <p style="font-size: 15px ;color:#001838;padding: 0 15px;font-weight:500" class="text-justify m-0">
                {{__('Our exclusive team of wealth management professionals studies in detail the market trends creating customized strategies to diversify your assets smartly. Our strategies allow us to take advantage of volatility and be profitable regardless of market direction')}}.
              </p>

            </div>

          </div>

        </div>

      </div>

    </div>
  </section><!-- End Why Us Section -->



  <section id="why-nosotros" class="why-us">
    <div class="contenedor" data-aos="fade-up">

      <div class="row">

        <div class="col-lg-5 offset-md-1 " data-aos="zoom-in" data-aos-delay="100">
          <div class="why-us-image por m-0">
            <h3 class="text-center titulo">{{__('Our Purpose')}}</h3>
            <div class="features " data-aos="fade-up" data-aos-delay="100">
              <p style="font-size: 15px; color:#001838;padding: 0 15px;font-weight:500" class="text-justify">
                {{__('With our investment plans we desire to generate new sources of income and help you achieve your financial goals. We will create a customized investment portfolio with the guidance of our experts according to your risk profile and financial objectives')}}.
              </p>

            </div>
          </div>
        </div>

        <div class="col-lg-4 p-0 offset-md-1 ">
          <div class="why-us-content us">
            <img src="{{asset('assets/img/purpose.png')}}" alt="" class="img-fluid img-about">


          </div>

        </div>

      </div>

    </div>
  </section><!-- End Why Us Section -->




  <section id="why-nosotros" class="why-us mb-5  ">
    <div class="contenedor" data-aos="fade-up">

      <div class="row">

        <div class="col-lg-4 offset-lg-1 p-0" data-aos="zoom-in" data-aos-delay="100">
          <div class="why-us-img">
            <img src="{{asset('assets/img/business.png')}}" alt="" class="img-fluid img-about">

          </div>
        </div>

        <div class="col-lg-5 offset-lg-1">
          <div class="contenido">
            <h3 class="text-center titulo ">{{__('Our Business Model')}}</h3>
            <div class="features" data-aos="fade-up" data-aos-delay="100">
              <p style="font-size: 15px ;color:#001838;padding: 0 15px;font-weight:500" class="text-justify">
                {{__('At Empirex Capital we raise capital from accredited investors and institutions to form a private pool in any of our portfolios. Then, we sign an agreement for a specified time frame. We assign this capital to our brokers or traders who simultaneously buy and sell financial assets such as stocks, currencies, commodities or cryptocurrencies. The returns generated from these operations are delivered back to our investors monthly, quarterly or annually. Profits can be withdrawn or reinvested indefinitely, thus achieving a constant cash flow, long term growth, protection from inflation and financial independence')}}.

              </p>

            </div>

          </div>

        </div>

       

      </div>

    </div>
    
  </section><!-- End Why Us Section -->


   
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 my-5 text-center">
          <a href="{{url('/portfolio')}}" class="btn-portfolio">{{__('Our Portfolio')}}</a>
       </div>

       <div class="col-md-9 my-5 mx-auto">
        <h3 class="text-center" style="color:#001838; font-weight:700">{{__('Want to Learn More? Book a Free Consultation')}}.</h3>
           <div class="card-about">
         
             <form action="{{route('message.about')}}" method="POST">
                @csrf
               <div class="row">
                <div class="col-md-6">

                   <div class="form-group">
                     <label for="">{{__('First Name')}}:</label>
                     <input type="text" name="firstname" class="form-control" required>
                   </div>

                   <div class="form-group">
                     <label for="">{{__('Email')}}:</label>
                     <input type="email" name="email" class="form-control" required>
                   </div>

                   <div class="form-group">
                     <label for="">{{__('Address')}}:</label>
                     <input type="text" name="address" class="form-control" required>
                   </div>
                </div>

                <div class="col-md-6">

                   <div class="form-group">
                     <label for="">{{__('Last Name')}}:</label>
                     <input type="text" name="lastname" class="form-control" required>
                   </div>
                     
                   <div class="form-group">
                     <label for="">{{__('Phone')}}:</label>
                     <input type="text" name="phone" class="form-control" required>
                   </div>
                   <div class="form-group">
                    <label for="">{{__('Message')}}:</label>
                    <input type="text" name="message" class="form-control" required>
                  </div>
                   
                </div>
               </div>
               <button type="submit" class="btn-about-form">{{__('Send')}}</button>

             </form>
           </div>
      </div>
      </div>
    </div>
  </section>

  </div>
  
</main><!-- End #main -->



<!-- ======= Call To Action Section ======= 
       <section id="call-to-action" class="call-to-action">
                  <div class="container" data-aos="zoom-out">
                    <div class="row">
                      <div class="col-lg-9 text-center text-lg-left">
                      <h3 class="cta-title">{{__('Invest from')}} <b>{{__('$10,000 USD')}}</b></h3>
                        <p class="cta-text">{{__('Choose your agreement time frame 3, 6, or 12 months')}}</p>
                      </div>
                      <div class="col-lg-3 cta-btn-container text-center">
                      <a class="cta-btn align-middle" href="{{asset('assets/Brochure.pdf')}}" target="_blank">{{__('Download Brochure')}}</a>
                      </div>
                    </div>
            
                  </div>
       </section>  End Call To Action Section -->



<!-- ======= Footer ======= -->
@include('partials.footer')

<!-- End  Footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>

<script>

$(document).ready(function () {


$(window).on("scroll", function(){
    if($(window).scrollTop() > 600){
    $("#header").addClass("header_scroll");
    $("#header").removeClass("header_top");
    console.log('scroll', $(window).scrollTop());
    }
    else if($(window).scrollTop() < 600){
      $("#header").removeClass("header_scroll");
    $("#header").addClass("header_top");
    console.log('scroll top', $(window).scrollTop());
    }
});


});


</script>