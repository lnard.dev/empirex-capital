@extends('layouts.principal')


@section('styles')

<style>
.btn-portal{
    width:120px !important;
}
</style>

@endsection

@section('content')

   <!-- ======= Header ======= -->
   @include('partials.navigation')
   <!-- End Header -->
  
<div class="container contact-form">
  
  <form method="post" action="{{route('message.index')}}">
    @csrf
      <h3>{{__('Contact Us') }}</h3>
     <div class="row">
          <div class="col-md-6">
              <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Your Name *" value="" required />
              </div>
              <div class="form-group">
                  <input type="text" name="email" class="form-control" placeholder="Your Email *" value="" required />
              </div>
              <div class="form-group">
                  <input type="text" name="phone" class="form-control" placeholder="Your Phone Number *" value="" required />
              </div>
          </div>
          <div class="col-md-6">
              <div class="form-group">
                  <textarea name="message" class="form-control" placeholder="Your Message *"  required style="width: 100%; height: 150px;"></textarea>
              </div>
          </div>
          <div class="g-recaptcha ml-3 mb-3"  data-sitekey="6LdlqykaAAAAAPlDnqbx4IVmgB1AjCc3pDu3-zp_"></div>

          <div class="form-group w-100">
            <input type="submit" value="Send Message" class="btnContact" />
            <a href="https://empirexcapital.setmore.com" target="_blank" class="btnContact2">{{__('Book Consultation')}}</a>
         </div>
      </div>
  </form>
</div>

<!-- ======= Footer ======= -->
@include('partials.footer')

@endsection


@section('scripts')

<script type="text/javascript">
  var RecaptchaOptions = {
        lang : 'fr',
  };
  </script>

@endsection