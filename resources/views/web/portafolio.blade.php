@extends('layouts.principal')


<style>

.section-headers{
  font-family: var(--font-principal)!important;
    color: var(--font-color)!important;
    font-weight: 700!important;
    font-size: 30px!important;
    text-align: center!important;
}


.video3{
    width: 1519px;
    margin-top: -150px;
}




.btn-portal{
    width:120px !important;
}


  @media (min-width: 1200px){
    .contenedor {
        max-width: 1319px!important;
        margin: 0 auto!important;
        font-family: var(--font-principal)!important;
    }

}

  @media (max-width: 768px){
     .why-us{
       margin-top:50px !important;
      }

}

@media only screen and (min-width: 320px) and (max-width: 736px) {

  .social-links{
    margin-top:-150px !important;
   }
   
   .why-us{
       margin-top:-300px !important;
   }
   
   .aboutimage{
        margin-top:80px;
   }

}



</style>

@section('title','Portfolio Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->

      <div class="contenedor">

    <section id="hero3" class="clearfix">
        <div class="d-flex justify-content-center align-items-center h-100 w-100">
          <div class="intro-info" data-aos="zoom-in" data-aos-delay="100" style="margin-top:-300px">
            <h2>{{__('Portfolio')}}<br>
            <br>
              <span>{{__('We have the best investment plans')}}</span>
            </h2>
                 
                   <img src="{{asset('assets/img/portfolio.jpg')}}" class="img-fluid aboutimage">
                 
          </div>
        <video autoplay loop muted="muted" preload="auto" webkit-playsinline playsinline class="video3">
          <source src="{{asset('media/icome.mp4')}}" type="video/mp4">
       </video>
        </div>
    </section><!-- End Hero -->

    <main id="main" class="">
              <!-- ======= Why Us Section ======= -->
        <section id="why-us" class="why-us" style="margin-top:-150px">
          <div class="container" data-aos="fade-up">
            <div class="row">

              <div class="col-lg-6">
                <div class="section-headers">
                 <p class="">{{__('Traditional Investments')}} </p>
                </div>

              <div class="" data-aos="fade-up" data-aos-delay="100">
                  <p style="font-size: 20px" class="text-justify color"> {{__('Our private fund consists of generating strategies for the purchase and sale of assets such as stocks, currencies, raw materials, futures contracts, among others. Our investment managers are highly qualified professionals who apply proven successful methods supported by advanced technology to always stay ahead of market opportunities')}}
                  </p>
              </div>
              </div>

              <div class="col-lg-6 text-center" data-aos="zoom-in" data-aos-delay="100">
                <div class="why-us-img">
                  <img src="{{asset('assets/img/traditional.jpg')}}" class="zoom" width="500" height="300" style="max-width: 100%" >
                </div>
              </div>

            </div>
          </div>
        </section><!-- End Why Us Section -->
        
        <section id="" class="portafolio section-bg2 ">
          <div class="container" data-aos="fade-up">

            <div class="row mt-5 mb-5">

              <div class="col-lg-6 text-center" data-aos="zoom-in" data-aos-delay="100">
                <div class="why-us-img mt-5">
                  <img src="{{asset('assets/img/celubit.jpg')}}" class="zoom" width="500" height="300"  style="max-width: 100%" >
                </div>
              </div>


              <div class="col-lg-6 pt-2 pb-2 mt-5">
                <div class="section-headers">
                 <p class="text-white">{{__('Cryptocurrencies Investments')}} </p>
                </div>
                
                <div class="" data-aos="fade-up" data-aos-delay="100">
                  <p style="font-size: 20px; font-weight:400" class="text-justify text-white"> {{__('Our team of investment risk management experts analyze the markets and carry out different strategies such as marginal trading, mining, arbitrage, among others. This allows us to diversify our portfolio, thus obtaining higher investment returns with efficient risk management')}}
                  </p>
                </div>
            </div>

            </div>

          </div>

        </section><!-- End Why Us Section -->
        
        <section id="" class="mt-5 pt-5 mb-5">
          <div class="container" data-aos="fade-up">
           <p class="h3-portafolio color">{{__('Contact us for more information')}} </p>
            <div class="row">

              <div class="col-lg-12 pb-5 text-center">
                
                <div class="section-headers">
                     <!--<p class="p1-portafolio">{{__('Traditional Markets')}} </p>-->
                    <!--<p class="p2-portafolio">{{__('from $ 10,000 USD')}} </p>-->
                    <!--<p class="p3-portafolio">{{__('with fixed income 5% net monthly with the possibility of monthly payments or at the expiration of the contract')}}</p>-->
                    <p class="p4-portafolio">{{__('Choose your term of stay plans of 3, 6 and 12 months')}}</p>
                </div>

                  <div class="" data-aos="fade-up" data-aos-delay="100">
                    @if(Session::has('locale'))
                      <a href="{{asset('assets/brochure-spanish.pdf')}}" target="_blank" class="btn-empirex">{{__('DOWNLOAD BROCHURE')}}</a>
                      @else
                      <a href="{{asset('assets/brochure.pdf')}}" target="_blank" class="btn-empirex">{{__('DOWNLOAD BROCHURE')}}</a>
                    @endif
                </div>
              </div>

            </div>

          </div>

        </section><!-- End Why Us Section -->
   </main>
   <!-- End #main -->
</div>


  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- ======= Footer ======= -->
    @include('partials.footer')
@endsection


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>

<script>

$(document).ready(function () {


$(window).on("scroll", function(){
    if($(window).scrollTop() > 600){
    $("#header").addClass("header_scroll");
    $("#header").removeClass("header_top");
    console.log('scroll', $(window).scrollTop());
    }
    else if($(window).scrollTop() < 600){
      $("#header").removeClass("header_scroll");
    $("#header").addClass("header_top");
    console.log('scroll top', $(window).scrollTop());
    }
});


});


</script>