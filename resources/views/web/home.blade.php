@extends('layouts.principal')
@section('title','Home Empirex Capital US | EMPIREX CAPITAL')

@section('styles')
    <style>
      .otra2 {
        background: #41BF49;
        color:#fff !important;
      }
      .otra {
        background: #fff;
        color:#001838 !important;
      }
      .label-form{
        text-align: left !important;
        color:#001838;
        font-weight: 500
      }
    </style>
@endsection
@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent w-100">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->


  

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->

      @if(session('info'))
           
      <div class="alert alert-success">
        {{session('info')}}
      </div>

      @endif

      <!-- ======= Hero Section ======= -->
      <section id="hero" class="clearfix">
        <div class="d-flex justify-content-center align-items-center h-100 w-100">
         {{--  <div class="intro-info" data-aos="zoom-in" data-aos-delay="100">
                <h2 class="">{{__('Crypto Hedge Fund & OTC')}}<br>
                  <span>{{__('Invest or Buy Crypto')}}</span><br><br>
               </h2>
            </h2>
          </div> --}}
               
        <a  data-toggle="modal" data-target="#buy" style="cursor: pointer;width:100%">
          <video preload="auto" autoplay loop muted="muted" webkit-playsinline playsinline class="video">
            <source src="{{asset('media/principal3.mp4')}}" type="video/mp4">
         </video>
        </a>
        </div>
    </section>
    <!-- End Hero -->
    <main id="main">
      <section class="section-principal sec ">
        <div class="container ">
          <div class="row">
              <div class="col-md-6 linea invest">
                <h3 class=" invest-h3" style="color:#001838;font-weight:bold">INVEST</h3>
                <p class="text-justify " style="cursor: #001838">
                {{__('At Empirex Capital we raise capital from accredited and
                  institutional clients to invest in the financial markets')}}.</p>
                  <div class="text-center">
                    <a href="https://empirexcapital.com/portfolio" class="hedge">Hedge Fund</a>
                  </div>
              </div>

              <div class="col-md-6 cryp">
                <h3 style="color:#001838; font-weight:bold" class="cryp-h3">BUY CRYPTO</h3>
                <p class="text-justify" style="cursor: #001838">{{__('We also offer OTC services to any client who
                  wants to buy or sell cryptocurrencies in bulk')}}.</p>
                  <div class="text-center">
                    <a  data-toggle="modal" data-target="#buy" class="comprar mt-5" >Buy Crypto</a>
                  </div>
              </div>
          </div>
        </div>
     </section>

    {{--  <section class="mt-5">
          
            <a href="https://empirexcapital.setmore.com"  data-toggle="modal" data-target="#buy"  style="height: 300px; cursor:pointer">
              <video  autoplay loop muted="muted" webkit-playsinline playsinline width="100%" >
                <source src="{{asset('media/banner2.mp4')}}" type="video/mp4">
             </video>
            </a>
          
     </section>  --}}

    <!-- ======= Services Section ======= -->
      <section id="services" class="services">
        <div class="contenedor mt-5 pt-3" >
          <h2 class="text-center titulo mb-5">{{__('Simplified Investment Process')}}</h2>
          <div class="row">
           
            <div class="col-md-6 col-lg-3 text-center wow bounceInUp mt-2  " data-aos="zoom-in" data-aos-delay="100" >
              <div class="box" height="auto">
                <div style="padding-bottom: 10px">
                   <h4 class="title"><a href="">{{__('Invest')}}</a></h4>
                </div>
                  <img src="{{asset('assets/img/1.png')}}" alt="" width="200" height="180">
                <p class="description text-center px-5">
                  {{__('Choose your portfolio and invest in traditional or cryptocurrency assets')}}. <br>
                </p>
              </div>
              <!--<a href="{{route('portafolio')}}" class="btn-empirex">{{__('see more')}}</a>-->
            </div>

            <div class="col-md-6 col-lg-3 text-center  mt-2" data-aos="zoom-in" data-aos-delay="200">
              <div class="box">
                <div style="padding-bottom: 10px">
                       <h4 class="title"><a href="">{{__('Sign an Agreement')}}</a></h4>
                </div>
                      <img src="{{asset('assets/img/b.png')}}"  width="280" alt="" height="180">
                <p class="description text-center  p-2">
                  {{__('Start with a minimum time frame')}} <br>
                  {{__('of 3, 6, or 12 months')}}.
                </p>
              </div>
              <!--<a href="{{route('portafolio')}}" class="btn-empirex">{{__('see more')}}</a>-->

            </div>

            <div class="col-md-6 col-lg-3 text-center    mt-2" data-aos="zoom-in" data-aos-delay="300">
              <div class="box">
                <div style="padding-bottom: 10px" >
                  <h4 class="title"><a href="">{{__('We Get to Work')}}</a></h4>
                </div>
                   <img src="{{asset('assets/img/2.png')}}"  width="320" alt="" height="180">
                <p class="description text-justify p-2">
                  {{__('Our team of professionals sets the strategies and simultaneously trade assets such as stocks, fiat currencies, commodities or cryptocurrencies taking advantage of the conditions of the markets')}}.
                </p>
              </div>
              <!--<a href="{{route('portafolio')}}" class="btn-empirex">{{__('see more')}}</a>-->
            </div>

            <div class="col-md-6 col-lg-3 text-center    mt-2" data-aos="zoom-in" data-aos-delay="300">
              <div class="box">
                <div style="padding-bottom: 10px" >
              
                      <h4 class="title"><a href="">{{__('Receive Returns')}}</a></h4>
                </div>
                <img src="{{asset('assets/img/d.png')}}"  width="200" alt="" height="180">
                <p class="description text-justify p-2">
                  {{__('From our operations, a return is generated which is delivered back to our investors monthly, quarterly or upon their agreement maturity')}}.
                </p>
              </div>
              <!--<a href="{{route('portafolio')}}" class="btn-empirex">{{__('see more')}}</a>-->
            </div>
               
         </div>
      </section><!-- End Services Section -->

        <!-- ======= Pricing Section ======= -->
        <!--<section id="pricing" class="pricing section-bg wow fadeInUp">
          <div class="container" data-aos="fade-up">
            <header class="section-header">
              <h3 class="mt-5 mb-5">{{__('TRADITIONAL MARKETS / CRYPTOCURRENCIES')}}</h3>
            </header>
    
            <div class="row flex-items-xs-middle flex-items-xs-center">
    
           
              <div class="col-xs-12 col-lg-4 col-md-6  wow bounceInUp" data-aos="zoom-in" data-aos-delay="100">  
                  <img src="{{asset('assets/img/3.png')}}" class="zoom" alt="">
              </div>
    
         
              <div class="col-xs-12 col-lg-4 col-md-6" data-aos="zoom-in"  data-aos-delay="100">  
                <img src="{{asset('assets/img/6.png')}}" alt=""  class="zoom">
            </div>
    
       
              <div class="col-xs-12 col-lg-4 col-md-6" data-aos="zoom-in"  data-aos-delay="100">  
                <img src="{{asset('assets/img/12.png')}}" alt="" class="zoom">
            </div>
    
            </div>
          </div>
    
        </section>  -->

       
        <section id="why-nosotros" class="why-us section-bg2   ">
          <div class="contenedor" data-aos="fade-up">
    
            <div class="row">
    
              <div class="col-lg-5 offset-lg-1" data-aos="zoom-in" data-aos-delay="100">
                <div class="why-us-img">
                  <img src="{{asset('assets/img/traditional1.png')}}" alt="" class="img-fluid">
                  
                </div>
              </div>
    
              <div class="col-lg-5 pb-5 ">
                <div class="contenidos">
                  <h3 class="text-center titulo text-white mt-5">{{__('Traditional Markets')}}</h3>
                  <div class="features" data-aos="fade-up" data-aos-delay="100">
                    <p style="font-size: 20px" class="text-justify text-white"> {{__('Our private fund manages investments in traditional markets that consist of trading assets such as stocks, commodities, currencies, among others')}}.
                    </p>
                    <div class="text-center">
                      <a href="{{route('portafolio')}}" class="btn-protafolio text-center">Portfolio</a>
                    </div>
                  </div>
    
                </div>
    
              </div>
    
            </div>
    
          </div>
        </section><!-- End Why Us Section -->
     

        <section id="why-nosotros" class="why-us section-bg ">
          <div class="contenedor" data-aos="fade-up">
    
            <div class="row">
    
              <div class="col-lg-5 offset-lg-2  pb-5" data-aos="zoom-in" data-aos-delay="100">
                <div class="why-us-image">
                  <h3 class="text-center titulo">{{__('Cryptocurrency Market')}}</h3>
                  <div class="features " data-aos="fade-up" data-aos-delay="100">
                    <p style="font-size: 20px; color:#001838" class="text-justify"> {{__('Our private fund manages investments in cryptocurrency markets that consist of the negotiation of decentralized and encrypted digital currencies that are transferred between people or corporations')}}.
                    </p>
                    <div class="text-center">
                    <a href="{{route('portafolio')}}" class="btn-protafolio1 text-center">Portfolio</a>
                    </div>
                  </div>
                </div>
              </div>
    
              <div class="col-lg-4  mt-5 ">
                <div class="why-us-content text-center ">
                  <img src="{{asset('assets/img/bitcoin1.png')}}" alt="" class="img-fluid img-image-bitcoin">
                  
    
                </div>
    
              </div>
    
            </div>
    
          </div>
        </section><!-- End Why Us Section -->
    

    
         <!-- ======= ceo Section ======= -->
      <section id="ceo" class="about mb-5 ">

        <div class="container" data-aos="fade-up">
          <div class="row">
  
            <div class="col-12 col-lg-8 col-md-7 mt-5 ceo">
             
             <h2 class="mt-5 mb-0" style="color: #001838">  <span>“</span> {{__('Remember That a Good Financial Decision Can Be Your Best Ally Towards Success')}}.” </h2>
             <span class="autor float-right" > - Rafael Vargas, Founder and CEO</span>

             <p class="info-ceo p-2" >{{__('Rafael adds real value to people looking to increase their investments. The 21st century is a brave new world for investors, which while very exciting, can easily become overwhelming. Rafael and our team of investment experts constantly study markets and trends to provide our clients with investment advice that best suits their personal finances')}}.</p>
             <div class="social text-center">
                <!--<a href="https://www.instagram.com/ceo_ny/" tagert="_blank" ><i class="fa fa-instagram fa-2x" style="color: #fff;background:#001838; padding:.4em; border-radius:50% 50%"></i></a>-->
                <a href="https://www.linkedin.com/in/rafael-empirex-4ab668140/" tagert="_blank"><i class="fa fa-linkedin fa-2x redes" style="color: #fff;background:#001838; padding:.4em; border-radius:50% 50%; margin-left:5px"></i></a>
              </div>
            </div>
  
            <div class="col-9 col-lg-4 col-md-5 text-center mx-auto mt-5 " >
              <img src="{{asset('assets/img/ceo.png')}}" class="img-fluid" width="600" alt="">
              <img src="{{asset('assets/img/firma.png')}}" class="img-fluid" width="500" alt="">
              <!--<h4 style="color: #001838"> <b> "{{__('FIRST STEPS IN THE WORLD OF TRADING')}}"</b></h4>-->
            </div>
          </div>
        </div>
  
      </section>
      <!-- End About Section -->

    <!-- ======= Testimonials Section ======= -->
    <!-- ======= Team Section ======= -->
    <!--<section id="team" class="team section-bg">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3>{{__('Team')}}</h3>
          <p style="font-size: 20px">{{__('Every achievement is accompanied by a great team')}}</p>
          </p>
        </div>
        <div class="row">
          <div class="col-lg-5 offset-lg-1 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <div class="member">
              <img src="assets/img/team-1.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>David Acero</h4>
                  <span>{{__('Executive Manager')}}</span>
                  <div class="social">
                    <a href="https://www.instagram.com/davidacero2/" tagert="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="" tagert="_blank"><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5 col-md-5" data-aos="fade-up" data-aos-delay="200">
            <div class="member">
              <img src="assets/img/team-2.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Catherine Rojas</h4>
                  <span>{{__('Executive Director')}}</span>                 
                  <div class="social">
                    <a href="https://www.instagram.com/cathekarate" tagert="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="" tagert="_blank"><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> End Team Section -->



  {{-- banner --}}
  
    {{--  <section class="my-5">
          <div class="conatiner">
            <a href="https://empirexcapital.setmore.com" target="__blank">
              <video  autoplay loop muted="muted" webkit-playsinline playsinline class="banner">
                <source src="{{asset('media/banner.mp4')}}" type="video/mp4">
             </video>
            </a>
          </div>
     </section> --}}

  {{--  --}}


    {{-- card --}}
  
 {{--   <section class="fijo">
          <div class="card">
              <h2>Buy Crypto</h2>
              <p>Get started from $1,000 USD</p>
              <small>It’s simple and you don’t need to create an account on
                any exchange</small>
                <button>Buy now</button>
          </div>
  </section>  --}}
  {{--  --}}


        <!-- ======= F.A.Q Section ======= -->
        <section id="faq" class="faq " >
          <div class="container" data-aos="fade-up">
            <header class="section-header">
              <h3>{{__('Frequently Asked Questions') }}</h3>
              
            </header>
    
            <ul id="faq-list" data-aos="fade-up" data-aos-delay="100">
              <div class="row">
                 <div class="col-md-6">
                  <li>
                    <a data-toggle="collapse" class="collapsed" href="#faq1">{{__('What is a private investment fund') }}? <i class="ion-android-remove"></i></a>
                    <div id="faq1" class="collapse" data-parent="#faq-list">
                      <p  class="text-justify">
                        {{__('It is a financial instrument for collective investment that is privately organized, managed by professionals who charge commissions on the profitability obtained') }}.
                      </p>
                    </div>
                  </li>
        
                  <li>
                  <a data-toggle="collapse" href="#faq2" class="collapsed">{{__('What do you mean financial market')}}? <i class="ion-android-remove"></i></a>
                    <div id="faq2" class="collapse" data-parent="#faq-list">
                      <p  class="text-justify">{{__('Financial markets refer broadly to any marketplace  where the trading of securities occurs, including the stock market, bond market, forex market and derivatives market, among others')}}.
                      </p>
                    </div>
                  </li>
        
    
        
                 </div>
                 <div class="col-md-6">
                  <li>
                    <a data-toggle="collapse" href="#faq3" class="collapsed">{{__('Who is an Empirex Capital Ambassador')}}? <i class="ion-android-remove"></i></a>
                    <div id="faq3" class="collapse" data-parent="#faq-list">
                      <p  class="text-justify">{{__('An Empirex Capital Ambassador is the person who refers clients to our company and receives a commission from this connection')}}.
                      </p>
                    </div>
                  </li>
    
                  <li>
                    <a data-toggle="collapse" href="#faq4" class="collapsed">{{__('What is the role of a financial market')}}? <i class="ion-android-remove"></i></a>
                    <div id="faq4" class="collapse" data-parent="#faq-list">
                      <p class="text-justify">{{__('Financial markets play a vital role in the allocation of resources and operation of modern economies. Financial markets create products that provide a return for those who have excess funds')}}.
                      </p>
                    </div>
                  </li>
    
                 </div>
              </div>
            </ul>
    
          </div>
        </section><!-- End F.A.Q Section -->
    

    <div class="contenedor Markets">
      <header class="section-header">
        <h3 class="pt-5">{{__('Blog') }}</h3>
      </header>
      <div class="row">

        <div class="col-md-4">
          <div class="card tarjeta">
            <img src="{{asset('assets/img/portfolio/27.jpg')}}" alt=""  class="img-fluid" height="300">
 
            <div class="card-body cuerpo">
              <h2>{{__("The Cardano Surpasses the Bitcoin Rally and Ranks in the Top 3 of the Crypto")}}</h2>
              <small></small>
              <p class="text-justify">{{__('The Cardano is now the third largest cryptocurrency after Bitcoin and Ethereum, after shooting in recent weeks, surpassing even the massive Bitcoin rally')}}.</p>
              <a href="{{route('thirtyone')}}" class="btn-protafolio1 text-center">{{__('see more')}}</a>
            </div>
          </div>
        </div>


        <div class="col-md-4">
          <div class="card tarjeta">
            <img src="{{asset('assets/img/portfolio/29.jpg')}}" alt=""  class="img-fluid" height="300">
 
            <div class="card-body cuerpo">
              <h2>{{__("IBEX Exceeds 8,400 Points While Markets Weigh the Effect of Biden's Stimulus")}}</h2>
              <small></small>
              <p class="text-justify">{{__('The Ibex-35 opened the week strongly, over the 8,400-point barrier, in another debt market turmoil session in the face of concern about the effect of the AID')}}.</p>
              <a href="{{route('thirty')}}" class="btn-protafolio1 text-center">{{__('see more')}}</a>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="card tarjeta">
            <img src="{{asset('assets/img/portfolio/28.jpg')}}" alt=""  class="img-fluid" height="300">
 
            <div class="card-body cuerpo">
              <h2>{{__("Hong Kong Companies have Committed $ 1.3 Billion in just Seven Months")}}</h2>
              <small></small>
              <p class="text-justify">{{__('Before the coup that ousted Aung San Suu Kyi, Hong Kong companies had committed US$1.3 billion to the country in a stretch of just seven months')}}.</p>
              <a href="{{route('eight')}}" class="btn-protafolio1 text-center">{{__('see more')}}</a>
            </div>
          </div>
        </div>

      

      

        

   
        
      
     
        <!--<div class="col-md-6 col-sm-6 col-lg-4 pt-4 pr-1 pb-3">
          <div class="tradingview-widget-container">
                <div class="tradingview-widget-container">
                  <div class="tradingview-widget-container__widget"></div>
                  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
                  {
                  "colorTheme": "dark",
                  "dateRange": "12M",
                  "showChart": false,
                  "locale": "es",
                  "largeChartUrl": "",
                  "isTransparent": false,
                  "showSymbolLogo": true,
                  "width": "340",
                  "height": "465",
                  "tabs": [
                    {
                      "title": "Indices",
                      "symbols": [
                        {
                          "s": "FOREXCOM:SPXUSD",
                          "d": "S&P 500"
                        },
                        {
                          "s": "FOREXCOM:NSXUSD",
                          "d": "Nasdaq 100"
                        },
                        {
                          "s": "FOREXCOM:DJI",
                          "d": "Dow 30"
                        },
                        {
                          "s": "INDEX:NKY",
                          "d": "Nikkei 225"
                        },
                        {
                          "s": "INDEX:DEU30",
                          "d": "DAX Index"
                        },
                        {
                          "s": "FOREXCOM:UKXGBP",
                          "d": "FTSE 100"
                        }
                      ],
                      "originalTitle": "Indices"
                    },
                    {
                      "title": " ",
                      "symbols": [
                        {
                          "s": "CME_MINI:ES1!",
                          "d": "S&P 500"
                        },
                        {
                          "s": "CME:6E1!",
                          "d": "Euro"
                        },
                        {
                          "s": "COMEX:GC1!",
                          "d": "Gold"
                        },
                        {
                          "s": "NYMEX:CL1!",
                          "d": "Crude Oil"
                        },
                        {
                          "s": "NYMEX:NG1!",
                          "d": "Natural Gas"
                        },
                        {
                          "s": "CBOT:ZC1!",
                          "d": "Corn"
                        }
                      ],
                      "originalTitle": "Commodities"
                    }
                  ]
                }
                  </script>
                </div>
             
         </div>
        </div>
       
       <div class="col-md-6 col-sm-6 col-lg-4 pt-4 pr-1 pb-3">
         <div class="tradingview-widget-container">
           <div class="tradingview-widget-container__widget"></div>
           <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
           {
           "colorTheme": "dark",
           "dateRange": "12M",
           "showChart": false,
           "locale": "es",
           "largeChartUrl": "",
           "isTransparent": false,
           "showSymbolLogo": true,
           "width": "340",
           "height": "465",
           "tabs": [
             {
               "title": "Crypto",
               "symbols": [
                 {
                   "s": "BITSTAMP:BTCUSD"
                 },
                 {
                   "s": " COINBASE:ETHUSD"
                 },
                 {
                   "s": "BITSTAMP:XRPUSD"
                 },
                 {
                   "s": "COINBASE:BCHUSD"
                 },
                 {
                   "s": "COINBASE:LTCUSD"
                 }
               ]
             },
             {
               "title": " ",
               "symbols": [
                 {
                   "s": "CME_MINI:ES1!",
                   "d": "S&P 500"
                 },
                 {
                   "s": "CME:6E1!",
                   "d": "Euro"
                 },
                 {
                   "s": "COMEX:GC1!",
                   "d": "Gold"
                 },
                 {
                   "s": "NYMEX:CL1!",
                   "d": "Crude Oil"
                 },
                 {
                   "s": "NYMEX:NG1!",
                   "d": "Natural Gas"
                 },
                 {
                   "s": "CBOT:ZC1!",
                   "d": "Corn"
                 }
               ],
               "originalTitle": "Commodities"
             }
           ]
         }
           </script>
         </div>
       </div>
        <div class="col-md-6 col-sm-6 col-lg-4  pt-4 pr-1 pb-3">
               
              <div class="tradingview-widget-container">
                <div class="tradingview-widget-container__widget"></div>
                <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
                {
                "colorTheme": "dark",
                "dateRange": "12M",
                "showChart": false,
                "locale": "es",
                "largeChartUrl": "",
                "isTransparent": false,
                "showSymbolLogo": true,
                "width": "340",
                "height": "465",
                "tabs": [
                  {
                    "title": "Forex",
                    "symbols": [
                      {
                        "s": "FX:EURUSD"
                      },
                      {
                        "s": "FX:GBPUSD"
                      },
                      {
                        "s": "FX:USDJPY"
                      },
                      {
                        "s": "FX:USDCHF"
                      },
                      {
                        "s": "FX:AUDUSD"
                      },
                      {
                        "s": "FX:USDCAD"
                      }
                    ],
                    "originalTitle": "Forex"
                  },
                  {
                    "title": " ",
                    "symbols": [
                      {
                        "s": "CME_MINI:ES1!",
                        "d": "S&P 500"
                      },
                      {
                        "s": "CME:6E1!",
                        "d": "Euro"
                      },
                      {
                        "s": "COMEX:GC1!",
                        "d": "Gold"
                      },
                      {
                        "s": "NYMEX:CL1!",
                        "d": "Crude Oil"
                      },
                      {
                        "s": "NYMEX:NG1!",
                        "d": "Natural Gas"
                      },
                      {
                        "s": "CBOT:ZC1!",
                        "d": "Corn"
                      }
                    ],
                    "originalTitle": "Commodities"
                  }
                ]
              }
                </script>
              </div>
           
        </div>-->

      </div>
    </div>
 

       {{--  <div class="container">
          <div class="row">
            <div class="col-md-5 mx-auto" style="height: 200px">
              <div class="card pb-5">
                 <div class="card-body mb-5" style="height: 280px">
                   <h3 class="text-purple">{{__('Subscribe to our Newsletter')}}</h3>
                   <form action="{{route('newsletter')}}" method="POST">
                    @csrf
              
                    <div class="form-group">
                      <input type="text" class="form-control" name="name" placeholder="First Name" required>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="lastname" placeholder="Last Name" required>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" name="email" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                       <div class="g-recaptcha mt-2"  data-sitekey="6LdlqykaAAAAAPlDnqbx4IVmgB1AjCc3pDu3-zp_"></div>
                       <button class="btn subs mt-2" style="cursor: pointer">{{__('Subscribe')}}</button>
                    </div>
                  </form> 
                 </div>
              </div>
            </div>
          </div>
        </div>
    <br><br><br><br><br> --}}

    <section id="testimonials" class="testimonials" >
      <div class="container mt-5" data-aos="zoom-in">

        <div class="row justify-content-center">
          <div class="col-lg-8 mt-5">

            <div class="owl-carousel testimonials-carousel">

              <div class="testimonial-item text-center">
                <h3 style="margin-left:0px" class="text-center">Miami</h3>
                <h4 style="margin-left:0px" class="text-center">848 Brickell Ave. Suite 1005.</h4>
              </div>

              <div class="testimonial-item text-center">
                <h3 style="margin-left:0px" class="text-center">New York</h3>
                <h4 style="margin-left:0px" class="text-center">3 Columbus Circle. 15Th Floor.</h4>
              </div>

              <div class="testimonial-item text-center">
                <h3 style="margin-left:0px" class="text-center">Hong Kong</h3>
                <h4 style="margin-left:0px" class="text-center">Taikoo Place 979 Kings Rd. Combrige HS.</h4>
              </div>

            </div>

          </div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->
    


  

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <a href="https://api.whatsapp.com/send?phone=573507543101" target="_blank" class="whatsapp">
    <img src="{{asset('assets/img/whatsapp.png')}}"  alt="">
    </a>



<!-- Modal -->
<div class="modal" id="buy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-center modal-xl" role="document">
    <div class="modal-content" style="background: #fff;border-radius:20px">
    
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white float-left pl-3 pt-3" >
            <i class="fa fa-times-circle" style="background: #001838; color:#fff" aria-hidden="true"></i>
          </span>
        </button> 
     
      <div class="modal-body">
         <div class="container-fluid">
           <div class="row text-center">
             <div class="col-md-4 col-sm-12 banner-video" >
                   <video  autoplay loop muted="muted" webkit-playsinline playsinline   width="350px">
                    <source src="{{asset('media/pop.mp4')}}" type="video/mp4">
                </video>
               
             </div>

             <div class="col-md-8 col-sm-12 mx-auto">
                <h2 class="banner-title text-center">BUY/SELL CRYPTO?</h2>
                <h4 class="banner-subtitle">Minimum $1,000 USD</h4>
                <p class="banner-content" >Fill out this form and get your quote</p>
               <div class="banner-line"></div>
               <div class="banner-looking">
                 
               <form action="{{route('banner')}}" method="POST" class="mt-5" enctype="multipart/form-data">
                @csrf
                  <p>looking To:</p>
                 <div class="form-group">
                 
                  <a style="cursor: pointer"  class="buy" id="buy-a">  
                   <input type="radio" value="Buy" class="radioBuy"  id="flexRadioDefault1"  name="looking" id="flexRadioDefault1" style="opacity:0" required>
                   <label for="flexRadioDefault1" style="cursor: pointer" > Buy</label></a>
                  
                  <a style="cursor: pointer" class="shell" id="sell-a"> 
                   <input type="radio" value="Sell" class="radioSell" id="flexRadioDefault2" name="looking" id="flexRadioDefault2" style="opacity:0" required>
                   <label for="flexRadioDefault2" id="sell-label" style="cursor: pointer" > Sell</label>
                </a>
                 </div>
               </div>

               
                 <div class="container">
                     <div class="row mb-2">
                     
                            <div class="col-md-4 linea">
                                  <label for="" class="banner-label">Select crypto:</label>
                                  <select name="crypto" id="" class="banner-input select" required>
                                    <option value="" class="banner-option ">SELECT</option>
                                    <option value="BTC - Bitcoin" style="background-image: url(assets/img/b1.png)"  class="banner-option b1">BTC - Bitcoin</option>
                                    <option value="ETH - Ethereum" class="banner-option">ETH - Ethereum</option>
                                    <option value="USDT - Theter (ERC-20)" class="banner-option">USDT - Theter (ERC-20)</option>
                                    <option value="BCH - Bitcoin Cash" class="banner-option">BCH - Bitcoin Cash</option>
                                    <option value="OTHER" class="banner-option">OTHER</option>
                                  </select>
                              
                            </div>
                                  
                    
                        
                            <div class="col-md-4 linea">
                              <div class="form-group">
                                  <label for="" class="banner-label">Amount USD:</label>
                                  <input type="text" placeholder="Enter Amount USD" min="1000" class="banner-input" name="amount" required>
                              </div>
                              
                            </div>

                        

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="banner-label">Wallet:</label>
                                    <input type="text" placeholder="Enter Address" class="banner-input" name="wallet">
                                </div>
                            </div>

                      </div>

                 
                 
                 
                    <div class="row">
                         <div class="col-md-4">
                           <div class="form-group">
                    
                             <input type="text" placeholder="First Name" name="firstname" class="banner-form"  onfocus="this.placeholder = ''" required>
                           </div>

                           <div class="form-group">
                            <input type="email" placeholder="Email" name="email" class="banner-form" onfocus="this.placeholder = ''" required>
                          </div>

                          <div class="form-group">
                            <input type="text" placeholder="Address" name="address" class="banner-form" onfocus="this.placeholder = ''" required>
                          </div>
                         </div>

                        <div class="col-md-4">
                            <div class="form-group">
                              <input type="text" placeholder="Last Name" name="lastname" class="banner-form" onfocus="this.placeholder = ''" required>
                            </div>
                            <div class="form-group">
                              <input type="text" placeholder="Phone Number" name="phone" class="banner-form" onfocus="this.placeholder = ''" required>
                            </div>
                            <div class="form-group">
                              <input type="text" placeholder="City" name="city" class="banner-form" onfocus="this.placeholder = ''" required>
                            </div>
                        </div>


                           <div class="col-md-4">
                             
                              <div class="row">
                                  
                                <div class="col-md-12 mt-4 mb-2">
                                  <div class="file-drop-area">
                                    <span class="fake-btn">{{__('Upload ID')}} <i class="fa fa-upload"></i></span>
                                      <span class="file-msg"></span>
                                      <input class="file-input" type="file" name="file" id="file-upload" >
                                  </div>
                                  <span class="">(Passport or US Driver License)</span>
                                </div>
                               

                                <div class="col-md-6">
                                  <div class="form-group">
                                    <input type="text" placeholder="Country" name="country" class="banner-form" onfocus="this.placeholder=''" required>
                                  </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                      <input type="text" placeholder="Zip Code" name="code" class="banner-form" onfocus="this.placeholder=''" required>
                                    </div>
                                </div>
                              </div>
                           </div>
                        </div>
                        <p class="text-center mt-2" style="font-size: 14px; font-weight:400;color:#001838"> <input type="checkbox" required> I acknowledge the collection of my personal data is processed by Empirex Capital as described in Terms and Conditions.</p>

                        <button class="banner-boton btn-block" type="submit">Create Account</button>
                      </div>
                 
                 
               </form>
             </div>
           </div>
         </div>
      </div>
   
    </div>
  </div>
</div>


@endsection
@section('scripts')

@if(session('info'))
   <script>
      $( document ).ready(function() {
              $('#buy').modal('hide') 
              
     })
   </script>

   @else
   <script>
      $( document ).ready(function() {
              $('#buy').modal('toggle') 
        })  
   </script>

@endif

 <script>
   
    const buy = document.getElementById('flexRadioDefault1')
    const sell = document.getElementById('flexRadioDefault2')
    const sella = document.getElementById('sell-a')
    const buya =document.getElementById('buy-a')
    buy.addEventListener('change',()=>{
      buya.classList.add('buy')
       sella.classList.add('otra')
       sella.classList.remove('shell')
    })
    
    sell.addEventListener('change',()=>{
       buya.classList.remove('buy')
       buya.classList.add('otra')
       sella.classList.add('shell')
    })
     document.getElementById('file-upload').addEventListener('change',function(e){
             document.querySelector('.fake-btn').innerHTML="added"
      })
 </script>
@endsection