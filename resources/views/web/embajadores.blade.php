@extends('layouts.principal')
@section('title','Ambassadors Empirex Capital US | EMPIREX CAPITAL')

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
          <!-- ======= Hero Section ======= -->
          <section id="hero4" class="clearfix">
            <div class="d-flex justify-content-center align-items-center h-100 w-100">
              <div class="intro-info" data-aos="zoom-in" data-aos-delay="100">
                <h2 class="">{{__('Private investor fund')}}<br>
                <span>{{__('Grow your portfolio with us')}}</span>
              </h2>
        
            </div>
           </div>
          </section><!-- End Hero -->
 

    <main id="main">

       <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials ">
      <div class="container" data-aos="zoom-in">

       <div class="row">
         <div class="col-md-12">
          <header class="section-header">
            <h3 class="h3-embajador">{{__('Ambassadors') }}</h3>
            <p style="font-size: 30px" class="p-embajador">{{__('GENERATE EXTRA INCOME IN YOUR SPARE TIME') }}!</p>
            <p style="font-size: 20px; font-family:'Montserrat';">{{__('You don’t have to be a professional salesperson, you just have to have good relationships with potential investors') }}</p>
          </header>
  
         </div>
       </div>
        <div class="row">
          <div class="col-lg-12">

            <div class="owl-carousel testimonials-carousel">
              <div class="testimonial-item text-center">
                <img src="assets/img/embajadores/1.png" class="center" alt="">
              </div>

              <div class="testimonial-item">
                <img src="assets/img/embajadores/2.png" class="center" alt="">
              </div>

              <div class="testimonial-item">
                <img src="assets/img/embajadores/3.png" class="center" alt="">
              </div>



            </div>

          </div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->


   <!-- ======= Pricing Section ======= -->
   <section id="iconos" class="pricing section-bg wow fadeInUp">

    <div class="container" data-aos="fade-up">
      <header class="section-header">
      <h3 class="mt-5 mb-0 text-white upper">{{__('faq')}}</h3>
      </header>

      <div class="row flex-items-xs-middle flex-items-xs-center">
        <!-- Basic Plan  -->
        <div class="col-xs-12 col-lg-4 col-md-6  wow bounceInUp" data-aos="zoom-in" data-aos-delay="100">  
            <img src="{{asset('assets/img/icons/1.png')}}" class="img-fluid boom" data-toggle="modal" data-target="#modal1" data-toggle="tooltip" data-placement="top" title="{{__('Press me')}}" >
            <p class="p-emb">{{__('how does it works')}}?</p>
          </div>

        <!-- Regular Plan  -->
        <div class="col-xs-12 col-lg-4 col-md-6  wow bounceInUp" data-aos="zoom-in" data-aos-delay="100">  
          <img src="{{asset('assets/img/icons/2.png')}}" class="img-fluid boom" data-toggle="modal" data-target="#modal2" data-toggle="tooltip" data-placement="top" title="{{__('Press me')}}"  >
          <p class="p-emb">{{__('when are payments made')}}?</p>

        </div>

        <!-- Premium Plan  -->
        <div class="col-xs-12 col-lg-4 col-md-6  wow bounceInUp" data-aos="zoom-in" data-aos-delay="100">  
          <img src="{{asset('assets/img/icons/3.png')}}" class="img-fluid boom" data-toggle="modal" data-target="#modal3" data-toggle="tooltip" data-placement="top" title="{{__('Press me')}}"  >
          <p class="p-emb">{{__('how do i receive my commission')}}?</p>
        </div>

      </div>
    </div>

  </section>
  
  <br><br><br><br><br>
  
  <!-- End Pricing Section -->
             <!-- Modal 1 -->
             <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                  <div class="modal-header" style="background: #001838">
                  <h5 class="modal-title text-white text-center upper" id="exampleModalLabel">{{__('how does it works')}}?</h5>
                  </div>
                  <div class="modal-body text-justify">
                    <p> <b> {{__('step')}} 1: </b> {{__('You must contact and refer new investors')}}</p>
                    <p> <b> {{__('step')}} 2: </b> {{__('For each new contract you will receive 6% commission for invested capital')}}</p>
                    <p> <b> {{__('step')}} 3: </b> {{__('If you encourage your referrals to renew their contract, the first time you will receive a 4% commission')}}</p>
                    <p> <b> {{__('step')}} 4: </b> {{__('If you encourage your referrals to renew their contract for the second time, you will receive 1% commission')}}</p>
                  </div>
                </div>
              </div>
             </div>

              <!-- Modal 2 -->
              <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                  <div class="modal-content">
                    <div class="modal-header" style="background: #001838">
                    <h5 class="modal-title text-white text-justify upper" id="exampleModalLabel">{{__('when are payments made')}}?</h5>
                    </div>
                    <div class="modal-body">
                      <p>{{__('Payments are made every Friday after the referrer has made his deposit. If the deposit is reported on Thursday after 5:00 PM New York. your commission will be paid on Friday of the following week')}}</p>
                    </div>
                  </div>
                </div>
               </div>

              <!-- Modal 2 -->
              <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                  <div class="modal-content">
                    <div class="modal-header" style="background: #001838">
                    <h5 class="modal-title text-white upper" id="exampleModalLabel">{{__('how do i receive my commission')}}?</h5>
                    </div>
                    <div class="modal-body">
                      <p>{{__('It is done through a bank transfer or PayPal and within the United States by Zelle or Venmo')}}</p>
                    </div>
                  </div>
                </div>
               </div>
  
    </main>
    
    
    <br><br><br><br>
   <!-- End #main -->




  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection


@section('scripts')
    
@endsection