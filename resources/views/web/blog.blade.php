@extends('layouts.principal')
@section('title','Blog Empirex Capital US | EMPIREX CAPITAL')

<style>
  .portfolio-wrap{
    /*padding: 10px!important;*/
  }
  .portfolio-item h4 {

    margin-left: 50px;
    margin-right: 50px;

  }
  
  @media only screen and (min-width: 320px) and (max-width: 736px) {

  .social-links{
    margin-top:-150px !important;
   }

}

.btn-portal{
    width:120px !important;
}

</style>

@section('content')
  <!-- ======= Top Bar ======= 
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="https://twitter.com/empirexcapital" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
        <a href="https://www.facebook.com/EmpirexCapital" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
        <a href="https://www.linkedin.com/company/empirexcapital" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.instagram.com/empirexcapital" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
       <a href="https://www.youtube.com/channel/UCScVUH6wY3br46j97niU5cQ" target="_blank" class="instagram"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </div>-->

      <!-- ======= Header ======= -->
      @include('partials.navigation')
      <!-- End Header -->
      <!-- ======= Hero Section ======= -->
        <!-- ======= Hero Section ======= 
        <section id="hero2" class="clearfix">
          <div class="d-flex justify-content-center align-items-center h-100 w-100">
      
            <video autoplay loop muted="muted" class="video">
              <source src="{{asset('media/portafolio.mp4')}}" type="video/mp4">
           </video>
         </div>
        </section> -->

    <main id="main">

    

   <section id="portfolio" class="portfolio section-bg">
    <div class="container" data-aos="fade-up">

      <header class="section-header">
        <h3 class="section-title">Blog</h3>
      </header>
      
      <div class="row" data-aos="fade-up" data-aos-delay="100">
        <div class="col-lg-12">
          <ul id="portfolio-flters">
            <li data-filter=".filter-1" class="filter-active">{{__('Page 1')}}</li>
            <li data-filter=".filter-2">{{__('Page 2')}}</li>
            <li data-filter=".filter-3">{{__('Page 3')}}</li>
            <li data-filter=".filter-4">{{__('Page 4')}}</li>
          </ul>
        </div>
      </div>
     

      <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

        <div class="col-lg-6 col-md-6 portfolio-item filter-1" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/30.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("US-China Talks: Keys on Wall Street")}}.</h4>
            <p class="text-center font-weight-bold" style="margin:0; color:#001838">30/03/2021</p>
      
            <div class="text-center" >
              <a href="{{route('thirtyfour')}}"class="link-details" title="More Details">Read More</a>
            </div>
            <br>
       </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-1" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/27.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("The Cardano Surpasses the Bitcoin Rally and Ranks in the Top 3 of the Crypto")}}.</h4>
            <p class="text-center font-weight-bold" style="margin:0; color:#001838">12/03/2021</p>
      
            <div class="text-center" >
              <a href="{{route('thirtyone')}}"class="link-details" title="More Details">Read More</a>
            </div>
            
       </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-1" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/26.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("IBEX Exceeds 8,400 Points While Markets Weigh the Effect of Biden's")}}.</h4>
            <p class="text-center font-weight-bold" style="margin:0; color:#001838">11/03/2021</p>
      
            <div class="text-center" >
              <a href="{{route('thirty')}}"class="link-details" title="More Details">Read More</a>
            </div>
      
       </div>

        <div class="col-lg-6 col-md-6 portfolio-item filter-1" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/25.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Top 5 Cryptocurrencies: BTC, UNI, THETA, VET, LUNA")}}.</h4>
            <p class="text-center font-weight-bold" style="margin:0; color:#001838">10/03/2021</p>
      
            <div class="text-center" >
              <a href="{{route('eight')}}"class="link-details" title="More Details">Read More</a>
            </div>
        
       </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-1" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/24.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Netflix 'Might' be Next Fortune 100 Firm to Buy Bitcoin")}}.</h4>
            <p class="text-center font-weight-bold" style="margin:0; color:#001838">09/03/2021</p>
      
            <div class="text-center" >
              <a href="{{route('eight')}}"class="link-details" title="More Details">Read More</a>
            </div>
        
      </div>

        <div class="col-lg-6 col-md-6 portfolio-item filter-1" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/23.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("3 Reasons Bitcoin Recovered by 8% Overnight")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">04/03/2021</p>

          <div class="text-center" >
            <a href="{{route('twentynine')}}"class="link-details" title="More Details">Read More</a>
          </div>
      
        </div>

        
        <div class="col-lg-6 col-md-6 portfolio-item filter-1" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/22.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("How Crypto is Going to Shake Up the World of Mixed Martial Arts")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">02/03/2021</p>

          <div class="text-center" >
            <a href="{{route('twentyeight')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>

        <div class="col-lg-6 col-md-6 portfolio-item filter-1" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/21.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Tesla, Bitcoin and the Crypto Space: The show Musk go on")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">25/02/2021</p>

          <div class="text-center" >
            <a href="{{route('twentyseven')}}"class="link-details" title="More Details">Read More</a>
          </div>
          <br>
        </div>

        <div class="col-lg-6 col-md-6 portfolio-item filter-2" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/20.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Swiss Crypto ETP Issuer Passes $1B Assets Under Management")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">25/02/2021</p>

          <div class="text-center" >
            <a href="{{route('twentysix')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>

        <div class="col-lg-6 col-md-6 portfolio-item filter-2" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/19.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Soccer Fan Tokens on the March as Poland’s")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">24/02/2021</p>

          <div class="text-center" >
            <a href="{{route('twentyFive')}}"class="link-details" title="More Details">Read More</a>
          </div>
          <br>
        </div>

   
        <div class="col-lg-6 col-md-6 portfolio-item filter-2" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/18.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Bitcoin's Coinbase Premium Turned Negative")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">23/02/2021</p>

          <div class="text-center" >
            <a href="{{route('twentyFour')}}"class="link-details" title="More Details">Read More</a>
          </div>
          <br>
        </div>

        <div class="col-lg-6 col-md-6 portfolio-item filter-2" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/17.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Dogecoin: The Cryptocurrency that Became the New Target of Investors")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">22/02/2021</p>

          <div class="text-center" >
            <a href="{{route('twentyThree')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>



        <div class="col-lg-6 col-md-6 portfolio-item filter-2" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/3.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("DBlockchain Mysteries: Biggest Crypto Transaction fee Oddities")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">19/02/2021</p>

          <div class="text-center" >
            <a href="{{route('six')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-2" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/13.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Bitcoin Price Breaks $40K: Here’s Where BTC May Go Next")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">12/02/2021</p>

          <div class="text-center" >
            <a href="{{route('twenty')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>



        <div class="col-lg-6 col-md-6 portfolio-item filter-2" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/12.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Optimism of the Joe Biden presidency for Bitcoin")}}.</h4><br>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">10/02/2021</p>

          <div class="text-center" >
            <a href="{{route('nineteen')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>



        <div class="col-lg-6 col-md-6 portfolio-item filter-2" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/10.jfif')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Robinhood Users Still Have a Purchase Limit for One Gamestop Share")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">8/02/2021</p>

          <div class="text-center" >
            <a href="{{route('seventeen')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-3" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/9.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("DThe European Stock Exchanges Remain Without Fuelle")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">05/02/2021</p>

          <div class="text-center" >
            <a href="{{route('fifteen')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-3" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/8.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Ether Price Finally Beats its 2018 All-Time High, Surpassing $1,428")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">3/02/2021</p>

          <div class="text-center" >
            <a href="{{route('fourteen')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-3" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/15.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("¿Ether is Flying to the Moon? The Analyze for Empirex Capital?")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">17/02/2021</p>
  
          <div class="text-center" >
            <a href="{{route('twentyTwo')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>
  


        <div class="col-lg-6 col-md-6 portfolio-item filter-3" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/7.jfif')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Bitcoin Real Daily Trading Volume Tops $22B")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">01/02/2021</p>
        
          <div class="text-center" >
            <a href="{{route('second')}}"class="link-details" title="More Details">Read More</a>
          </div><br>
        </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-3" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/6.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Fidelity Doubles Down on Hong Kong Crypto Operator")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">30/01/2021</p>

          <div class="text-center" >
            <a href="{{route('thirteen')}}"class="link-details" title="More Details">Read More</a>
          </div>
        </div>
  
        <div class="col-lg-6 col-md-6 portfolio-item filter-3" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/5.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Bitcoin Price Eyes 30% Correction")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">27/01/2021</p>

          <div class="text-center" >
            <a href="{{route('first')}}"class="link-details" title="More Details">Read More</a>
          </div>
          <br>
        </div>

        <div class="col-lg-6 col-md-6 portfolio-item filter-3" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/1.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Bitcoin Price Eyes 30% Correction")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">25/01/2021</p>

          <div class="text-center" >
            <a href="{{route('ten')}}"class="link-details" title="More Details">Read More</a>
          </div>
          <br>
        </div>


      <div class="col-lg-6 col-md-6 portfolio-item filter-3" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/2.png')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
        <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Ethereum Into list Of Top 100 Assets In The World By Market Cap")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">22/01/2021</p>

          <div class="text-center" >
            <a href="{{route('eleven')}}"class="link-details" title="More Details">Read More</a>
          </div>
          
      </div>


        <div class="col-lg-6 col-md-6 portfolio-item filter-4" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="{{asset('assets/img/portfolio/3.png')}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <div>
                </div>
              </div>
            </div>
          <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Young Americans Will Buy Bitcoin With Their Stimulus Checks")}}.</h4>
            <p class="text-center font-weight-bold" style="margin:0; color:#001838">20/01/2021</p>

            <div class="text-center" >
              <a href="{{route('twelve')}}"class="link-details" title="More Details">Read More</a>
            </div>
            
       </div>


       <div class="col-lg-6 col-md-6 portfolio-item filter-4" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="{{asset('assets/img/portfolio/1.jpg')}}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <div>
              </div>
            </div>
          </div>
        <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Bitcoin Sets New Record to Top $ 23,000")}}.</h4>
          <p class="text-center font-weight-bold" style="margin:0; color:#001838">18/01/2021</p>

          <div class="text-center" >
            <a href="{{route('four')}}"class="link-details" title="More Details">Read More</a>
          </div>
          <br>
      </div>



      <div class="col-lg-6 col-md-6 portfolio-item filter-4" data-wow-delay="0.2s">
        <div class="portfolio-wrap">
          <img src="{{asset('assets/img/portfolio/14.jpg')}}" class="img-fluid" alt="">
          <div class="portfolio-info">
            <div>
            </div>
          </div>
        </div>
        <h4 style="color: #001838; margin-bottom:0" class="text-center" style="font-family: Arial, Helvetica, sans-serif; color:18px; margin:0">{{__("Winklevoss Brothers Top Forbes Bitcoin Billionaires List")}}.</h4>
        <p class="text-center font-weight-bold" style="margin:0; color:#001838">15/02/2021</p>

        <div class="text-center" >
          <a href="{{route('twentyOne')}}"class="link-details" title="More Details">Read More</a>
        </div>
      </div>


      </div>

    </div>
  </section> 

     <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="">
          <div class="container" data-aos="fade-up">
            <div class="row">

              <div class="col-lg-6 mt-5 mb-4  text-center">
                <div class="section-header">
                    <h3 class="h3-blog">{{__('Listen to our podcast')}} </h3>
                </div>

                  <div class="" data-aos="fade-up" data-aos-delay="100">
                     <p class="p2-blog"> {{__('Every Thursday listen to all our financial news and pertinent and updated information on investment')}}.
                    </p>
                 </div>
                <a href="https://open.spotify.com/show/59XfRKpeNXoxCIISmW5tVn" target="_blank" class="btn-empirex">{{__('Listen to all our podcast')}}</a>
              </div>

              <div class="col-lg-6  text-center" data-aos="zoom-in" data-aos-delay="100">
                  <video  width="300" height="500">
                      <source src="{{asset('media/potcast.mp4')}}" type="video/mp4">  
                  </video>
              </div>

            </div>
          </div>
    </section><!-- End Why Us Section -->
        
   </main>
   <!-- End #main -->

  <!-- ======= Footer ======= -->
  @include('partials.footer')

  <!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection
