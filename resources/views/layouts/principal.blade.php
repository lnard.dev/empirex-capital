<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title')</title>}

 <!-- Facebook Pixel Code -->
   <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '794652964351719');
      fbq('track', 'PageView');
      </script>
      <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=794652964351719&ev=PageView&noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->
  



  <link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}" type="image/x-icon">
  
    <!-- principal style -->
  <link href="{{asset('css/app.css') }}" rel="stylesheet">

    



  
    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{asset('assets/vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
    <link href="{{asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
     
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,600;0,800;0,900;1,700&display=swap" rel="stylesheet">    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

  </head>
    @yield('styles')
    <style>
 

        body::-webkit-scrollbar {
            width: 10px;
            height: 10px;
        }
        body::-webkit-scrollbar-track {
            background: #fff ; 
            border-radius: 10px;
        }
        body::-webkit-scrollbar-thumb {
            border-radius: 15px;
            height: 5px;
            background: #001838;
        }
            
    </style>
  <body>
      

    @if(session('flash'))
    <div class="alert alert-success">
       {{session('flash')}}
    </div>
    @endif
  @yield('content')
  

 <!-- script principal  -->
<script src="{{asset('js/app.js')}}"></script>

 <!-- Vendor js Files -->
<script src="{{asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>
<script src="{{asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/vendor/counterup/counterup.min.js')}}"></script>
<script src="{{asset('assets/vendor/venobox/venobox.min.js')}}"></script>
<script src="{{asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/vendor/aos/aos.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

 <!-- script -->
<script src="{{asset('js/main.js')}}"></script>

@if(session('info'))
  
<script>
  toastr.success("{!! session('info') !!}")
</script>
 
@endif

@if(session('err'))
 
<script>
  toastr.error("{!! session('err') !!}" )
</script>

@endif
  @yield('scripts')

</body>
</html>  