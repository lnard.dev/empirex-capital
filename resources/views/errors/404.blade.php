@extends('layouts.principal')

@section('styles')
  
<style>
    .header{
      display: flex;
      justify-content: center;
      align-items: center;
      height: 70vh;
      text-align: center;
      width: 100%;
    }
    h1{
      font-size: 90px;
      font-weight: bold;
      width:100%;
      font-family: 'Montserrat';
      line-height: 1;
    }
    span{
       color:#001838;
    }
    p{
      font-weight: bold;
      font-family: 'Montserrat';
      font-size: 50px;
      line-height: 1;
    }
</style>

@endsection

@section('content')

  
<div class="d-flex justify-content-center align-items-center h-100">
  <div class="content text-center">
    <img src="{{asset('assets/img/logo1.png')}}" width="500px" class="img-fluid">
  </div>
</div>


<div class="header">
  <div class="header-grid">
    <h1>ERROR <span>404</span></h1>
    <P>PAGE NOT FOUND</P>
    <a class="btn-empirex" href="{{route('home')}}" >Back Home</a>
  </div>
</div>

@endsection