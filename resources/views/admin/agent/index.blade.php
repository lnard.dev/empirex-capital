@extends('admin.layouts')

@section('styles')
   
@endsection

@section('content')

<div class="container-fluid">
  <div class="d-flex justify-content-between align-items-center my-3">
  <a href="{{route('agent.create')}}"  class="btn btn-primary btn-sm">{{__('Add')}} {{__('Agent')}}</a>
  </div>
  <div class="row">
     <div class="col-md-12">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{__('Agent List')}}</h6>
        </div>
        <div class="card-body">
           
          <div class="table-responsive">
            <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>{{__('Name')}}</th>
                        <th>{{__('Email')}}</th>
                        <th>{{__('Agent Number')}}</th>
                        <th>{{__('Status')}}</th>
                        <th>{{__('Created')}}</th>
                         <th>{{__('Actions')}}</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($agents as $agent)
                    <tr>
                        <td>{{$agent->id}}</td>
                        <td>{{$agent->name}}</td>
                        <td>{{$agent->email}}</td>
                        <td>{{$agent->contrat_number}}</td>
                        <td>
                          @if($agent->status =="active")
                              <div class="badge text-white" style="background: #001838">
                                 {{$agent->status }}
                              </div> 
                          @else 
                            {{$agent->status}}
                          @endif
                        </td>
                        <td>{{$agent->created_at->diffForHumans()}}</td>

                        <td>
                            <a href="{{route('agent.show',$agent->id)}}" class="btn btn-primary btn-sm">  <i class="fas fa-eye"></i></a>
                            <form action="{{route('agent.destroy',$agent->id)}}" class="d-inline" method="POST">
                              @csrf
                              @method('DELETE')
                                <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
            
                </tbody>
               </table>
           </div> 
        </div>
     </div>
     </div>
   </div>
</div>
  <!-- /.container-fluid -->


@endsection

@section('scripts')

@endsection