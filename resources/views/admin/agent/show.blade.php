@extends('admin.layouts')

@section('styles')
    <style>
  
    </style>
@endsection

@section('content')
  <div class="container">
    <h2 class="text-purple text-center"> <strong> {{__('Profile')}}</strong></h2>
    <div class="row">
      <div class="col-md-10 mx-auto">
        <div class="card">
           <div class="card-body">
             <div class="row">
               <div class="col-md-6">
                <p class="text-purple" ><strong>{{__('Name')}}:</strong>   {{$agent->name}}</p>
                <p class="text-purple" ><strong>{{__('Email')}}:</strong>   {{$agent->email}}</p>
                <p class="text-purple" ><strong>{{__('Agent Number')}}:</strong>   {{$agent->contrat_number}}</p>
                <p class="text-purple" ><strong>{{__('Country')}}:</strong>   {{$agent->country}}</p>
               </div>
               <div class="col-md-6">
                <p class="text-purple" ><strong>{{__('Address')}}:</strong>   {{$agent->address}}</p>
                <p class="text-purple" ><strong>{{__('City')}}:</strong>   {{$agent->city}}</p>
                <p class="text-purple" ><strong>{{__('Region')}}:</strong>   {{$agent->region}}</p>
                <p class="text-purple" ><strong>{{__('Phone')}}:</strong>   {{$agent->phone}}</p>
               </div>
             </div>
           </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container mt-5">
    <h2 class="text-purple text-center"> <strong> {{__('Documents')}}</strong></h2>
    <div class="row">
      <div class="col-md-12">
       
           <table class="table table-bordered">
             <thead>
               <tr>
                 <th>Id</th>
                 <th>{{__('Name')}}</th>
                 <th>{{__('Document')}}</th>
                 <th>{{__('Status')}}</th>
                 
               </tr>
             </thead>
             <tbody>
               @foreach ($document as $doc)
                   <tr>
                     <td>{{$doc->id}}</td>
                     <td>{{$doc->name}}</td>
                     <td><a href="{{Storage::url($doc->url)}}">
                      {{$doc->name}}
                    </a></td>
                     <td>
                      <form action="{{route('edit.doc',$doc->id)}}">
                        <div class="input-group">
                          <select name="status" id="" class="form-control" >
                            <option value="Pending" {{$doc->status==='Pending' ? 'selected' :'' }}> Pending  </option>
                            <option value="confirmed" {{$doc->status==='confirmed' ? 'selected' :'' }} >Confirmed  </option>
                          </select>
                          <button type="submit" class="btn btn-primary btn-sm ml-2">{{__('Change')}}</button>
                        </div>
                      </form>
                     </td>
                    
                   </tr>

    
               @endforeach
             </tbody>
           </table>
         
      </div>
    </div>
  </div>




@endsection


@section('scripts')
    <script>
     
    </script>
@endsection
  
