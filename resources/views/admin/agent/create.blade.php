@extends('admin.layouts')

@section('styles')
   
@endsection

@section('content')

  <div class="container-fluid">
      <div class="d-flex justify-content-between align-items-center my-3">
        <h1 class="h3 mb-2 text-gray-800">{{__('Create a')}} {{__('Agent')}}</h1>
      </div>
      <div class="row">
         <div class="col-md-10 ">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Agent')}}</h6>
            </div>
            <div class="card-body">
          <form action="{{ route('agent.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
            <div class="row">    
                 <!--form-->
              <div class="col-md-6">

                  <div class="form-group">
                     <label for="">Name <span class="text-danger">*</span></label>
                     <input type="text" class="form-control  {{ $errors->any()  ? 'is-invalid' :''}}" name="name" value="{{old('name')}}" >
                     @error('name') <small class="text-danger">{{$message}}</small> @enderror
                  </div>
                 
                 <div class="form-group">
                  <label for="">Agente Number <span class="text-danger">*</span></label>
                  <input type="text" class="form-control  {{ $errors->any()  ? 'is-invalid' :''}}"   name="contrat_number"  value="{{old('contrat_number')}}" >
                  @error('contrat_number') <small class="text-danger">{{$message}}</small> @enderror
                 </div>

                <div class="form-group">
                  <label for="">address</label>
                  <input type="text" class="form-control" name="address"  value="{{old('address')}}">
                  @error('address') <small class="text-danger">{{$message}}</small> @enderror
                </div>

                <div class="form-group">
                  <label for="">country</label>
                  <input type="text" class="form-control " name="country"  value="{{old('country')}}">
                  @error('country') <small class="text-danger">{{$message}}</small> @enderror
                </div>

                
                <div class="form-group">
                  <label for="">city</label>
                   <input type="text" class="form-control" name="city"  value="{{old('city')}}">
                   @error('city') <small class="text-danger">{{'The Agent number field is required'}}</small> @enderror
                 </div>


                <small>{{__('the fields with')}} <span class="text-danger">*</span> {{__('are required')}}</small>
              </div>

                <div class="col-md-6">
                  
                  <div class="form-group">
                     <label for="">Email <span class="text-danger">*</span></label>
                     <input type="email" class="form-control   {{ $errors->any()  ? 'is-invalid' :''}}"  " name="email"  value="{{old('email')}}" >
                      @error('email') <small class="text-danger">{{$message}}</small> @enderror
                  </div>

                   <div class="form-group">
                   <label for="">region</label>
                   <input type="text" class="form-control" name="region"  value="{{old('region')}}">
                    @error('region') <small class="text-danger">{{$message}}</small> @enderror
                   </div>
                
                    <div class="form-group">
                     <label for="">phone </label>
                     <input type="text" class="form-control" name="phone"  value="{{old('phone')}}">
                     @error('phone') <small class="text-danger">{{$message}}</small> @enderror
                    </div>

                    <div class="form-group">
                    <label for="">code postal </label>
                    <input type="text" class="form-control" name="code_postal"  value="{{old('code_postal')}}">
                    @error('code_postal') <small class="text-danger">{{$message}}</small> @enderror
                   </div>

                </div>
                 <!--end-->
            </div>
               <br>
              <button class="btn btn-primary" type="submit">{{__('Save')}}</button>
               <a href="javascript:history.back()" class="btn btn-success">{{__('Back')}}</a>
        </form>

           </div>
         </div>
       </div>
    </div>
      <!-- /.container-fluid -->

@endsection

@section('scripts')

@endsection