@extends('admin.layouts')

@section('styles')
    <style>
      .boton{
        transition: all .5s ease-in-out;
      }

      .boton:hover{
        padding: 10px 10px;
        font-size: 20px;
      }

      .tarjeta{
        display: flex;
        justify-content: center;
        align-items: center;
        height:50vh;
        flex-direction: column
      }
   
    </style>
@endsection

@section('content')

  <div class="container">
    <div class="row">
   
      <div class="col-md-12">
         <div class="card">
           <div class="card-header">
            <h4 class="text-purple text-center" style="color: #001838; font-weight:bold">Contract Generator</h4>
           </div>
            <div class="card-body">
               <form action="{{route('agente.contratos')}}" method="POST">
                   @csrf
                   <div class="row">
                     <div class="col-md-6">
                        
                        <div class="form-group">
                          <label for="">{{__('Investor Name')}}</label>
                            <input type="text" class="form-control" placeholder="Name" name="investor_name">
                        </div>

                        <div class="form-group">
                          <label for="">{{__('Agent Name')}}</label>
                          <input type="text" class="form-control" placeholder="Name" name="agent_name">
                        </div>

                        <div class="form-group">
                          <label for="">{{__('Amount')}}</label>
                          <input type="text" min="1000" class="form-control" name="amount" placeholder="amount" value="{{old('amount')}}">
                         </div>

                     </div>

                     <div class="col-md-6">

                        <div class="form-group">
                          <label for="">{{__('Investor Email')}}</label>
                          <input type="email" class="form-control" placeholder="Email" name="email">
                         </div>

                         <div class="form-group">
                          <label for="">{{__('Term')}}</label>
                            <select name="term" id="" class="form-control">
                              <option value="3">3 month</option>
                              <option value="6">6 month</option>
                              <option value="12">12 month</option>
                            </select>
                           </div> 
                         </div>

                     </div>

                   </div>
                  <div class="form-group">
                     <button class="btn btn-primary">
                      Send <i class="fas fa-paper-plane"></i>
                     </button>
                  </div>
               </form>
            </div>
         </div>
      </div>
    </div>
  </div>
@endsection
  
