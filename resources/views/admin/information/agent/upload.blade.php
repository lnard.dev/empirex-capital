@extends('admin.layouts')

@section('styles')
    <style>
     
      .upload{
        border:4px dotted #001838;
        padding:30px;
        width:100%;
        position: relative;
        max-width: 100%;
      }
      .upload-file{
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        cursor: pointer;
        opacity: 0;
  
      }

      .fake-btn{
        display:flex;
        justify-content: center;
        align-items: center;
        height: 30px;
      }
    </style>
@endsection

@section('content')

  <div class="container-fluid mt-5">
    <h2 class="text-center"></h2>
     <div class="row">
       <div class="col-md-4">
        <div class="card">
          <div class="card-body">
           <form action="{{route('agente.file')}}" method="post" enctype="multipart/form-data">
                @csrf
                   <label for="" class="text-purple">Documents</label>
                    <select name="name"  class="form-control" required>
                        <option value="">select</option>
                        <option value="ID Picture">{{__('ID Picture')}}</option>
                        <option value="Bank Transfer Receipt">{{__('Bank Transfer Receipt')}}</option>
                        <option value="Other">{{__('Other')}}</option>
                    </select>
                   
                  <div class="form-group">
                    <label for="" class="text-purple">Uplaod your document</label>
                       <div class="upload">
                        <span class="fake-btn">{{__('Choose or drag File')}}</span>
                        <input type="file" class="upload-file" id="file" name="file">
                         
                       </div>
                       @error('file')
                         <small class="text-danger">{{$message}}</small> 
                         @enderror
                         <br>
                       <small>max size 1mb</small>
                  </div>
     
                  <div class="form-group">
                        <button type="submit" class="btn btn-primary boton">Upload File</button>
                  </div>
                   
           
            
            </form>
          </div>
        </div>
       </div>
       <div class="col-md-5 offset-md-1">
          <table class="table table-striped">
            <thead >
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Document</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($documents as $doc)
              <tr>
                <td>{{$doc->id}}</td>
                <td>{{$doc->name}}</td>
                <td>
                  <a href="{{Storage::url($doc->url)}}" target="_blank">{{$doc->name}}</a>
                </td>
                <td>
                  @if($doc->status=="Pending")
                    <div class="badge bg-warning">{{$doc->status}}</div>
                    @else
                    <div class="badge badge-success">{{$doc->status}}</div>
                  @endif
                </td>
                <td>
                 <a href="{{route('remove.document',$doc->id)}}">
                  <i class="fa fa-trash text-primary"></i>
                 </a>
                </td>
              </tr> 
              @endforeach
              
            </tbody>
          </table>
       </div>
     </div>
  </div>
@endsection


@section('scripts')
    <script>
      document.getElementById('file').addEventListener('change',(e)=>{
           document.querySelector('.fake-btn').innerHTML= e.target.files[0].name
      })
    </script>
@endsection
  
