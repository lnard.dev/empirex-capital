@extends('admin.layouts')



@section('content')

  <div class="container-fluid">
      <div class="d-flex justify-content-between align-items-center my-3">
        <h1 class="h3 mb-2" style="color: #001838; font-weight:bold">{{__('Hello')}}, {{ auth()->user()->name }}</h1>
      </div>
      <div class="row">
         <div class="col-md-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Profile')}}</h6>
            </div>
       <div class="card-body">     
          <form action="{{ route('agente.update')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <input type="hidden" value="{{$user->id}}" name="id">
              <div class="row">    
                  <!--form-->
                <div class="col-md-6">

                    <div class="form-group">
                      <label for="" class="text-purple">Name </label>
                      <input type="text" class="form-control" name="name"  value="{{$user->name}}" required disabled>
                      @error('name') <small class="text-danger">{{$message}}</small> @enderror
                    </div>

                 
                  
                  <div class="form-group">
                    <label for="" class="text-purple">Agente Number </label>
                    <input type="text" class="form-control" name="contrat_number"  value="{{$user->contrat_number ?? ''}}" required disabled>
                    @error('contrat_number') <small class="text-danger">{{$message}}</small> @enderror
                  </div>

                  <div class="form-group">
                    <label for="" class="text-purple">Address</label>
                    <input type="text" class="form-control" name="address"  value="{{$user->address ?? ''}}">
                    @error('address') <small class="text-danger">{{$message}}</small> @enderror
                  </div>

                  <div class="form-group">
                    <label for="" class="text-purple">Country</label>
                    <input type="text" class="form-control" name="country" value="{{$user->country ?? ''}}">
                    @error('country') <small class="text-danger">{{$message}}</small> @enderror
                  </div>

                  <div class="form-group">
                    <label for="" class="text-purple">City</label>
                    <input type="text" class="form-control" name="city"  value="{{$user->city ?? ''}}">
                    @error('city') <small class="text-danger">{{$message}}</small> @enderror
                  </div>


                </div>

                  <div class="col-md-6">
                    
                    <div class="form-group">
                      <label for="" class="text-purple" >Email </label>
                      <input type="email" class="form-control" name="email"  value="{{$user->email ?? ''}}" required disabled>
                        @error('email') <small class="text-danger">{{$message}}</small> @enderror
                    </div>

                    <div class="form-group">
                      <label for="">Password </label>
                      <input type="password" class="form-control" name="password"  value="{{$user->password ?? ''}}" required>
                      @error('password') <small class="text-danger">{{$message}}</small> @enderror
                    </div>

                    <div class="form-group">
                    <label for="" class="text-purple">Region / Province</label>
                    <input type="text" class="form-control" name="region" value="{{$user->region ?? ''}}">
                      @error('region') <small class="text-danger">{{$message}}</small> @enderror
                    </div>
                  
                      <div class="form-group">
                      <label for="" class="text-purple">Phone </label>
                      <input type="text" class="form-control" name="phone"  value="{{$user->phone ?? ''}}">
                      @error('phone') <small class="text-danger">{{$message}}</small> @enderror
                      </div>

                      <div class="form-group">
                      <label for="" class="text-purple">Code Postal </label>
                      <input type="text" class="form-control" name="code_postal"  value="{{$user->code_postal ?? ''}}">
                      @error('code_postal') <small class="text-danger">{{$message}}</small> @enderror
                    </div>

               
                  </div>
                  <!--end-->
              </div>
                <br>
                <button class="btn btn-primary" type="submit">{{__('Save profile')}}</button>
          </form>
        </div>
      </div>
    </div>
   </div>
  </div>
      <!-- /.container-fluid -->

@endsection
  
