@extends('admin.layouts')

@section('styles')
    <style>
     .file-drop-area {
  position: relative;
  display: flex;
  align-items: center;
  width: 450px;
  max-width: 100%;
  padding: 25px;
  border: 1px dashed #ccc;
  border-radius: 3px;
  transition: 0.2s;
     }
  .file-drop-area.is-active {
    background-color: rgba(255, 255, 255, 0.05);
  }


.fake-btn {
  flex-shrink: 0;
  background-color: rgba(255, 255, 255, 0.04);
  border: 1px solid rgba(255, 255, 255, 0.1);
  border-radius: 3px;
  padding: 8px 15px;
  margin-right: 10px;
  font-size: 12px;
  text-transform: uppercase;
}

.file-msg {
  font-size: small;
  font-weight: 300;
  line-height: 1.4;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.file-input {
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  cursor: pointer;
  opacity: 0;
  
  
}

.file-input:focus{
  outline: none;
}

    </style>
@endsection

@section('content')

  <div class="container-fluid">
      <div class="d-flex justify-content-between align-items-center my-3">
        <h2 class="h3 mb-2 text-gray-800" style="color: #001838; font-weight:bold">{{__('Create a New')}} {{__('Client')}}</h2>
      </div>
      <div class="row">
         <div class="col-md-10 ">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Client')}}</h6>
            </div>
            <div class="card-body">
            <form action="{{ route('agente.investors.store')}}" method="POST" enctype="multipart/form-data">
              @csrf
               <div class="row">
                   <div class="col-md-6">

                    <div class="form-group">
                      <label for="">Name <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" name="name" placeholder="Name" value="{{old('name')}}" required>
                      @error('name') <small class="text-danger">{{$message}}</small> @enderror
                   </div>

                <div class="form-group">
                 <label for="">Amount</label>
                 <input type="text" min="1000" class="form-control" name="amount" placeholder="amount" value="{{old('amount')}}" required>
                </div>

                <div class="form-group">
                  <label for="">Start date</label>
                  <input type="date" class="form-control" name="start_date" placeholder="start_date" value="{{old('start_date')}}"required >
                </div>

                <div class="form-group">
                  <label for="">Status</label>
                    <select name="status" id="" class="form-control">
                      <option value="active" selected>Active</option>
                      <option value="inactive">Inactivo</option>
                    </select>
                 </div>


                   </div>
                   <div class="col-md-6">

                    <div class="form-group">
                      <label for="">Email <span class="text-danger">*</span></label>
                      <input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')}}" required>
                      @error('email') <small class="text-danger">{{$message}}</small> @enderror

                   </div>

                   <div class="form-group">
                    <label for="">Phone </label>
                    <input type="text" class="form-control" name="phone" placeholder="phone" value="{{old('phone')}}">
                    @error('phone') <small class="text-danger">{{$message}}</small> @enderror

                 </div>
                 <div class="for-group">
                  <label for="">Type of contract
                  </label>

                         <select name="type_contrat" class="form-control" required>
                          <option value="">Select</option>
                          <option value="Traditional Fixed">Traditional Fixed - Payouts At The End </option>
                          <option value="Traditional Fixed Monthly Payouts">Traditional Fixed - Monthly Payouts </option>
                          <option value="Traditional Variable">Traditional Variable - Payouts At The End</option>
                          <option value="Traditional Variable Monthly Payouts">Traditional Variable - Monthly Payouts</option>
                          <option value="Crypto Variable">Crypto Variable Payouts At The End</option>
                          <option value="Traditional Short Term Variable">Tradictional - Short -Term -Variable</option>

                </select>
                 </div>
           
                    
                 <div class="form-group mt-3">
                   <label for="">Term</label>
                    <select name="term" class="form-control" required>
                      <option value="">Select</option>
                      <option value="3">3 Month</option>
                      <option value="6">6 Month</option>
                      <option value="12">12 Month</option>
                    </select>
                </div>

              
                   </div>
               </div>
               <br>
              <button class="btn btn-primary" type="submit">{{__('Save and go the Contract')}}</button>
               <a href="javascript:history.back()" class="btn btn-success">{{__('Back')}}</a>
            </form>

         </div>
         </div> 
       </div>
    </div>
      <!-- /.container-fluid -->

@endsection

@section('scripts')
    <script>
           document.getElementById('file-upload').addEventListener('change',function(e){
             document.querySelector('.fake-btn').innerHTML=e.target.files[0].name
           })
              
    </script>
@endsection