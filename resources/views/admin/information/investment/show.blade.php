@extends('admin.layouts')

@section('styles')

@endsection

@section('content')

  <div class="container-fluid">
       <div class="d-flex justify-content-between align-items-center my-3">
          <h3 class="h3 mb-2  font-weight-bold text-purple" style="color: #001838">{{__('General Infomation ')}}</h3>
       </div>

      <div class="row">
        <div class="col-md-12">
            {{-- information --}}
            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0" style="color: #001838;font-weight:bold">
                      Profile
              
                  </h5>
                </div>
            
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="table-responsive">
                          <table class="table table-bordered border-primary">
                            <thead>
                              <tr>
                                <th style="background: #eaecf4; border:1px solid #ccc">ID</th>
                                <td>{{$user->identification}}</td>
                              </tr>
                              <tr>
                                <th style="background: #eaecf4; border:1px solid #ccc">Investor #</th>
                                <td>{{$user->contrat_number}}</td>
                              </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">Name</th>
                                  <td>{{$user->name}}</td>
                                </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">LastName</th>
                                  <td>{{$user->lastname}}</td>
                                </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc" >Email</th>
                                  <td>{{$user->email}}</td>
                                </tr>
  
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">Second Email</th>
                                  <td>{{$user->email}}</td>
                                </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">Phone</th>
                                  <td>{{$user->phone}}</td>
                                </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">Address</th>
                                  <td>{{$user->address}}</td>
                                </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">State</th>
                                  <td>{{$user->state}}</td>
                                </tr>
  
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">City</th>
                                  <td>{{$user->city}}</td>
                                </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">Country</th>
                                  <td>{{$user->country}}</td>  
                                </tr>
  
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">Zip Code</th>
                                  <td>{{$user->code_postal}}</td>  
                                </tr>
                               <thead>
                          </table>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            {{--   <div class="card">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <button class="btn" style="color: #001838;font-weight:bold" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Bank Information
                    </button>
                  </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <table class="table table-bordered border-primary">
                          <thead>
                            <tr>
                              <th style="background: #eaecf4; border:1px solid #ccc">Bank</th>
                              <td>{{$user->bank}}</td>
                            </tr>
                            <tr>
                              <th style="background: #eaecf4; border:1px solid #ccc">Account Number</th>
                              <td>{{$user->account_number}}</td>
                            </tr>
                              <tr>
                                <th style="background: #eaecf4; border:1px solid #ccc">Routing Number or Swift</th>
                                <td>{{$user->routing_number}}</td>
                              </tr>
                              <tr>
                                <th style="background: #eaecf4; border:1px solid #ccc">Zelle</th>
                                <td>{{$user->zelle}}</td>
                              </tr>
                             
                             <thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div> --}}

              
             {{--  <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn" style="color: #001838;font-weight:bold" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Documents
                    </button>
                  </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                           <div class="col-md-12">
                             <table class="table table-bordered border-primary">
                               <thead>
                                 <tr>
                                     <td style="background: #eaecf4; border:1px solid #ccc">Date of Agreement</td>
                                     <td style="background: #eaecf4; border:1px solid #ccc">Due Date</td>
                                     <td style="background: #eaecf4; border:1px solid #ccc">Type of Agreement</td>
                                     <td style="background: #eaecf4; border:1px solid #ccc">Document</td>
                                 </tr>
                               </thead>
                               <tbody>
                                 @foreach ($agreements as $agreement)
                                      <tr>
                                        <td>{{$agreement->start_date}}</td>
                                        <td>{{$agreement->end_date}}</td>
                                        <td>{{$agreement->type_agreement}}</td>
                                        <td>
                                          <a href="{{Storage::url($agreement->upload)}}" target="_blank">
                                            <img src="{{asset('assets/img/pdf.png')}}" alt="" width="30">
                                         </a>
                                        </td>
                                      </tr>
                            
                                     
                                  @endforeach
                               </tbody>
                             </table>
                           </div>
                        </div>
                     </div> 
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                           <div class="col-md-12">
                             <table class="table table-bordered border-primary">
                               <thead>
                                 <tr>
                                     <td style="background: #eaecf4; border:1px solid #ccc">Date of Profit</td>
                                     <td style="background: #eaecf4; border:1px solid #ccc">Profit Amount</td>
                                     <td style="background: #eaecf4; border:1px solid #ccc">Document</td>
                                 </tr>
                               </thead>
                               <tbody>
                                 @foreach ($profits as $profit)
                                      <tr>
                                        <td>{{$profit->date_porfit}}</td>
                                        <td>{{$profit->amount}}</td>
                                 
                                        <td>
                                          <a href="{{Storage::url($profit->upload)}}" target="_blank">
                                            <img src="{{asset('assets/img/pdf.png')}}" alt="" width="30">
                                         </a>
                                        </td>
                                      </tr>
                            
                                     
                                  @endforeach
                               </tbody>
                             </table>
                           </div>
                        </div>
                     </div>
                    </div>

                  </div>
                </div>
              </div> --}}
              

             {{--  <div class="card">
                <div class="card-header" id="headingFour">
                  <h5 class="mb-0">
                    <button class="btn" style="color: #001838;font-weight:bold" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                      Inquiry
                    </button>
                  </h5>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                  <div class="card-body">
                   
                     <div class="row">
                       <div class="col-md-5">
                        <form method="POST" action="{{route('message.store')}}">
                           @csrf
                          <div class="form-group">
                              <input type="subject" name ="subject" class="form-control" placeholder="Your Subject">
                          </div>
                          <div class="form-group">
                               <textarea  name="body" placeholder="Message"  rows="5" class="form-control"></textarea>
                           </div>
                           <button class="btn btn-primary">Send</button>
                        </form>
                       </div>
                     </div>

                  </div>
                </div>
              </div>
 --}}
            </div>
            {{-- end information --}}
        </div>
    </div>
  </div>








@endsection

@section('scripts')
   
@endsection