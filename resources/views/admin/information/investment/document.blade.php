@extends('admin.layouts')

@section('styles')


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">

 

@endsection

@section('content')

  <div class="container-fluid">
       <div class="d-flex justify-content-between align-items-center my-3">
          <h3 class="h3 mb-2  font-weight-bold text-purple" style="color: #001838">{{__('General Infomation ')}}</h3>
       </div>

      <div class="row">
        <div class="col-md-8">
            {{-- information --}}
            <div id="accordion">
            
              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn" style="color: #001838;font-weight:bold" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Agreement <i class="fa fa-sort"></i>

                    </button>
                  </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="row">
                           <div class="col-md-12">
                            <div class="table-responsive">
                              <table  class="table">
                                <thead>
                                  <tr>
                                    <td style="background: #eaecf4; border:1px solid #ccc;font-size:14px">Date of Agreement</td>
                                    <td style="background: #eaecf4; border:1px solid #ccc;font-size:14px">Due Date</td>
                                    <td style="background: #eaecf4; border:1px solid #ccc;font-size:14px">Type of Agreement</td>
                                    <td style="background: #eaecf4; border:1px solid #ccc;font-size:14px">Document</td>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($agreements as $agreement)
                                       <tr>
                                         <td>{{$agreement->start_date}}</td>
                                         <td>{{$agreement->end_date}}</td>
                                         <td>{{$agreement->type_agreement}}</td>
                                         <td>
                                           <a href="{{Storage::url($agreement->upload)}}" target="_blank">
                                             <img src="{{asset('assets/img/pdf.png')}}" alt="" width="30">
                                          </a>
                                         </td>
                                       </tr>
                             
                                      
                                   @endforeach
                                </tbody>
                              </table>
                            </div>
                           </div>
                        </div>
                     </div> 
                    </div>

                   
                  </div>
                </div>
              </div> 


              <div class="card">
                <div class="card-header" id="headingfour">
                  <h5 class="mb-0">
                    <button class="btn" style="color: #001838;font-weight:bold" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapseThree">
                      Profits <i class="fa fa-sort"></i>
                    </button>
                  </h5>
                </div>
                <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordion">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="row">
                           <div class="col-md-12">
                            <div class="table-responsive">
                              <table  class="table table-bordered border-primary" style="font-size:15px">
                                <thead>
                                  <tr>
                                      <td style="background: #eaecf4; border:1px solid #ccc;font-size:15px">Statement Date</td>
                                      <td style="background: #eaecf4; border:1px solid #ccc;font-size:15px">Net Profits</td>
                                      {{-- <td style="background: #eaecf4; border:1px solid #ccc;font-size:15px">Total Paid</td>
                                      <td style="background: #eaecf4; border:1px solid #ccc;font-size:15px">Notes</td> --}}
                                      <td style="background: #eaecf4; border:1px solid #ccc;font-size:15px">File</td>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($profits as $profit)
                                       <tr>
                                         <td>{{$profit->date_porfit}}</td>
                                         <td>${{$profit->amount}}</td>
                                         
                                       
                                         <td>
                                           <a href="{{Storage::url($profit->upload)}}" target="_blank">
                                             <img src="{{asset('assets/img/pdf.png')}}" alt="" width="30">
                                          </a>
                                         </td>
                                       </tr>
                             
                                      
                                   @endforeach
                                </tbody>
                              </table>
                            </div>
                           </div>
                        </div>
                     </div> 
                    </div>

                   
                  </div>
                </div>
              </div> 
         

       
            </div>
            {{-- end information --}}
        </div>
    </div>
  </div>


             


@endsection

@section('scripts')


  
<script>
  
  $(document).ready(function() {
    $('#example').DataTable({
        responsive: true
    });
} );
     </script>
@endsection