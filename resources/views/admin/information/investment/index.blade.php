@extends('admin.layouts')



@section('content')

  <div class="container-fluid">
      <div class="d-flex justify-content-between align-items-center my-3">
        <h3 class="h3 mb-2  font-weight-bold text-purple" style="color: #001838">{{__('Hello')}}, {{auth()->user()->name}}</h3>

      </div>
      <div class="row">
         <div class="col-md-10">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Information')}}</h6>
            </div>
            <div class="card-body">
               
              <div class="table-responsive">
                <table class="table table-bordered"  width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="background: #eaecf4; border:1px solid #ccc">Id</th>
                            <td>{{$user->id}}</td>
                       </tr>
                       <tr>
                          <th style="background: #eaecf4; border:1px solid #ccc">Name</th>
                           <td>{{$user->name}}</td>
                       </tr>

                       <tr>
                        <th style="background: #eaecf4; border:1px solid #ccc">Email</th>
                        <td>{{$user->email}}</td>
                       </tr>
                          
                           
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc"> Investor Id</th>
                                  <td>{{$user->contrat_number}}</td>
                                </tr>
                             
                                <tr>
                                    
                                  <th style="background: #eaecf4; border:1px solid #ccc">Actions</th>
                                   <td>
                                    <a href="{{route('investment.show',$user->id)}}" class="btn btn-primary btn-sm">  <i class="fas fa-eye"> Profile</i></a>
                                    <a href="{{route('investment.edit',$user->id)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i> Edit  </a>
                                   </td>
                                </tr>

                        </tr>
                    </thead>
                    
                   
                   </table>
               </div> 
            </div>
         </div>
         </div>
       </div>
    </div>
      <!-- /.container-fluid -->

@endsection
  
