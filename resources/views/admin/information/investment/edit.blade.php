@extends('admin.layouts')

@section('styles')
<style>
     .file-drop-area {
      position: relative;
      display: flex;
      align-items: center;
      width: 450px;
      max-width: 100%;
      padding: 25px;
      border: 1px dashed #ccc;
      border-radius: 3px;
      transition: 0.2s;
        }
      .file-drop-area.is-active {
        background-color: rgba(255, 255, 255, 0.05);
      }


    .fake-btn {
      flex-shrink: 0;
      background-color: rgba(255, 255, 255, 0.04);
      border: 1px solid rgba(255, 255, 255, 0.1);
      border-radius: 3px;
      padding: 8px 15px;
      margin-right: 10px;
      font-size: 12px;
      text-transform: uppercase;
    }

    .file-msg {
      font-size: small;
      font-weight: 300;
      line-height: 1.4;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .file-input {
      position: absolute;
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      cursor: pointer;
      opacity: 0;
      
      
    }

    .file-input:focus{
      outline: none;
    }

</style>
@endsection

@section('content')

  <div class="container-fluid">
       <div class="d-flex justify-content-between align-items-center my-3">
          <h3 class="h3 mb-2  font-weight-bold text-purple" style="color: #001838">{{__('Edit your')}} {{__('Information')}}</h3>
       </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card shadow mb-4">
             <div class="card-header py-3"  >
                <h6 class="m-0 font-weight-bold">{{__('Investor')}}</h6>
             </div>
             <div class="card-body">
                 <form action="{{ route('investment.update',$user->id)}}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
            
                
                              <div class="form-group">
                                <label for="">Investor # <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="contrat_number"  value="{{$user->contrat_number ?? ''}}" disabled >
                                @error('contrat_number') <small class="text-danger">{{$message}}</small> @enderror
                              </div>
                
                                <div class="form-group">
                                    <label for="">Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="name"  value="{{$user->name ?? ''}}"  >
                                    @error('name') <small class="text-danger">{{$message}}</small> @enderror
                                </div>

                                <div class="form-group">
                                  <label for="">Password <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="password"  required >
                                  <small class="text-gray">change your password</small>

                                  @error('password') <small class="text-danger">{{$message}}</small> @enderror
                              </div>
                
                                <div class="form-group">
                                  <label for="">Email <span class="text-danger">*</span></label>
                                  <input type="email" min="1000" class="form-control" name="email"  value="{{$user->email ?? ''}}"  >
                                  @error('email') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
                
                                <div class="form-group">
                                  <label for="">Home Address <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="address"  value="{{$user->address ?? ''}}"  >
                                  @error('address') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
                
                                <div class="form-group">
                                <label for="">State <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="state"  value="{{$user->state ?? ''}}" >
                                @error('state') <small class="text-danger">{{$message}}</small> @enderror
                
                              </div>
            
                              <div class="form-group">
                                <label for="">Country </label><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="country" value="{{$user->country ?? ''}}">
                                @error('country') <small class="text-danger">{{$message}}</small> @enderror
                            </div>
            
                              <h4 style="color:#001838; font-weight:bold">Bank Information</h4>
                            <div class="form-group">
                              <label for="">Bank <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="bank" disabled value="{{$user->bank ?? ''}}" >
                                @error('bank') <small class="text-danger">{{$message}}</small> @enderror
                            </div>
            
                            <div class="form-group">
                              <label for="">Routing Number or Swift <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="routing_number" disabled  value="{{$user->routing_number ?? ''}}">
                              @error('routing_number') <small class="text-danger">{{$message}}</small> @enderror
              
                            </div>
            
                          
            
                          {{-- <div class="file-drop-area">
                            <span class="fake-btn">{{__('Choose or drag your Contract')}}</span>
                              <span class="file-msg"></span>
                              <input class="file-input" type="file" name="document" id="file-upload" >
                          </div>
                            <small>max size 1mb</small>
                            @error('document') <small class="text-danger">{{$message}}</small> @enderror --}}
                        </div>
        
                          <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">ID <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="identification"  value="{{$user->identification ?? ''}}" disabled >
                                    @error('identification') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                                <div class="form-group">
                                  <label for="">Last Name <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="lastname"  value="{{$user->lastname ?? ''}}">
                                  @error('last_name') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                                <div class="form-group">
                                  <label for="">Secondary Email <span class="text-danger">*</span></label>
                                  <input type="email" class="form-control" name="second_email"  value="{{$user->second_email ?? ''}}">
                                  @error('second_email') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
            
                                <div class="form-group">
                                  <label for="">Phone <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="phone"  value="{{$user->phone ?? ''}}">
                                  @error('phone') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                              
                                <div class="form-group">
                                  <label for="">City <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="city" value="{{$user->city ?? ''}}" >
                                  @error('city') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                                <div class="form-group">
                                  <label for="">Zip Code <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="code_postal"  value="{{$user->code_postal ?? ''}}" >
                                  @error('code_postal') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                                <div class="form-group" style="margin-top:160px">
                                  <label for="">Account Number <span class="text-danger" >*</span></label>
                                  <input type="text" class="form-control" name="account_number" disabled  value="{{$user->account_number ?? ''}}" >
                                  @error('account_number') <small class="text-danger">{{$message}}</small> @enderror
                              </div>
                              <div class="form-group">
                                <label for="">Zelle </label>
                                <input type="text" class="form-control" name="zelle"  disabled value="{{$user->zelle ?? ''}}">
                              </div>
                              
                          </div>
                    </div>
                  
                      <br>
                      <button class="btn btn-primary mx-2" type="submit">{{__('Save')}}</button>
                      <a href="javascript:history.back()" class="btn btn-success">{{__('Back')}}</a>
                 </form>
            </div>
          </div>
        </div>
       
    
    </div>
  </div>








@endsection

@section('scripts')
   
@endsection