@extends('admin.layouts')

@section('styles')

@endsection

@section('content')

  <div class="container-fluid">
       <div class="d-flex justify-content-between align-items-center my-3">
          <h3 class="h3 mb-2  font-weight-bold text-purple" style="color: #001838">{{__('General Infomation ')}}</h3>
       </div>

      <div class="row">
        <div class="col-md-6">
            {{-- information --}}
            <div id="accordion">
              
              <div class="card">
                <div class="card-header" >
                  <h5 class="mb-0" style="color: #001838;font-weight:bold">
                      Bank Information
                  </h5>
                </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="table-responsive">
                          <table class="table table-bordered border-primary">
                            <thead>
                              <tr>
                                <th style="background: #eaecf4; border:1px solid #ccc">Bank</th>
                                <td>{{$user->bank}}</td>
                              </tr>
                              <tr>
                                <th style="background: #eaecf4; border:1px solid #ccc">Account Number</th>
                                <td>{{$user->account_number}}</td>
                              </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">Routing Number or Swift</th>
                                  <td>{{$user->routing_number}}</td>
                                </tr>
                                <tr>
                                  <th style="background: #eaecf4; border:1px solid #ccc">Zelle</th>
                                  <td>{{$user->zelle}}</td>
                                </tr>
                               
                               <thead>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                
              </div>
            

            

            </div>
            {{-- end information --}}
        </div>
    </div>
  </div>








@endsection

@section('scripts')
   
@endsection