@extends('admin.layouts')

@section('styles')

@endsection

@section('content')

  <div class="container-fluid">
       <div class="d-flex justify-content-between align-items-center my-3">
          <h3 class="h3 mb-2  font-weight-bold text-purple" style="color: #001838">{{__('General Infomation ')}}</h3>
       </div>

      <div class="row">
        <div class="col-md-12">
            {{-- information --}}
            <div id="accordion">

              <div class="card">
                <div class="card-header" id="headingFour">
                  <h5 class="mb-0"  style="color: #001838;font-weight:bold" >
                   
                      Request
                 
                  </h5>
                </div>
                  <div class="card-body">
                   
                     <div class="row">
                       <div class="col-md-5">
                        <form method="POST" action="{{route('message.store')}}">
                           @csrf
                          <div class="form-group">
                              <input type="subject" name ="subject" class="form-control" placeholder="Your Subject">
                          </div>
                          <div class="form-group">
                               <textarea  name="body" placeholder="Message"  rows="5" class="form-control"></textarea>
                           </div>
                           <button class="btn btn-primary">Send</button>
                        </form>
                       </div>
                     </div>

                  </div>
               
              </div>

            </div>
            {{-- end information --}}
        </div>
    </div>
  </div>








@endsection

@section('scripts')
   
@endsection