@extends('admin.layouts')

@section('styles')


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">

 

@endsection

@section('content')

<div class="container-fluid">
  
  <div class="row">
     <div class="col-md-10 mx-auto">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{__('Newsletter')}}</h6>
        </div>
        <div class="card-body">
           

            <div class="table-responsive">
                <table class="table table-striped table-hover" id="example" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>{{__('First Name')}}</th>
                            <th>{{__('Last Name')}}</th>
                            <th>{{__('Email')}}</th>
                    </thead>
                    <tbody>
                        @foreach ($newsletter as $news)
                        <tr>
                            <td>{{$news->id}}</td>
                            <td>{{$news->name}}</td>
                            <td>{{$news->lastname}}</td>
                            <td>{{$news->email}}</td>
                        </tr>
                        @endforeach
                
                    </tbody>
                   </table>
          
            </div>
        
        </div>
     </div>
     </div>
   </div>
</div>
  <!-- /.container-fluid -->


@endsection

@section('scripts')
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>



     <script>
  
  $(document).ready(function() {
    $('#example').DataTable({
        responsive: true
    });
} );
     </script>
@endsection