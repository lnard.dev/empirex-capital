<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Empirex Capital - Dashboard</title>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}" type="image/x-icon">

    <!-- Custom fonts for  this template-->
    <link href="{{asset('adminlte/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
        <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.7.0/css/buttons.bootstrap.min.css">

    <!-- Custom styles for this template-->
<link href="{{asset('adminlte/css/sb-admin-2.css')}}" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
 <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> 

  @yield('styles')
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion move" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center " >
                   <!--<div class="sidebar-brand-icon ">
                      <img src="{{asset('assets/img/logo2.png')}}" alt="" width="80" height="50">
                  </div>-->
                <div class="sidebar-brand-text mx-3">Empirex Capital</div>
           
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            @hasrole('Admin')
             <li class="nav-item {{request()->routeIs('root') ? 'active' : ''}}">
               <a class="nav-link" href="{{route('root')}}"  data-toggle="collapse" data-target="#collapseUtilities3" aria-expanded="true" aria-controls="collapseUtilities3">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Admin</span>
               </a>
               <div id="collapseUtilities3" class="collapse" aria-labelledby="headingUtilities"
               data-parent="#accordionSidebar">
                     <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom Utilities:</h6>
                        <a class="collapse-item" href="{{route('root')}}">{{__('General informacion ')}}</a>                    
                         
                    </div>
               </div>
             </li>

             <li class="nav-item {{request()->routeIs('investors.index') ? 'active':''}} ">
                <a class="nav-link" href="{{route('investors.index')}}">
                  <i class="fas fa-fw fa-folder"></i>                   
                   <span>{{__('Investor')}}</span></a>
             </li>

             <li class="nav-item {{request()->routeIs('ambassadors.index') ? 'active':''}} ">
                <a class="nav-link" href="#">
                  <i class="fas fa-fw fa-folder"></i>                   
                   <span>{{__('Ambassador')}}</span></a>
             </li>

             <li class="nav-item {{request()->routeIs('agent.index') ? 'active':''}} ">
                <a class="nav-link" href="{{route('agent.index')}}">
                  <i class="fas fa-fw fa-folder"></i>                   
                   <span>{{__('Agent')}}</span></a>
             </li>

             <li class="nav-item  {{request()->routeIs('sell.index')? 'active':'' }} ">
                <a class="nav-link" href="{{route('sell.index')}}">
                  <i class="fas fa-fw fa-folder"></i>                   
                   <span>{{__('Buy/Sell')}}</span></a>
             </li>

             <li class="nav-item {{request()->routeIs('newsletters.index') ? 'active':''}} ">
                <a class="nav-link" href="{{route('newsletters.index')}}">
                  <i class="fas fa-fw fa-folder"></i>                   
                   <span>{{__('Newsletter')}}</span></a>
             </li>
             
             <li class="nav-item {{request()->routeIs('newsletters.index') ? 'active':''}} ">
                <a class="nav-link" href="{{route('comment.index')}}">
                  <i class="fas fa-fw fa-folder"></i>                   
                   <span>{{__('Comments')}}</span></a>
             </li>
            @endhasrole
            <!-- Divider 
            <hr class="sidebar-divider">
            <!-- Heading 
            <div class="sidebar-heading">
                Interface
            </div>-->

            <!-- Investor -->
            @hasrole('Investor')
            <li class="nav-item {{request()->routeIs('root') ? 'active' : ''}}">
               

                 <a class="nav-link {{request()->routeIs('investment.index')? 'active':'' }}" href="{{route('investment.index')}}"> 
                      <i class="fas fa-fw fa-folder"></i>
                      {{__('Profile')}}
                 </a>
                 <a  class="nav-link {{request()->routeIs('investment.bank') ? 'active':''}}" href="{{route('investment.bank',auth()->user()->id)}}">
                    <i class="fas fa-fw fa-folder"></i>
                    {{__('Bank Information')}}</a>
                 <a  class="nav-link {{request()->routeIs('investment.document') ? 'active':''}}" href="{{route('investment.document',auth()->user()->id)}}">
                    <i class="fas fa-fw fa-folder"></i>
                    {{__('Documents')}}
                </a>
                 <a  class="nav-link {{request()->routeIs('inbox') ? 'active':''}}" href="{{route('inbox')}}">
                    <i class="fas fa-fw fa-folder"></i>
                    {{__('Request')}}
                </a>

              
               
                </li>
                @endhasrole
                <!-- Ambassaador-->

             @hasrole('Ambassadors')
            <li class="nav-item  {{request()->routeIs('ambassadors.index') ? 'active' : ''}} ">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>{{__('Ambassadors')}}</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom Utilities:</h6>
                          <a class="collapse-item" href="cards.html">{{__('Information Ambassador')}}</a>
                    </div>
                </div>
            </li>
            @endhasrole

            @hasrole('Agent')
            <li class="nav-item  {{request()->routeIs('agent.index') ? 'active' : ''}} ">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>{{__('Agent')}}</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom Utilities:</h6>
                          <a class="collapse-item" href="{{ route('agente.profile')}}">{{__('Profile')}}</a>
                          <a class="collapse-item" href="{{route('agente.client')}}">{{__('Client')}}</a>
                          <a class="collapse-item" href="{{route('agente.report')}}">{{__('Reports')}}</a>
                          <a class="collapse-item" href="{{route('agente.upload')}}">{{__('Upload Files')}}</a>
                    </div>
                </div>
            </li>
            @endhasrole
           
        @hasrole('Admin')
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                settings
            </div>

            <!-- Nav Item - Charts -->
          
             <li class="nav-item {{request()->routeIs('activity.index') ? 'active':''}} ">
                <a class="nav-link" href="#">
                  <i class="fas fa-fw fa-folder"></i>                   
                   <span>{{__('Activity')}}</span></a>
             </li>
          
            <!-- Nav Item - Tables -->
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="fas fa-fw fa-table"></i>
                <span>{{__('Information')}}</span></a>
            </li>
         @endhasrole
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    <form
                        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                                aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </li>

                        <!-- Nav Item - Alerts -->
                        <li class="nav-item dropdown no-arrow mx-1">
                            
                            <!-- Dropdown - Alerts 
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="alertsDropdown">
                                <h6 class="dropdown-header">
                                    Alerts Center
                                </h6>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-primary">
                                            <i class="fas fa-file-alt text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 12, 2019</div>
                                        <span class="font-weight-bold">A new monthly report is ready to download!</span>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-success">
                                            <i class="fas fa-donate text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 7, 2019</div>
                                        $290.29 has been deposited into your account!
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-warning">
                                            <i class="fas fa-exclamation-triangle text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 2, 2019</div>
                                        Spending Alert: We've noticed unusually high spending for your account.
                                    </div>
                                </a>
                                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                            </div>-->

                        </li>

                        <!-- Nav Item - Messages -->
                        <li class="nav-item dropdown no-arrow mx-1">  {{--  --}}
                            <a class="nav-link" href="{{ route('notification.index')}}">Inbox &nbsp; 
                                <!-- Counter - Messages --> 
                                @if(auth()->user()->unreadNotifications->count())
                                  <span class="badge badge-primary">{{auth()->user()->unreadNotifications->count() }}</span>
                                @endif 

                            </a>
                            <!-- Dropdown - Messages 
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="messagesDropdown">
                                <h6 class="dropdown-header">
                                    Message Center
                                </h6>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="img/undraw_profile_1.svg"
                                            alt="">
                                        <div class="status-indicator bg-success"></div>
                                    </div>
                                    <div class="font-weight-bold">
                                        <div class="text-truncate">Hi there! I am wondering if you can help me with a
                                            problem I've been having.</div>
                                        <div class="small text-gray-500">Emily Fowler · 58m</div>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="img/undraw_profile_2.svg"
                                            alt="">
                                        <div class="status-indicator"></div>
                                    </div>
                                    <div>
                                        <div class="text-truncate">I have the photos that you ordered last month, how
                                            would you like them sent to you?</div>
                                        <div class="small text-gray-500">Jae Chun · 1d</div>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="img/undraw_profile_3.svg"
                                            alt="">
                                        <div class="status-indicator bg-warning"></div>
                                    </div>
                                    <div>
                                        <div class="text-truncate">Last month's report looks great, I am very happy with
                                            the progress so far, keep up the good work!</div>
                                        <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60"
                                            alt="">
                                        <div class="status-indicator bg-success"></div>
                                    </div>
                                    <div>
                                        <div class="text-truncate">Am I a good boy? The reason I ask is because someone
                                            told me that people say this to all dogs, even if they aren't good...</div>
                                        <div class="small text-gray-500">Chicken the Dog · 2w</div>
                                    </div>
                                </a>
                                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
                            </div>-->
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow d-flex align-items-center">
                            <a class="nav-link dropdown-toggle " href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(auth()->user()->avatar)
                                        <img class="img-fluid" style="border-radius:7px " width="80px"
                                    src="{{ Storage::url(auth()->user()->avatar)  }}">
                                    @else
                                    <div class="list-group">
                                        <img class="img-fluid mt-4" style="border-radius:7px " width="70px"
                                        src="{{ Storage::url('default.png')  }}"> <br>
                                          
                                    </div>
                               </span>
                                 @endif
                             </a>
                             <a href="" style="text-decoration: none; color:#001838" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="contenido" style="line-height: 1.3">
                                    <span class="mr-2">
                                        <strong>{{auth()->user()->name}}</strong> <br>
                                        {{Auth::user()->email}} <br>
                                        @hasrole('Admin') <small style="color:#000; font-size:15px" class="">Admin</small> @endhasrole
                                        @hasrole('Investor')  <small style="color:#000">Investor</small> @endhasrole
                                        @hasrole('Agent')  <small style="color:#000">Agent</small> @endhasrole
                               
                                 </div>
                             </a>
                                  
                                  
                                    
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                               <!-- <div class="dropdown-divider"></div>-->
                               <a class="dropdown-item" data-toggle="modal" data-target="#exampleModal3" style="cursor: pointer">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                {{__('Change Image')}}
                             </a>
                               <a class="dropdown-item" href="{{route('logout')}}">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    {{__('Logout')}}
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Content -->
                   @yield('content')
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">{{__('Logout')}}</a>
                </div>
            </div>
        </div>
    </div>

    
    <!-- Modal -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Change Your Image</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="{{ route('investment.avatar', auth()->user()->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
             <div class="form-group">
                <input type="file" name="avatar" class="form-control" >

             </div>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
        </div>
      </div>
    </div>
  </div>









    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('adminlte/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('adminlte/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('adminlte/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('adminlte/js/sb-admin-2.min.js')}}"></script>



    <!-- Page level plugins -->
    <script src="{{asset('adminlte/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('adminlte/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>


    <!-- Page level custom scripts -->
    <script src="{{asset('adminlte/js/demo/datatables-demo.js')}}"></script>
   
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
   @if(session('info'))
     <script>
        toastr.success("{!! session('info') !!}")
     </script>
   @endif

   
    
   @yield('scripts')
</body>

</html>