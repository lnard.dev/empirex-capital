@extends('admin.layouts')

@section('styles')
<style>
     .list-group-item.active{
       background: #001838;
       color:#fff;
       border: #001838;
     }

</style>
@endsection

@section('content')

   <div class="container">
     <div class="row">
       <div class="col-md-8 mx-auto">
        <ul class="list-group">
          <li class="list-group-item active" aria-current="true">New Message</li>
          <li class="list-group-item"> <strong>Name:</strong>  {{$message->name}}</li>
          <li class="list-group-item"><strong>Subject:</strong> {{$message->subject}}</li>
          <li class="list-group-item"><strong>Message:</strong> {{$message->body}}</li>
        </ul>
        
       </div>
       @hasrole('Admin')
          
     <div class="col-md-8 mt-5 mx-auto">
        <ul class="list-group">
          <li class="list-group-item active">Response Message</li>
          <li class="list-group-item">
             <form action="{{route('response')}}" method="POST">
              @csrf
                <select name="user"  class="form-control js-example-basic-single" required>
                  <option value=""> Select </option>
                  @foreach($users as $user)
                       <option value="{{$user->id}}"> {{$user->name}} </option>
                  @endforeach
                </select>
               <div class="form-group mt-2">
                <textarea name="message" class="form-control" placeholder="message"  rows="3" required></textarea>
               </div>
                 <a href="javascript:history.back()" class="btn btn-success mt-2">Back</a>
                 <button type="submit" class="btn btn-primary mt-2">Send</button>
             </form>
          </li>
        </ul>
     </div> 

       @endhasrole
     </div>
   </div>


@endsection

@section('scripts')
   <script>
     $(document).ready(function(){
       $('.js-example-basic-single').select2();
     })
   </script>
@endsection