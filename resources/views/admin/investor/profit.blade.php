@extends('admin.layouts')

@section('content')

  <div class="container-fluid">

      <div class="row">
         <div class="col-md-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Edit Profits')}}</h6>
            </div>
            <div class="card-body">
               
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Profits</h5>
                      </button>
                    </div>
                    <div class="modal-body">
                       <form action="{{route('profits.update',$profit->id)}}" method="POST" enctype="multipart/form-data">
                         @csrf
                         @method('PUT')
                         <input type="hidden" value={{$profit->id}}>
                         <div class="form-group">
                           <label for="">Payment Date</label>
                            <input type="date" class="form-control" name="date_porfit" placeholder="date agreement"  value="{{$profit->date_porfit }}" required>
                         </div>
              
                         <div class="form-group">
                          <label for="">Net Profits</label>
                           <input type="text" class="form-control" name="amount"  value="{{$profit->amount  ?? ''}} " required>
                        </div>
              
                        <div class="form-group">
                          <label for="">Total Paid</label>
                           <input type="text" class="form-control" name="total"  value="{{$profit->total ?? '' }} " required>
                        </div>
              
                        <div class="form-group">
                          <label for="">Notes</label>
                           <input type="text" class="form-control" name="notes"  value="{{$profit->notes ?? ''}}  " required >
                        </div>
              
                        <div class="form-group">
                          <label for="">Upload</label>
                          <div class="input-group mb-3">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="upload" id="inputGroupFile02">
                              <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                            </div>
                            <div class="input-group-append">
                              <span class="input-group-text" id="">Upload</span>
                            </div>
                          </div>
                        </div>
                          <button type="submit" class="btn btn-primary">Save changes</button>
              
                       </form>
                    </div>
                    <div class="modal-footer">
                    </div>
                  </div>
                </div>
              



            </div>
         </div>
         </div>
       </div>
    </div>
      <!-- /.container-fluid -->

@endsection

@section('scripts')
    <script>
      
    </script>
@endsection