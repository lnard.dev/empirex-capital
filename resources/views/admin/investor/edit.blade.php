@extends('admin.layouts')

@section('styles')
<style>
     .file-drop-area {
      position: relative;
      display: flex;
      align-items: center;
      width: 450px;
      max-width: 100%;
      padding: 25px;
      border: 1px dashed #ccc;
      border-radius: 3px;
      transition: 0.2s;
        }
      .file-drop-area.is-active {
        background-color: rgba(255, 255, 255, 0.05);
      }


    .fake-btn {
      flex-shrink: 0;
      background-color: rgba(255, 255, 255, 0.04);
      border: 1px solid rgba(255, 255, 255, 0.1);
      border-radius: 3px;
      padding: 8px 15px;
      margin-right: 10px;
      font-size: 12px;
      text-transform: uppercase;
    }

    .file-msg {
      font-size: small;
      font-weight: 300;
      line-height: 1.4;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    .file-input {
      position: absolute;
      left: 0;
      top: 0;
      height: 100%;
      width: 100%;
      cursor: pointer;
      opacity: 0;
      
      
    }

    .file-input:focus{
      outline: none;
    }

</style>
@endsection

@section('content')

  <div class="container-fluid">
       <div class="d-flex justify-content-between align-items-center my-3">
          <h3 class="h3 mb-2  font-weight-bold text-purple" style="color: #001838">{{__('Edit a')}} {{__('Investor')}}</h3>
       </div>

      <div class="row">
        <div class="col-md-6">
          <div class="card shadow mb-4">
             <div class="card-header py-3"  >
                <h6 class="m-0 font-weight-bold">{{__('Investor')}}</h6>
             </div>
             <div class="card-body">
                 <form action="{{ route('investors.update',$user->id)}}" method="POST" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
            
                
                              <div class="form-group">
                                <label for="">Investor # <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="contrat_number"  value="{{$user->contrat_number ?? ''}}" >
                                @error('contrat_number') <small class="text-danger">{{$message}}</small> @enderror
                              </div>
                
                                <div class="form-group">
                                    <label for="">Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="name"  value="{{$user->name ?? ''}}"  >
                                    @error('name') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
                
                                <div class="form-group">
                                  <label for="">Email <span class="text-danger">*</span></label>
                                  <input type="email" min="1000" class="form-control" name="email"  value="{{$user->email ?? ''}}"  >
                                  @error('email') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
                
                                <div class="form-group">
                                  <label for="">Home Address <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="address"  value="{{$user->address ?? ''}}"  >
                                  @error('address') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
                
                                <div class="form-group">
                                <label for="">State <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="state"  value="{{$user->state ?? ''}}" >
                                @error('state') <small class="text-danger">{{$message}}</small> @enderror
                
                              </div>
            
                              <div class="form-group">
                                <label for="">Country </label><span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="country" value="{{$user->country ?? ''}}">
                                @error('country') <small class="text-danger">{{$message}}</small> @enderror
                            </div>
            
                              <h4 style="color:#001838; font-weight:bold">Bank Information</h4>
                            <div class="form-group">
                              <label for="">Bank <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="bank" value="{{$user->bank ?? ''}}" >
                                @error('bank') <small class="text-danger">{{$message}}</small> @enderror
                            </div>
            
                            <div class="form-group">
                              <label for="">Routing Number or Swift <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="routing_number"  value="{{$user->routing_number ?? ''}}">
                              @error('routing_number') <small class="text-danger">{{$message}}</small> @enderror
              
                            </div>

                            <div class="form-group">
                              <label for="">status <span class="text-danger">*</span></label>
                              <select name="status" class="form-control">
                                <option value="active" {{$user->status ==="active" ? 'selected':''}}>active</option>
                                <option value="inactive" {{$user->status ==="inactive" ? 'selected':''}}>inactive</option>

                              </select>
                              @error('routing_number') <small class="text-danger">{{$message}}</small> @enderror
              
                            </div>
                           
                   
            
                          {{-- <div class="file-drop-area">
                            <span class="fake-btn">{{__('Choose or drag your Contract')}}</span>
                              <span class="file-msg"></span>
                              <input class="file-input" type="file" name="document" id="file-upload" >
                          </div>
                            <small>max size 1mb</small>
                            @error('document') <small class="text-danger">{{$message}}</small> @enderror --}}
                        </div>
        
                          <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">ID <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="identification"  value="{{$user->identification ?? ''}}" >
                                    @error('identification') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                                <div class="form-group">
                                  <label for="">Last Name <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="lastname"  value="{{$user->lastname ?? ''}}">
                                  @error('last_name') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                                <div class="form-group">
                                  <label for="">Secondary Email <span class="text-danger">*</span></label>
                                  <input type="email" class="form-control" name="second_email"  value="{{$user->second_email ?? ''}}">
                                  @error('second_email') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
            
                                <div class="form-group">
                                  <label for="">Phone <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="phone"  value="{{$user->phone ?? ''}}">
                                  @error('phone') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                              
                                <div class="form-group">
                                  <label for="">City <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="city" value="{{$user->city ?? ''}}" >
                                  @error('city') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                                <div class="form-group">
                                  <label for="">Zip Code <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="code_postal"  value="{{$user->code_postal ?? ''}}" >
                                  @error('code_postal') <small class="text-danger">{{$message}}</small> @enderror
                                </div>
            
                                <div class="form-group mt-5">
                                  <label for="">Account Number <span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="account_number"  value="{{$user->account_number ?? ''}}" >
                                  @error('account_number') <small class="text-danger">{{$message}}</small> @enderror
                              </div>
                              <div class="form-group">
                                <label for="">Zelle </label>
                                <input type="text" class="form-control" name="zelle"  value="{{$user->zelle ?? ''}}">
                              </div>

                              <div class="form-group">
                                <label for="">ID Picture </label>
                                <input type="file" class="form-control" name="url" >
                                @error('url') <small class="text-danger">{{$message}}</small>  @enderror
                              </div>
              
                            
                          </div>
                    </div>
                  
                      <br>
                      <button class="btn btn-primary mx-2" type="submit">{{__('Save')}}</button>
                      <a href="javascript:history.go(-1) " class="btn btn-success">{{__('Back')}}</a>
                 </form>
            </div>
          </div>
        </div>
       
        <div class="col-md-6">
           <div class="card">
             <div class="card-header d-flex justify-content-between align-items-center" >
                <h6 class="font-weight-bold">Agreement</h6>
                <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Add</a>
             </div>
             <div class="card-body">
               <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Date of Agreement</th>
                      <th>Due Date</th>
                      <th>Type of Agreement</th>
                      <th >Download Agreements</th>
                    </tr>
                  </thead>
                  <tbody>
                     @forelse ($agreements as $agreement)
                      <tr>
                        <td>{{$agreement->start_date}}</td>
                        <td>{{$agreement->end_date}}</td>
                        <td>{{$agreement->type_agreement}}</td>
                        <td>
                          <a href="{{Storage::url($agreement->upload)}}" target="_blank">
                          <img src="{{asset('assets/img/pdf.png')}}" alt role="presentation" aria-hidden="true" width="30">
                        </a>
                        </td>
                        <td >
                          <form action="{{route('agreements.destroy',$agreement->id)}}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" style="border: none;background:none">
                              <i class="fas fa-trash text-danger"></i>
                            </button>
                          </form>
                         
                        </td>

                        @empty 
                        <td colspan="4" class="text-center">Not data yet</td>

                      </tr>
                     @endforelse
                  </tbody>
               </table>
             </div>
           </div>

           <div class="card mt-5">
            <div class="card-header d-flex justify-content-between align-items-center" >
               <h6 class="font-weight-bold">Profits</h6>
               <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal2">Add</a>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Payment Date</th>
                      <th>Net Profit</th>
                      <th>Total Paid</th>
                      <th>Notes</th>
                      <th>Download Profits Report</th>
                    </tr>
                    
                  </thead>
                   <tbody>
                    @forelse ($profits as $profit)
                       <tr>
                         <td>{{$profit->date_porfit}}</td> 
                         <td>${{$profit->amount}}</td>   
                         <td>${{$profit->total}}</td>   
                         <td>{{$profit->notes}}</td>   
                         <td>
                           <a href="{{Storage::url($profit->upload)}}" target="_blank">
                              <img src="{{asset('assets/img/pdf.png')}}" alt="" width="30">
                           </a>
                          </td>  
                          <td>
                            <a href="{{route('profits.edit',$profit->id)}}"><i class="fas fa-pencil-alt text-danger mt-2"></i></a>
                          </td>
                          <td>
                             <form action="{{route('profits.destroy',$profit->id)}}" method="POST">
                               @csrf
                               @method('DELETE')
                               <button class="btn" style="border: none; background:none"><i class="fas fa-trash text-danger"></i></button>   
                            </form>  
                          </td> 
                      </tr> 
                      
                  
                      
                      @empty
                       <td colspan="3">
                        <p class="text-center">Not data yet</p>
                       </td>
                    @endforelse
                   
                   </tbody>
              </table>
            </div>
          </div>

           
          <div class="card mt-5">
            <div class="card-header d-flex justify-content-between align-items-center" >
               <h6 class="font-weight-bold">ID Picture</h6>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Picture</th>
                    </tr>
                    
                  </thead>
                   <tbody>
                  
                       <tr>
                         <td>{{$user->name}}</td> 
                         <td>
                         @if(isset($user->url))
                         <a href="{{Storage::url($user->url)}}" download>Download</a></td>    
                         @endif
                       </tr> 
                   
                   </tbody>
              </table>
            </div>
          </div>


        </div>
    </div>
  </div>





  {{-- modal agreement --}}
    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agreements</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form action="{{route('agreements.store')}}" method="POST" enctype="multipart/form-data">
          @csrf
          <input type="hidden" value="{{$user->id}}" name="id">
           <div class="form-group">
             <label for="">Date of Agreement</label>
              <input type="date" class="form-control" name="start_date" placeholder="date agreement">
           </div>

           <div class="form-group">
            <label for="">Due Date</label>
             <input type="date" class="form-control" name="end_date" placeholder="date agreement">
          </div>

          <div class="form-group">
            <label for="">Type of Agreement</label>
             <select name="type_agreement" class="form-control" required>
                    <option value="">Select</option>
                    <option value="Traditional Fixed">Traditional Fixed - Payouts At The End </option>
                    <option value="Traditional Fixed Monthly Payouts">Traditional Fixed - Monthly Payouts </option>
                    <option value="Traditional Variable">Traditional Variable - Payouts At The End</option>
                    <option value="Traditional Variable Monthly Payouts">Traditional Variable - Monthly Payouts</option>
                    <option value="Crypto Variable">Crypto Variable Payouts At The End</option>
                    <option value="Traditional Short Term Variable">Tradictional - Short -Term -Variable</option>
          </select>
          </div>

          <div class="form-group">
            <label for="">Upload</label>
            <div class="input-group mb-3">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="upload" id="inputGroupFile02">
                <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
              </div>
              <div class="input-group-append">
                <span class="input-group-text" id="">Upload</span>
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Save changes</button>

         </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Profits</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form action="{{route('profits.store')}}" method="POST" enctype="multipart/form-data">
           @csrf
           <input type="hidden" value="{{$user->id}}" name="id">
           <div class="form-group">
             <label for="">Payment Date</label>
              <input type="date" class="form-control" name="date_porfit" placeholder="date agreement">
           </div>

           <div class="form-group">
            <label for="">Net Profits</label>
             <input type="text" class="form-control" name="amount" >
          </div>

          <div class="form-group">
            <label for="">Total Paid</label>
             <input type="text" class="form-control" name="total" >
          </div>

          <div class="form-group">
            <label for="">Notes</label>
             <input type="text" class="form-control" name="notes" >
          </div>

          <div class="form-group">
            <label for="">Upload</label>
            <div class="input-group mb-3">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="upload" id="inputGroupFile02">
                <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
              </div>
              <div class="input-group-append">
                <span class="input-group-text" id="">Upload</span>
              </div>
            </div>
          </div>
            <button type="submit" class="btn btn-primary">Save changes</button>

         </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>




@endsection

@section('scripts')
    <script>
           document.getElementById('file-upload').addEventListener('change',function(e){
             document.querySelector('.fake-btn').innerHTML=e.target.files[0].name
           })
              
    </script>
@endsection