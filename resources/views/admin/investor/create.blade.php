@extends('admin.layouts')

@section('styles')
    <style>
     .file-drop-area {
        position: relative;
        display: flex;
        align-items: center;
        width: 450px;
        max-width: 100%;
        padding: 25px;
        border: 1px dashed #ccc;
        border-radius: 3px;
        transition: 0.2s;
     }
  .file-drop-area.is-active {
    background-color: rgba(255, 255, 255, 0.05);
  }


.fake-btn {
  flex-shrink: 0;
  background-color: rgba(255, 255, 255, 0.04);
  border: 1px solid rgba(255, 255, 255, 0.1);
  border-radius: 3px;
  padding: 8px 15px;
  margin-right: 10px;
  font-size: 12px;
  text-transform: uppercase;
}

.file-msg {
  font-size: small;
  font-weight: 300;
  line-height: 1.4;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.file-input {
  position: absolute;
  left: 0;
  top: 0;
  height: 100%;
  width: 100%;
  cursor: pointer;
  opacity: 0;
  
  
}

.file-input:focus{
  outline: none;
}

    </style>
@endsection

@section('content')

<div class="container-fluid">
  <div class="d-flex justify-content-between align-items-center my-3">
    <h3 class="h3 mb-2 font-weight-bold" style="coloer:#001838">{{__('Create a New')}} {{__('Investor')}}</h3>
  </div>
  <div class="row">
     <div class="col-md-10 ">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{__('Investor')}}</h6>
        </div>
        <div class="card-body">
          <form action="{{ route('investors.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
          <div class="row">

            <div class="col-md-6">


              <div class="form-group">
                <label for="">Investor # <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="contrat_number"value="{{old('contrat_number')}}"  >
                @error('contrat_number') <small class="text-danger">{{$message}}</small> @enderror
              </div>

                <div class="form-group">
                    <label for="">Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" value="{{old('name')}}" >
                    @error('name') <small class="text-danger">{{$message}}</small> @enderror
                </div>

                <div class="form-group">
                  <label for="">Email <span class="text-danger">*</span></label>
                  <input type="email" min="1000" class="form-control" name="email" value="{{old('email')}}" >
                  @error('email') <small class="text-danger">{{$message}}</small> @enderror
               </div>

                <div class="form-group">
                  <label for="">Home Address <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="address" value="{{old('address')}}" >
                  @error('address') <small class="text-danger">{{$message}}</small> @enderror
               </div>

               <div class="form-group">
                <label for="">State <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="state" value="{{old('state')}}" >
                @error('state') <small class="text-danger">{{$message}}</small> @enderror

              </div>

                <div class="form-group">
                  <label for="">Country </label><span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="country" value="{{old('country')}}" >
                  @error('country') <small class="text-danger">{{$message}}</small> @enderror
              </div>

              <h4 style="color:#001838; font-weight:bold">Bank Information</h4>
              <div class="form-group">
                <label for="">Bank <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="bank" value="{{old('bank')}}">
                @error('bank') <small class="text-danger">{{$message}}</small> @enderror

              </div>

              <div class="form-group">
                <label for="">Routing Number or Swift <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="routing_number" value="{{old('routing_number')}}" >
                @error('routing_number') <small class="text-danger">{{$message}}</small> @enderror

              </div>

              <div class="form-group">
                <label for="">Zelle </label>
                <input type="text" class="form-control" name="zelle" value="{{old('zelle')}}" >

              </div>

              

              {{-- <div class="file-drop-area">
                <span class="fake-btn">{{__('Choose or drag your Contract')}}</span>
                  <span class="file-msg"></span>
                  <input class="file-input" type="file" name="document" id="file-upload" >
              </div>
                <small>max size 1mb</small>
                @error('document') <small class="text-danger">{{$message}}</small> @enderror --}}
            </div>

             <div class="col-md-6">

                  <div class="form-group">
                      <label for="">ID <span class="text-danger">*</span></label>
                      <input type="text" class="form-control" name="identification" value="{{old('identification')}}" >
                      @error('identification') <small class="text-danger">{{$message}}</small> @enderror
                  </div>

                  <div class="form-group">
                    <label for="">Last Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="lastname" value="{{old('lastname')}}">
                    @error('last_name') <small class="text-danger">{{$message}}</small> @enderror
                  </div>

                  <div class="form-group">
                    <label for="">Secondary Email <span class="text-danger">*</span></label>
                    <input type="email" class="form-control" name="second_email" value="{{old('second_email')}}" >
                    @error('second_email') <small class="text-danger">{{$message}}</small> @enderror
                  </div>


                  <div class="form-group">
                    <label for="">Phone <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="phone" value="{{old('phone')}}" >
                    @error('phone') <small class="text-danger">{{$message}}</small> @enderror
                  </div>

                  
                <div class="form-group">
                  <label for="">City <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="city" value="{{old('city')}}" >
                  @error('city') <small class="text-danger">{{$message}}</small> @enderror
                </div>

                <div class="form-group">
                  <label for="">Zip Code <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="code_postal" value="{{old('code_postal')}}" >
                  @error('code_postal') <small class="text-danger">{{$message}}</small> @enderror
                </div>

                <div class="form-group mt-5">
                  <label for="">Account Number <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="account_number" value="{{old('account_number')}}" >
                  @error('account_number') <small class="text-danger">{{$message}}</small> @enderror

              </div>

              <div class="form-group mt-3">
                <label for="">Type of Account <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="type_account" value="{{old('type_account')}}" >
                @error('type_account') <small class="text-danger">{{$message}}</small> @enderror

              </div>

              <div class="form-group">
                <label for="">ID Picture </label>
                <input type="file" class="form-control" name="url" >
                @error('url') <small class="text-danger">{{$message}}</small>  @enderror
              </div>

          </div>

        
            <br>
            <button class="btn btn-primary mx-2" type="submit">{{__('Create')}}</button>
            <a href="javascript:history.back()" class="btn btn-success">{{__('Back')}}</a>
    </form>

     </div>
     </div>
   </div>
</div>
      <!-- /.container-fluid -->
 {{-- 
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Collapsible Group Item #1
                      </button>
                    </h5>
                  </div>

                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Collapsible Group Item #2
                      </button>
                    </h5>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Collapsible Group Item #3
                      </button>
                    </h5>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
 --}}
 @endsection

@section('scripts')
    <script>
           document.getElementById('file-upload').addEventListener('change',function(e){
             document.querySelector('.fake-btn').innerHTML=e.target.files[0].name
           })
              
    </script>
@endsection