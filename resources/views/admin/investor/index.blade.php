@extends('admin.layouts')

@section('content')

  <div class="container-fluid">
      <div class="d-flex justify-content-between align-items-center my-3">
        <h1 class="h3 mb-2 text-gray-800">{{__('List')}} {{__('Investor')}}</h1>
         <a href="{{route('investors.create')}}"  class="btn btn-primary btn-sm">{{__('Add')}} {{__('Investor')}}</a>
      </div>
      <div class="row">
         <div class="col-md-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Investor Table')}}</h6>
            </div>
            <div class="card-body">
               
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Investor #</th>
                            <th>Status</th>
                            <td>Actions</td>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach ($investors as $investor)
                        <tr>
                            <td>{{$investor->id}}</td>
                            <td>{{$investor->name}}</td>
                            <td>{{$investor->lastname}}</td>
                            <td>{{$investor->email}}</td>
                            <td>{{$investor->contrat_number}}</td>
                            <td>
                                @if($investor->status ==="active")
                                   <span class="badge bg-success text-white"> {{$investor->status}}</span>
                                   @else
                                   <span class="badge bg-danger text-white"> {{$investor->status}}</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('investors.edit',$investor->id)}}" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Edit  </a>
                                <form action="{{route('investors.destroy',$investor->id)}}" class="d-inline" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> Delete </button>
                                </form>
                                <a href="{{route('investors.send',$investor->id)}}" class="btn btn-success btn-sm"><i class="fas fa-send"></i> generar Password  </a>
                            </td>
                        </tr>
                        @endforeach
                
                    </tbody>
                </table>
               </div> 
            </div>
         </div>
         </div>
       </div>
    </div>
      <!-- /.container-fluid -->

@endsection

@section('scripts')
    <script>
      
    </script>
@endsection