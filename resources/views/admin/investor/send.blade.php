@extends('admin.layouts')

@section('styles')
     <style>
        
		 }


		 .content{
			 margin-top: 10px;
		 }
		 .content p {
			 border: 2px solid #001838;
			 border-radius: 20px;
			 padding: 5px 5px 5px 15px;
			 width: 400px;
			 margin: 5px auto;
			 color:#000;
			 text-align: left;
		 }
		 hr{
			border-bottom: 2px solid #001838;
			width: 450px
		 }

		 .information h2 {
       color:#F2B84B;
			 font-size: 70px;
		 }

		 .information .title {
			 color:#000;
			 text-transform: uppercase;
			 font-weight: bold;
			 font-size: 18px;

		 }

		 .information p {
			 color:#000;
			 text-align: justify;
			
		 }

		 .img {
			 position: absolute;
			 top:-100px;
			 left: -150px;
			 
		 }

		 .email {
			 text-decoration: none !important;
			 color:#001838 !important;
		 }
     </style>
@endsection


@section('content')

 
    <div class="container">
      <div class="row">
				<div class="col-md-6 mx-auto">
          <div class="card">
            <div class="card-body">
              <div class="col-md-12 mx-auto text-center">
                <img src="{{asset('assets/img/logo1.png')}}" alt="" width="300">
                <div class="content text-center">
                   <p class=" d-flex align-items-center"><img src="https://i.ibb.co/Fs4mVLV/User-Mesa-de-trabajo-1.png" width="20px" alt=""> <span class="pl-2 ">USUARIO: </a>	<a class="email">{{$user->email}}</span></p>
                   <p class=" d-flex align-items-center"><img src="https://i.ibb.co/9H1qvV3/Password-Mesa-de-trabajo-1.png" width="20px" alt=""> <span class="pl-2">CLAVE: {{$password}}  </span></p>
                   <hr>
                </div>
              </div> 
              <a href="javascript:history.back()" class="btn btn-primary">Back</a>
            </div>
          </div>
        </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script>
      
    </script>
@endsection