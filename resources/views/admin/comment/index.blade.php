@extends('admin.layouts')

@section('styles')
   
@endsection

@section('content')

<div class="container-fluid">
  
  <div class="row">
     <div class="col-md-12 mx-auto">
      <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{__('Comments')}}</h6>
        </div>
        <div class="card-body">
           
          <div class="table-responsive">
            <table class="table  table-hover" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>{{__('Name')}}</th>
                        <th>{{__('Email')}}</th>
                        <th>{{__('Comment')}}</th>
                        <th>{{__('Blog')}}</th>
                        <th>{{__('Created')}}</th>
                </thead>
                <tbody>
                    @forelse ($comments as $comment)
                    <tr>
                        <td>{{$comment->id}}</td>
                        <td>{{$comment->name}}</td>
                        <td>{{$comment->email}}</td>
                        <td>{{$comment->comment}}</td>
                        <td>{{$comment->blog_name}}</td>
                        <td>{{$comment->created_at->diffForHumans()}}</td>
                    </tr>
                    @empty
                    <tr class="col-md-12">
                      <td>Don't have comments yet</td>
                    </tr>
                    @endforelse


            
                </tbody>
               </table>
               {{$comments->links()}}
           </div> 
        </div>
     </div>
     </div>
   </div>
</div>
  <!-- /.container-fluid -->


@endsection

@section('scripts')

@endsection