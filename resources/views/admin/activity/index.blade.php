@extends('admin.layouts')

@section('content')

  <div class="container-fluid">
     
      <div class="row">
         <div class="col-md-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Recent Activities
                </h6>
            </div>
            <div class="card-body">
               
              <div class="table-responsive">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>description</th>
                            <th>created</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($activities as $activity)
                            <tr>
                                <td>{{$activity->name}}</td>
                                <td>{{$activity->description}}</td>
                                <td>{{$activity->created_at->diffForHumans()}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                   </table>
                  
               </div> 
            </div>
         </div>
         </div>
       </div>
    </div>
      <!-- /.container-fluid -->

@endsection