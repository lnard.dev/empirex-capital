@extends('admin.layouts')

@section('content') 
        @hasrole('Admin')
            <script>
                window.location="/dashboard/admin" 
            </script>
         ¿
        @endhasrole

        @hasrole('Investor')
        
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
             <h3 class="font-weight-bold text-primary">Hello,  {{auth()->user()->name}}</h3>
            <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Information')}}</h6>
            
            </div>
            <div class="card-body">
            
              <div class="table-responsive">
                <table class="table table-bordered"  width="100%" cellspacing="0">
                    <thead>
                        <thead>
                            <tr>
                                <th style="background: #eaecf4; border:1px solid #ccc">Id</th>
                                <td>{{auth()->user()->id}}</td>
                           </tr>
                           <tr>
                              <th style="background: #eaecf4; border:1px solid #ccc">Name</th>
                               <td>{{auth()->user()->name}}</td>
                           </tr>
    
                           <tr>
                            <th style="background: #eaecf4; border:1px solid #ccc">Email</th>
                            <td>{{auth()->user()->email}}</td>
                           </tr>
                              
                               
                                    <tr>
                                      <th style="background: #eaecf4; border:1px solid #ccc"> Investor Id</th>
                                      <td>{{auth()->user()->contrat_number}}</td>
                                    </tr>
                                 
                                    <tr>
                                        
                                      <th style="background: #eaecf4; border:1px solid #ccc">Actions</th>
                                       <td>
                                        <a href="{{route('investment.show',auth()->user()->id)}}" class="btn btn-primary btn-sm">  <i class="fas fa-eye"> Profile</i></a>
                                        <a href="{{route('investment.edit',auth()->user()->id)}}" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt"></i> Edit  </a>
                                       </td>
                                    </tr>
    
                            </tr>
                        </thead>
                    </thead>
                    
        
                   </table>
               </div> 
            </div>
          </div>
          </div>
        </div>
        @endhasrole

@endsection