@extends('admin.layouts')

@section('styles')


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">

    <style>
      .btn-excel {
        background: red
      }
    </style>

@endsection
@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 mx-auto">
          <div class="card">
            <div class="card-header">
              <h4 style="color: #001838;font-weight:bold">Buy/Sell</h4>
            </div>
            <div class="card-body">
               <table class="table" id="example">
                  <thead>
                     <tr>
                       <th>Id</th>
                       <th>Lookign</th>
                       <th>Crypto</th>
                       <th>Amount</th>
                       <th>First Name</th>
                       <th>last Name</th>
                       <th>Address</th>
                       <th>City</th>
                      
                       <th>Country</th>
                       <th>Phone</th>
                       <th>Date</th>
                       <th>Upload ID</th>
                       <th></th>
                     </tr>
                  </thead>
                  <tbody>
                    @forelse ($cryptos as $crypto)
                        <tr>
                          <td>{{$crypto->id}}</td>
                          <td>{{$crypto->looking}}</td>
                          <td>{{$crypto->crypto}}</td>
                          <td>{{$crypto->amount}}</td>
                          <td>{{$crypto->firstname}}</td>
                          <td>{{$crypto->lastname}}</td>
                          <td>{{$crypto->address}}</td>
                          <td>{{$crypto->city}}</td>
                         
                          <td>{{$crypto->country}}</td>
                          <td>{{$crypto->phone}}</td>
                          <td>{{$crypto->created_at}}</td>
                           <td>
                            @if($crypto->document)

                            <a href="{{Storage::url($crypto->document)}}" download>Document Id</a>
                              @else
                               Not Data
                            @endif
                          </td>
                          <td>
                            <form action="{{route('sell.destroy',$crypto->id)}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-danger btn-sm">
                                 <i class="fa fa-trash fa-1x"></i>
                              </button>
                            </form>
                          </td>
                        </tr>
               
                    @empty
                         <p>Not Data Yet</p>
                    @endforelse
                  </tbody>
               </table>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>



     <script>
  
  $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            { extend: 'excel', className: 'btn-excel',text:'Download Report' }
        ]
    } );
} );
     </script>
@endsection