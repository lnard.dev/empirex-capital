@extends('admin.layouts')

@section('styles')


@endsection

@section('content')

@hasrole('Admin')

<div class="container">
  <h2 class="text-center text-purple font-weight-bold">List Notifications</h2>
    <div class="row">
         <div class="col-md-6 text-center mt-5">
           <div class="card">
              <div class="card-header">
                <h4 class="text-purple font-weight-bold">Not Read</h2>
              </div>
              <ul class="list-group">
                @foreach($unreadNotifications as $unreadNotification)
                 <li class="list-group-item d-flex justify-content-between">
                   <a href="{{$unreadNotification->data['link']}}">
                            {{$unreadNotification->data['text']}} 
                   </a>
                 <form action="{{route('notification.read', $unreadNotification->id)}}" method="post">
                   @csrf
                   @method('PATCH')
                     <button style="cursor:pointer" class="btn btn-danger btn-sm" type="submit"> <i class="fa fa-trash "></i></button>
                   </form>
                 </li>
                  @endforeach
              </ul>
           </div>
         </div>

         <div class="col-md-6 text-center mt-5">
          <div class="card">
            <div class="card-header">
              <h4 class="text-primary font-weight-bold">Read</h4>
            </div>
          <ul class="list-group">
            @foreach($readNotifications as $readNotification)
               <li class="list-group-item list-group-item d-flex justify-content-between">
                  <a href="{{$readNotification->data['link']}}">
                           {{$readNotification->data['text']}} 
                  </a>
                  <form action="{{route('notification.destroy', $readNotification->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                      <button class="btn btn-danger" type="submit"> <i class="fa fa-trash"></i></button>
                    </form>
                </li>
            @endforeach
          </ul>
          </div>
        </div>
    </div>
</div>


@endhasrole

@hasrole('Investor')
<div class="container">
  <div class="row">
    <div class="col-md-6 mx-auto" style="color:#001838">
      <h4 class="font-weight-bold text-center">Response Empirex Capital Team</h4>
     <div class="card ">
     
         <ul class="list-group">
          @foreach($unreadNotifications as $unreadNotification)
           <li class="list-group-item d-flex justify-content-between">
             <a href="{{$unreadNotification->data['link']}}">
                      {{$unreadNotification->data['text']}} 
             </a>
           <form action="{{route('notification.read', $unreadNotification->id)}}" method="post">
             @csrf
             @method('PATCH')
               <button style="cursor:pointer" class="btn btn-danger btn-sm" type="submit"> <i class="fa fa-trash "></i></button>
             </form>
           </li>
            @endforeach
        </ul>
     
     </div>
    </div>
  </div>
</div>


@endhasrole

@endsection

@section('scripts')
   
@endsection