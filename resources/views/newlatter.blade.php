@extends('layouts.principal')

@section('styles')
    <style>
      body {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
      }

      .card{
        z-index: 5;
        border-radius: 10px;
      }

      h2{
        color:#001938;
        font-weight: bold
      }

      .overlay {
        position: fixed;
        top:0;
        left: 0;
        bottom: 0;
        right: 0;
        background-image: url('../assets/img/fondo2.jpg');
        background-size: cover;
        filter: blur()
        z-index: 2;
      }

      .fondo{
        background:#001838;
        opacity: 0.5;
        position: fixed;
        top:0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: 3;
        height: 100vw;
      }
    
    </style>
@endsection

@section('content')
    
  <div class="card p-5">
    <h2>Subscribe to our Newsletter</h2>
    <form action="{{route('newsletter')}}" method="POST">
      @csrf

      <div class="form-group">
        <input type="text" class="form-control" name="name" placeholder="First Name" required>
      </div>
      <div class="form-group">
        <input type="text" class="form-control" name="lastname" placeholder="Last Name" required>
      </div>
      <div class="form-group">
        <input type="text" class="form-control" name="email" placeholder="Email" required>
      </div>
      <div class="form-group">
         <div class="g-recaptcha mt-2"  data-sitekey="6LdlqykaAAAAAPlDnqbx4IVmgB1AjCc3pDu3-zp_"></div>
         <button class="btn subs mt-2">{{__('Subscribe')}}</button>
      </div>
    </form> 
  </div>
 
  <div class="overlay"></div>

  <div class="fondo"></div>
@endsection