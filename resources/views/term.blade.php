
 @extends('layouts.principal')

@section('styles')
    <style>
      body {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
      }

      .card{
        z-index: 5;
        border-radius: 10px;
        width: 600px;
        height:800px;
        position: fixed;
        overflow-y: scroll
      }

      h2{
        color:#001938;
        font-weight: bold
      }

      .overlay {
        position: fixed;
        top:0;
        left: 0;
        bottom: 0;
        right: 0;
        background-image: url('../assets/img/fondo2.jpg');
        background-size: cover;
        filter: blur()
        z-index: 2;
      }

      .fondo{
        background:#001838;
        opacity: 0.5;
        position: fixed;
        top:0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: 3;
        height: 100vw;
      }

      .card::-webkit-scrollbar {
            width: 5px;
            height: 10px;
        }
        .card::-webkit-scrollbar-track {
            background: #fff ; 
            border-radius: 10px;
        }
        .card::-webkit-scrollbar-thumb {
            border-radius: 15px;
            height: 5px;
            background: #ccc;
        }
    
    </style>
@endsection

@section('content')
    
  <div class="card p-5">
    <h3 class="text-center font-weight-bold" style="color:#001838">Terms and Conditions</h3>
    <p class="text-justify">Empirex Capital LLC. Is the only operator collecting or maintaining personal information through the www.empirexcapital.com website. A member (any visitor or other person    making use of the website) may contact www.empirexcapital.com using the following email address: info@empirexcapital.com.</p>
         
    <h5 style="color:#001838" class="font-weight-bold">Risk Disclaimer</h5>
    <p class="text-justify"> <strong>EMPIREX CAPITAL LLC</strong>  provides investments opportunities in the financial markets, giving the opportunity to any investor to participate in the strategies that Empirex Capital create in its own discretion. Investors subscribed in investments with Empirex Capital LLC make an agreement directly with the company, either natural persons or legally registered companies.Empirex Capital LLC does not have third parties, nor mediators in the process of Investing.</p>

    <p class="text-justify">You must be aware of the risks and be willing to accept the them in order to invest in the stock, binary , futures markets and cryptocurrencies. Do not invest  with any capital you cannot afford to lose. No representation is being made that any account will or is likely to achieve profits or losses similar to those discussed on this website or on any reports. The past performance of any investment is not necessarily indicative of future results.</p>

    <h5 style="color:#001838" class="font-weight-bold">Privacy Policy</h5>

       <p class="text-justify">This privacy notice discloses the privacy practices for <strong>EMPIREX CAPITAL</strong>. This privacy notice applies solely to information collected by this website. It will notify you of the following: <strong>Information Collection, Use, and Sharing</strong>.
        </p>

         <p class="text-justify">We are the sole owners of the information collected on this site. We only have access to and/or collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone. We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request (ie. set up your account). Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p>

      
         <p class="text-justify">   <strong>Your Access to and Control Over Information</strong> <br> You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website.</p>

         <ul>
           <li>See what data we have about you, if any. </li>
           <li>Change/correct any data we have about you.</li>
           <li>Have us delete any data we have about you.</li>
          <li>Express any concern you have about our use of your data.</li>
           
         </ul>
         <h5 style="color:#001838" class="font-weight-bold">Security</h5>

         <p class="text-justify">We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline. We also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>

         <p class="text-justify">
           The owner of <strong>EMPIREX CAPITAL</strong>, their Instagram, Twitter, Telegram, LinkedIn, Pinterest & Facebook Account, their INVESTMENT portafolios  hereby waive any liability whatsoever due to the use of its website. Use of the <strong>EMPIREX CAPITAL</strong>  account, the content and the information is made on the user’s sole liability. The user hereby releases the owner of EMPIREX CAPITAL from any liability for damage caused, through the use of <strong>EMPIREX CAPITAL</strong> and/or its content and/or its various services. The user hereby releases the owner of <strong>EMPIREX CAPITAL</strong>  and any of their social media account, their partners, agents, employees, officers, managers, directors, shareholders, etc. from any liability for losses and/or financial damages and/or personal bodily damages and/or fraud, caused through third parties. All rights reserved. If you feel that we are not abiding by this privacy policy, you should contact us immediately via telephone at 646-741-3770 in New York (USA) or via email at info@empirexcapital.com.
         </p>
         <h5 style="color:#001838" class="font-weight-bold"> Data Policy</h5>
        
         <p class="text-justify"><strong>Collection of Information</strong><br>
           1.	The website (www.empirexcapital.com) actively collects information from its members such as first and last name, age, city and state, email address, gender, birth date, and information through signup forms, applications, voluntary surveys, sweepstakes, contests, purchases, and participation in public forums such as bulletin boards. <br>
           2.	The site reserves the right to passively collect information related to site usage such as time spent on site, areas visited, and sites linked to and from. In the future, the site may collect additional information and may install cookies on members’ hard drives. <br>
           3.	The site allows members to make personal information available in public forums such as bulletin boards, to the extent the members choose to disclose such information. <br>
           4.	If a member contacts any employee or affiliated person, a record of that correspondence may be kept. <br>
           5.	The site is prohibited from conditioning a member’s participation in an activity on the member is disclosing more personal information than is reasonably necessary to participate in such activity. <br>
           
         </p>


         <h5 style="color:#001838" class="font-weight-bold"> Usage of Information</h5>
         <p class="text-justify">
           1.	Personal information is used for record keeping, site management, activities on the site, the fulfillment of requested transactions, and marketing only to members who have consented to such marketing. This may be in the form of email, text messaging, or direct mail amongst others. <br>
           2.	Personal information collected from members is not disclosed to third parties, except companies with which the web site is affiliated by common ownership. The site may 6 retain third parties to review certain personal information to advise it on demographic and marketing related issues, typically on an aggregated basis, and only after such third parties have agreed to maintain the confidentiality, security, and integrity of any personal information obtained.

         </p>
  </div>
 
  <div class="overlay"></div>

  <div class="fondo"></div>
@endsection

